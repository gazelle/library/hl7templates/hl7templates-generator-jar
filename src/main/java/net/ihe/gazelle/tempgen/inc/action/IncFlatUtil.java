package net.ihe.gazelle.tempgen.inc.action;

import org.apache.commons.lang.StringUtils;

import net.ihe.gazelle.tempmodel.org.decor.art.behavior.HasItem;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class IncFlatUtil {
	
	private IncFlatUtil() {
		// private constructor
	}
	
	public static void updateItemReferences(HasItem originalHasItem, HasItem updatedHasItem) {
		if (originalHasItem != null) {
			updateItemReferences(originalHasItem.getItem(), updatedHasItem);
		}
	}
	
	public static void updateItemReferences(Item originalItem, HasItem updatedHasItem) {
		if (originalItem != null && originalItem.getLabel() != null) {
			boolean isNew = false;
			if (updatedHasItem.getItem() == null) {
				updatedHasItem.setItem(new Item());
				isNew = true;
			}
			updatedHasItem.getItem().setLabel(
					StringUtils.defaultString(updatedHasItem.getItem().getLabel()) + 
					(isNew?"":";") + 
					originalItem.getLabel());
		}
	}
	
}
