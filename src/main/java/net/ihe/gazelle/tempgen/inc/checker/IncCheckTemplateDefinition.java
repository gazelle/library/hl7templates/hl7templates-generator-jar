package net.ihe.gazelle.tempgen.inc.checker;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import net.ihe.gazelle.tempapi.impl.TemplateDefinitionProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IncCheckTemplateDefinition extends TemplateDefinitionProcessorImpl {
	
	private Map<String, Set<String>> mapIncludedTemplates;
	
	private TemplateDefinition currentTemplateDefinition;
	
	@SuppressWarnings("unchecked")
	@Override
	public void process(TemplateDefinition t, Object... objects) {
		this.currentTemplateDefinition = t;
		mapIncludedTemplates = (Map<String, Set<String>>) objects[0];
		super.process(t, objects);
	}
	
	@Override
	public void processIncludes(List<IncludeDefinition> includes) {
		for (IncludeDefinition includeDefinition : includes) {
			if (this.mapIncludedTemplates.get(this.currentTemplateDefinition.getId()) == null){
				this.mapIncludedTemplates.put(this.currentTemplateDefinition.getId(), new  TreeSet<String>());
			}
			Rules rules = this.currentTemplateDefinition.getParentObject();
			this.mapIncludedTemplates.get(this.currentTemplateDefinition.getId()).add(
					RulesUtil.getReferenceTemplateIdentifier(rules, includeDefinition.getRef()));
		}
	}

}
