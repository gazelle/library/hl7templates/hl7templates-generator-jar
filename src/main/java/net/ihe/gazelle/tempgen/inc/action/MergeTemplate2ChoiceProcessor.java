package net.ihe.gazelle.tempgen.inc.action;

import java.util.List;

import net.ihe.gazelle.tempapi.impl.TemplateDefinitionProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.CloneUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class MergeTemplate2ChoiceProcessor extends TemplateDefinitionProcessorImpl {
	
	private ChoiceDefinition parentChoiceDefinition;
	
	private IncludeDefinition currentIncludeDefinition;
	
	private Integer indexAdd;
	
	@Override
	public void process(TemplateDefinition t, Object... objects) {
		if (objects[0] instanceof ChoiceDefinition){
			this.parentChoiceDefinition = (ChoiceDefinition)objects[0];
		}
		// objects[1] is a includeDefinition to be replaced
		this.currentIncludeDefinition = (IncludeDefinition) objects[1];
		this.indexAdd = this.parentChoiceDefinition.getIncludeOrElementOrConstraint().indexOf(this.currentIncludeDefinition);
		this.parentChoiceDefinition.getIncludeOrElementOrConstraint().remove(this.currentIncludeDefinition);
		super.process(t, objects);
	}
	
	@Override
	public void processElements(List<RuleDefinition> elements) {
		if (elements != null){
			for (RuleDefinition ruleDefinition : elements) {
				RuleDefinition clone = CloneUtil.cloneHL7TemplateEl(ruleDefinition);
				clone.setParentObject(this.parentChoiceDefinition);
				MergeUtil.updateCloneProperties(clone, this.currentIncludeDefinition.getMinimumMultiplicity(), 
						currentIncludeDefinition.getMaximumMultiplicity(), this.currentIncludeDefinition.getIsMandatory(), 
						this.currentIncludeDefinition.getConformance());
				this.parentChoiceDefinition.getIncludeOrElementOrConstraint().add(this.indexAdd++, clone);
			}
		}
	}
	
	@Override
	public void processIncludes(List<IncludeDefinition> includes) {
		if (includes != null) {
			for (IncludeDefinition includeDefinition : includes) {
				IncludeDefinition clone = CloneUtil.cloneHL7TemplateEl(includeDefinition);
				clone.setParentObject(this.parentChoiceDefinition);
				MergeUtil.updateCloneProperties(clone, this.currentIncludeDefinition.getMinimumMultiplicity(), 
						currentIncludeDefinition.getMaximumMultiplicity(), this.currentIncludeDefinition.getIsMandatory(), 
						this.currentIncludeDefinition.getConformance());
				this.parentChoiceDefinition.getIncludeOrElementOrConstraint().add(this.indexAdd++, clone);
			}
		}
	}
	
	@Override
	public void processItem(Item item) {
		IncFlatUtil.updateItemReferences(item, this.parentChoiceDefinition);
	}
	
}
