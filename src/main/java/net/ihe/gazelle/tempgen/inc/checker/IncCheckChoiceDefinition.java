package net.ihe.gazelle.tempgen.inc.checker;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import net.ihe.gazelle.tempapi.impl.ChoiceDefinitionProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IncCheckChoiceDefinition extends ChoiceDefinitionProcessorImpl {
	
	private Map<String, Set<String>> mapIncludedTemplates;
	
	private ChoiceDefinition currentChoiceDefinition;
	
	@SuppressWarnings("unchecked")
	@Override
	public void process(ChoiceDefinition t, Object... objects) {
		this.currentChoiceDefinition = t;
		mapIncludedTemplates = (Map<String, Set<String>>) objects[0];
		super.process(t, objects);
	}
	
	@Override
	public void processIncludes(List<IncludeDefinition> includes) {
		super.processIncludes(includes);
		TemplateDefinition td = ChoiceDefinitionUtil.getParentTemplateDefinition(this.currentChoiceDefinition);
		if (td != null && td.getId() !=null) {
			for (IncludeDefinition includeDefinition : includes) {
				if (includeDefinition != null) {
					if (this.mapIncludedTemplates.get(td.getId()) == null){
						this.mapIncludedTemplates.put(td.getId(), new  TreeSet<String>());
					}
					Rules rules = td.getParentObject();
					String tempIdentifier = RulesUtil.getReferenceTemplateIdentifier(rules,includeDefinition.getRef());
					if (tempIdentifier != null && this.mapIncludedTemplates.get(td.getId()) != null) {
						this.mapIncludedTemplates.get(td.getId()).add(tempIdentifier);
					}
				}
			}
		}
	}

}
