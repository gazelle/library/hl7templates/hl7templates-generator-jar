package net.ihe.gazelle.tempgen.valueset.action;

import net.ihe.gazelle.svs.ConceptListType;
import net.ihe.gazelle.svs.RetrieveValueSetResponseType;
import net.ihe.gazelle.tempapi.impl.ValueSetConceptListProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ValueSetConceptListUtil;

/**
 * 
 * @author Abderrazek Boufahja
 * 
 */
public class ValueSetConceptListProc extends ValueSetConceptListProcessorImpl {

	@Override
	public void process(ValueSetConceptList t, Object... objects) {
		RetrieveValueSetResponseType currentRetrieveValueSetResponseType = (RetrieveValueSetResponseType) objects[0];
		ValueSetConceptList currentValueSetConceptList = t;
		if (t != null && ValueSetConceptListUtil.getConcepts(currentValueSetConceptList) != null
				&& !ValueSetConceptListUtil.getConcepts(currentValueSetConceptList).isEmpty()
				&& currentRetrieveValueSetResponseType != null
				&& currentRetrieveValueSetResponseType.getValueSet() != null) {
			currentRetrieveValueSetResponseType.getValueSet().getUnsortedConceptList().add(new ConceptListType());
			currentRetrieveValueSetResponseType.getValueSet().getUnsortedConceptList().get(0).setLang("en-US");
			super.process(t, objects);
		}
	}

}
