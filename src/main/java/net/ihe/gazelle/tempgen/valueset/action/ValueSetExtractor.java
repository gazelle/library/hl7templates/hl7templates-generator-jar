package net.ihe.gazelle.tempgen.valueset.action;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.svs.RetrieveValueSetResponseType;
import net.ihe.gazelle.svs.SVSMarshaller;
import net.ihe.gazelle.tempapi.visitor.ValueSetVisitor;
import net.ihe.gazelle.tempgen.action.FileReadWrite;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempgen.valueset.flatten.VSFlattener;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class ValueSetExtractor {
	
	
	private static Logger log = LoggerFactory.getLogger(ValueSetExtractor.class);
	
	private ValueSetExtractor() {}
	
	public static void flattenAndExtractValueSetsFromDecor(Decor decorParam, File outputFile) throws IOException {
		Decor decor = VSFlattener.flattenDecorForValueSets(decorParam);
		extractValueSetsFromDecor(decor, outputFile);
	}
	
	public static void extractValueSetsFromDecor(Decor decor, File outputFile) throws IOException {
		if (decor.getTerminology() != null && decor.getTerminology().getValueSet() != null ) {
			if (!outputFile.isDirectory()) {
				ProblemHandler.handleError("The outputFile is not a directory", AnalyzerEnum.RD_VOCAB_PROCESS.getValue());
				return;
			}
			for (ValueSet valueSet : decor.getTerminology().getValueSet()) {
				RetrieveValueSetResponseType resp = new RetrieveValueSetResponseType();
				VSImplProvider vsImplProvider = new VSImplProvider();
				ValueSetVisitor.INSTANCE.visitAndProcess(valueSet, vsImplProvider, resp);
				String respString = SVSMarshaller.marshall(resp);
				if (respString.contains("Concept ")) {
					log.info("processed : " + valueSet.getId());
					String filepath = createFilePath(outputFile, valueSet);
					File out = new File(filepath);
					if (out.exists()) {
						log.info(valueSet.getId() + "already used !");
					}
					FileReadWrite.printDoc(respString, out.getAbsolutePath());
				}
				else {
					ProblemHandler.handleError("There are no concept in the declared valueset : " + valueSet.getId(), 
							AnalyzerEnum.RD_VOCAB_PROCESS.getValue());
					log.info("There are no concept in the declared valueset : " + valueSet.getId());
				}
			}
		}
	}

	private static String createFilePath(File outputFile, ValueSet valueSet) {
		StringBuilder sb = new StringBuilder();
		sb.append(outputFile.getAbsolutePath() + File.separator + valueSet.getId());
		if (valueSet.getEffectiveDate() != null) {
			sb.append("-" + valueSet.getEffectiveDate().toString().replace(":", ""));
		}
		sb.append(".xml");
		return sb.toString();
	}
	
}
