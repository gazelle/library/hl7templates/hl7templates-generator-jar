package net.ihe.gazelle.tempgen.valueset.action;

import net.ihe.gazelle.datatypes.CE;
import net.ihe.gazelle.svs.RetrieveValueSetResponseType;
import net.ihe.gazelle.tempapi.impl.ValueSetConceptProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ValueSetConceptProc extends ValueSetConceptProcessorImpl {
	
	protected RetrieveValueSetResponseType currentRetrieveValueSetResponseType;
	
	protected net.ihe.gazelle.datatypes.CE concept;
	
	@Override
	public void process(ValueSetConcept t, Object... objects) {
		this.currentRetrieveValueSetResponseType = (RetrieveValueSetResponseType) objects[0];
		if (this.currentRetrieveValueSetResponseType == null || this.currentRetrieveValueSetResponseType.getValueSet() == null ||
				this.currentRetrieveValueSetResponseType.getValueSet().getConceptList() == null ||
				this.currentRetrieveValueSetResponseType.getValueSet().getConceptList().isEmpty() ||
				this.currentRetrieveValueSetResponseType.getValueSet().getConceptList().get(0) == null
				) {
			return;
		}
		if (t == null) {
			return;
		}
		this.concept = new CE();
		super.process(t, objects);
		this.currentRetrieveValueSetResponseType.getValueSet().getConceptList().get(0).getConcept().add(concept);
	}
	
	@Override
	public void processCode(String code) {
		this.concept.setCode(code);
	}
	
	@Override
	public void processCodeSystem(String codeSystem) {
		this.concept.setCodeSystem(codeSystem);
	}
	
	@Override
	public void processCodeSystemName(String codeSystemName) {
		this.concept.setCodeSystemName(codeSystemName);
	}
	
	@Override
	public void processCodeSystemVersion(String codeSystemVersion) {
		this.concept.setCodeSystemVersion(codeSystemVersion);
	}

	@Override
	public void processDisplayName(String displayName) {
		this.concept.setDisplayName(displayName);
	}
	
}
