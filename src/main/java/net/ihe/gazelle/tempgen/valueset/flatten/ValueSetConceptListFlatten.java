package net.ihe.gazelle.tempgen.valueset.flatten;

import java.util.List;
import java.util.Map;
import java.util.Set;

import net.ihe.gazelle.tempapi.impl.ValueSetConceptListProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetRef;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ValueSetRefUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ValueSetConceptListFlatten extends ValueSetConceptListProcessorImpl {
	
	protected ValueSetConceptList currentValueSetConceptList;
	
	protected Map<String, Set<String>> cirdularRefs;
	
	VSFlattenStats stats;
	
	@SuppressWarnings("unchecked")
	@Override
	public void process(ValueSetConceptList t, Object... objects) {
		this.currentValueSetConceptList = t;
		if (objects != null && objects.length>0) {
			this.cirdularRefs = (Map<String, Set<String>>) objects[0];
		}
		if (objects != null && objects.length>1) {
			this.stats = (VSFlattenStats) objects[1];
		}
		if (this.currentValueSetConceptList != null) {
			super.process(t, objects);
		}
	}
	
	@Override
	public void processIncludes(List<ValueSetRef> includes) {
		if (includes != null) {
			for (ValueSetRef valueSetRef : includes) {
				if (valueSetRef.getRef() != null && 
						(cirdularRefs == null || cirdularRefs.get(currentValueSetConceptList.getParentObject().getId())== null || 
								!cirdularRefs.get(currentValueSetConceptList.getParentObject().getId()).contains(valueSetRef.getRef()))
						) {
					flattenInclude(valueSetRef);
				}
			}
		}
	}

	protected void flattenInclude(ValueSetRef valueSetRef) {
		int index = this.currentValueSetConceptList.getConceptOrInclude().indexOf(valueSetRef);
		if (index == -1) {
			index = this.currentValueSetConceptList.getConceptOrInclude().size();
		}
		this.currentValueSetConceptList.getConceptOrInclude().remove(valueSetRef);
		ValueSet vs = ValueSetRefUtil.extractReferencedValueSet(valueSetRef, 
				this.currentValueSetConceptList.getParentObject().getParentObject().getValueSet());
		if (vs != null && vs.getConceptList() != null) {
			if (this.stats != null) {
				this.stats.setNumberIntegrated(this.stats.getNumberIntegrated()+1);
				this.stats.getListVSFlattened().add(valueSetRef.getRef());
			}
			this.currentValueSetConceptList.getConceptOrInclude().addAll(index, vs.getConceptList().getConceptOrInclude());
		}
	}

}
