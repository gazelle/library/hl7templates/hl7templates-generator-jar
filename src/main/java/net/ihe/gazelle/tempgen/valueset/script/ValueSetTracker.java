package net.ihe.gazelle.tempgen.valueset.script;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.gen.common.Concept;
import net.ihe.gazelle.tempgen.action.FileReadWrite;

public final class ValueSetTracker {


    private static Logger log = LoggerFactory.getLogger(ValueSetTracker.class);

    private ValueSetTracker() {
    }

    public static void main(String[] args) {
        try {
            Set<String> lvs = getListUndeployedValueSet("/home/aboufahj/workspaceTopcased/ccda11-ad-validator-jar/src/main/resources/valueSets");
            File fis = new File("/home/aboufahj/workspaceTopcased/ccda11-ad-validator-jar/src/main/resources/tmp");
            fis.mkdir();
            for (String string : lvs) {
                String content =
						FileReadWrite.readDoc("/home/aboufahj/workspaceTopcased/ccda11-ad-validator-jar/src/main/resources/valueSets/" + string +
								".xml");
                FileReadWrite.printDoc(content, fis.getAbsolutePath() + "/" + string + ".xml");
            }
            log.info("not find number : {}", lvs.size());
            for (String string : lvs) {
                log.info("{}.xml", string);
            }
        } catch (IOException e) {
            log.error("Error: ", e);
        }

    }

    private static Set<String> getListUndeployedValueSet(String string) {
        Set<String> res = new TreeSet<>();
        File fil = new File(string);
        log.info("total number : {}", fil.listFiles().length);
        for (File fis : fil.listFiles()) {
            String id = fis.getName().substring(0, fis.getName().lastIndexOf('.'));
            List<Concept> aa = (new SVSConsumer()).getConceptsListFromValueSet(id, null);
            if (aa == null || aa.isEmpty()) {
                res.add(id);
            }
        }
        return res;
    }

}
