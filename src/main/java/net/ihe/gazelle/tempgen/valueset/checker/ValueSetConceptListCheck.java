package net.ihe.gazelle.tempgen.valueset.checker;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.ihe.gazelle.tempapi.impl.ValueSetConceptListProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetRef;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ValueSetConceptListCheck extends ValueSetConceptListProcessorImpl {
	
	protected ValueSetConceptList currentValueSetConceptList;
	
	protected Map<String, Set<String>> vsReferences;
	
	@SuppressWarnings("unchecked")
	@Override
	public void process(ValueSetConceptList t, Object... objects) {
		this.currentValueSetConceptList = t;
		this.vsReferences = (Map<String, Set<String>>) objects[0];
		if (this.currentValueSetConceptList != null) {
			super.process(t, objects);
		}
	}
	
	@Override
	public void processIncludes(List<ValueSetRef> includes) {
		if (includes != null) {
			for (ValueSetRef valueSetRef : includes) {
				if (valueSetRef.getRef() != null) {
					if (this.vsReferences.get(this.currentValueSetConceptList.getParentObject().getId()) == null) {
						this.vsReferences.put(this.currentValueSetConceptList.getParentObject().getId(), new HashSet<String>());
					}
					this.vsReferences.get(this.currentValueSetConceptList.getParentObject().getId()).add(valueSetRef.getRef());
				}
			}
		}
	}

}
