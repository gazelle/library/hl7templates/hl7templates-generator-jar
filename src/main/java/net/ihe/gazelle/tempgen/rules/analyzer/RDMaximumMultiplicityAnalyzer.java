package net.ihe.gazelle.tempgen.rules.analyzer;

import net.ihe.gazelle.tempgen.action.OCLCleaner;
import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempgen.action.OwnedRuleManager;
import net.ihe.gazelle.tempgen.action.UMLElementsManager;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class RDMaximumMultiplicityAnalyzer implements ConstraintGenerator{
	
	static final int MAX_INTEGER = 1000000000;
	
	@Override
	public String generateOCLConstraint(RuleDefinition ruleDefinition) {
		Integer maximumMultiplicity = null;
		String maximumMultiplicityString = ruleDefinition.getMaximumMultiplicity();
		if (maximumMultiplicityString  != null){
			if (maximumMultiplicityString.equals("*")){
				maximumMultiplicity = MAX_INTEGER;
			}
			else {
				maximumMultiplicity = Integer.valueOf(maximumMultiplicityString);
			}
		}
		String contextRule = (new OCLGenerator()).generateRuleToParentFromPath(ruleDefinition);
		String res = "";
		if (contextRule.equals("self")){
			res = contextRule + "." + RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition)
					+ OCLGenerator.getExtraSelectionIfNeeded(ruleDefinition)
					+ OCLCleaner.generateRejectOcl(false) + "->size()<" + (maximumMultiplicity+1);
		}
		else {
			res = contextRule + "->forAll(";
			int max = (new UMLElementsManager()).getMaxAttribute(RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition), null);
			if (max == 1){
				res = res + RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition) + ".oclIsUndefined())";
			}
			else{
				res = res + RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition)
						+ OCLGenerator.getExtraSelectionIfNeeded(ruleDefinition)
						+ "->size()<" +  + (maximumMultiplicity+1) + ")";
			}
		}
		return res;
	}
	
	@Override
	public String generateCommentConstraint(RuleDefinition selectedRuleDefinition) {
		String templateName = RuleDefinitionUtil.getParentTemplateDefinition(selectedRuleDefinition).getDisplayName();
		DParent dparent = RuleDefinitionUtil.getDParentOfTheParent(selectedRuleDefinition);
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		DParent dparentRD = RuleDefinitionUtil.getDParentOfRuleDefinition(selectedRuleDefinition);
		String elementName = DPathExtractor.createPathFromDParent(dparentRD.getTail()).substring(1);
		return "In " + templateName + ", " + parentPath + " SHALL contain at most " + 
				OwnedRuleManager.convertMultiplicity(selectedRuleDefinition.getMaximumMultiplicity()) + " " + elementName;
	}

	@Override
	public String getProcessIdentifier() {
		return AnalyzerEnum.RD_MAX_MULTIPLICITY_PROCESS.getValue();
	}

}
