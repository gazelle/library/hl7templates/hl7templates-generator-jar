package net.ihe.gazelle.tempgen.rules.analyzer;

import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempgen.action.OwnedRuleManager;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AttributeUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ContainDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

import java.util.List;

public class ContainMaximumMultiplicityAnalyzer implements ConstraintGenerator {

    /**
     * if max = *, no constraint will be generated
     */
    @Override
    public String generateOCLConstraint(RuleDefinition ruleDefinition) {

        String res = "";
        List<ContainDefinition> contains = RuleDefinitionUtil.getContains(ruleDefinition);
        RuleDefinition templateId = null;
        Integer maximumMultiplicity = null;
        if (contains != null && !contains.isEmpty()) {
            templateId = ContainDefinitionUtil.getTemplateIdElement(contains.get(0));
            if (templateId != null) {
                Attribute attr = templateId.getAttribute().get(0);
                String contextRule = (new OCLGenerator()).generateRuleToParentFromPath(attr);

                if (contextRule != null && !contextRule.isEmpty()) {
                    String maximumMultiplicityString = contains.get(0).getMaximumMultiplicity();
                    maximumMultiplicity = Integer.valueOf(maximumMultiplicityString);

                    if (maximumMultiplicity > 0) {
                        res = contextRule + "->size()<" + (maximumMultiplicity + 1);
                    } else if (maximumMultiplicity == 0) {
                        res = contextRule + "->size()=" + (maximumMultiplicity);
                    }
                }
            }
        }
        return res;
    }

    @Override
    public String generateCommentConstraint(RuleDefinition ruleDefinition) {
        List<ContainDefinition> contains = RuleDefinitionUtil.getContains(ruleDefinition);
        RuleDefinition templateId = null;
        Integer maximumMultiplicity = null;
        if (contains != null && !contains.isEmpty()) {
            templateId = ContainDefinitionUtil.getTemplateIdElement(contains.get(0));
            String maximumMultiplicityString = contains.get(0).getMaximumMultiplicity();
            maximumMultiplicity = Integer.valueOf(maximumMultiplicityString);
            String templateName = ContainDefinitionUtil.getParentTemplateDefinition(contains.get(0)).getDisplayName();
            Attribute attribute = templateId.getAttribute().get(0);
            DParent dparent = AttributeUtil.getDParentOfTheParent(attribute);

            String parentPath = DPathExtractor.createPathFromDParent(dparent);

            String elementName = DPathExtractor.createPathFromDParent(dparent.getTail()).substring(1);
            return "In " + templateName + ", " + parentPath + " SHALL contain at most " +
                    OwnedRuleManager.convertMultiplicity(maximumMultiplicity) + " " + elementName;
        }
        return null;
    }

    @Override
    public String getProcessIdentifier() {
        return AnalyzerEnum.CONTAIN_MAX_MULTIPLICITY_PROCESS.getValue();
    }
}
