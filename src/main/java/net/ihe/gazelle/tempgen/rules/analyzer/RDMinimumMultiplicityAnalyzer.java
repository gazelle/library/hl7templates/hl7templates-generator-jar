package net.ihe.gazelle.tempgen.rules.analyzer;

import net.ihe.gazelle.goc.uml.utils.UMLLoader;
import net.ihe.gazelle.tempgen.action.OCLCleaner;
import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempgen.action.OwnedRuleManager;
import net.ihe.gazelle.tempgen.action.UMLElementsManager;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class RDMinimumMultiplicityAnalyzer implements ConstraintGenerator {

	public String generateOCLConstraint_old(RuleDefinition ruleDefinition) {
		Integer minimumMultiplicity = ruleDefinition.getMinimumMultiplicity();
		String contextRule = (new OCLGenerator()).generateRuleToParentFromPath(ruleDefinition);
		String res = "";
		if (contextRule.equals("self")){
			res = "(not self.nullFlavor.oclIsUndefined()) or self."
					+ RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition)
					+ OCLGenerator.getExtraSelectionIfNeeded(ruleDefinition)
					+ OCLCleaner.generateRejectOcl(false) + "->size()>" + (minimumMultiplicity-1);
		}
		else {
			res = contextRule + OCLCleaner.generateRejectOcl(false) + "->forAll(";
			int max = (new UMLElementsManager()).getMaxAttribute(RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition), null);
			String nullFlavorEliminationOCL = extractEliminationOfNullFlavor(ruleDefinition);
			if (max == 1){
				res = res + nullFlavorEliminationOCL + "not " + RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition) + ".oclIsUndefined())";
			}
			else{
				res = res + nullFlavorEliminationOCL
						+ RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition)
						+ OCLGenerator.getExtraSelectionIfNeeded(ruleDefinition)
						+ "->size()>" +  + (minimumMultiplicity-1) + ")";
			}
		}
		return res;
	}

	private String extractEliminationOfNullFlavor(RuleDefinition ruleDefinition) {
		RuleDefinitionUtil.getParentTemplateDefinition(ruleDefinition);
		Object parent = ruleDefinition.getParentObject();
		if (parent instanceof RuleDefinition) {
			RuleDefinition pRD = (RuleDefinition)parent;
			String typeName = RuleDefinitionUtil.getConstrainedUMLTypeName(pRD);
			typeName = typeName + ".nullFlavor";
			if (UMLLoader.isAnAttribute(typeName)) {
				return "(not nullFlavor.oclIsUndefined()) or ";
			}
		}
		else if (parent instanceof ChoiceDefinition) {
			ChoiceDefinition cd = (ChoiceDefinition) parent;
			if (cd.getParentObject() instanceof RuleDefinition) {
				RuleDefinition pRD = (RuleDefinition)cd.getParentObject();
				String typeName = RuleDefinitionUtil.getConstrainedUMLTypeName(pRD);
				typeName = typeName + ".nullFlavor";
				if (UMLLoader.isAnAttribute(typeName)) {
					return "(not nullFlavor.oclIsUndefined()) or ";
				}
			}
		}
		return "";
	}

///////////////////////////////////////////////

	@Override
	public String generateOCLConstraint(RuleDefinition ruleDefinition) {
		OCLGenerator generator = new OCLGenerator();
		StringBuilder constraint = new StringBuilder(generator.generateRuleToParentFromPathNF(ruleDefinition));
		constraint.append(generator.getMinimumMultiplicityRuleForRD(constraint.toString(), ruleDefinition));
		return constraint.toString();
	}

	@Override
	public String generateCommentConstraint(RuleDefinition selectedRuleDefinition) {
		Integer minimumMultiplicity = selectedRuleDefinition.getMinimumMultiplicity();
		String templateName = RuleDefinitionUtil.getParentTemplateDefinition(selectedRuleDefinition).getDisplayName();
		DParent dparent = RuleDefinitionUtil.getDParentOfTheParent(selectedRuleDefinition);
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		DParent dparentRD = RuleDefinitionUtil.getDParentOfRuleDefinition(selectedRuleDefinition);
		String elementName = DPathExtractor.createPathFromDParent(dparentRD.getTail()).substring(1);
		return "In " + templateName + ", " + parentPath + " SHALL contain at least " + 
				OwnedRuleManager.convertMultiplicity(minimumMultiplicity) + " " + elementName;
	}

	@Override
	public String getProcessIdentifier() {
		return AnalyzerEnum.RD_MIN_MULTIPLICITY_PROCESS.getValue();
	}

}
