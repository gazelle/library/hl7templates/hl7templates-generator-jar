package net.ihe.gazelle.tempgen.rules.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public enum ElementClosed {
	TEMPLATE, ELEMENT;

	public static ElementClosed elementOrTemplateClosed(RuleDefinition ruleDefinition) {
		TemplateDefinition td = RuleDefinitionUtil.getParentTemplateDefinition(ruleDefinition);
		if (td != null && td.isIsClosed()) {
			return ElementClosed.TEMPLATE;
		}
		if (ruleDefinition != null && ruleDefinition.isIsClosed()) {
			return ElementClosed.ELEMENT;
		}
		return null;
	}
}