package net.ihe.gazelle.tempgen.rules.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public interface ConstraintGenerator {
	
	String generateOCLConstraint(RuleDefinition ruleDefinition);
	
	String generateCommentConstraint(RuleDefinition ruleDefinition);
	
	String getProcessIdentifier();

}
