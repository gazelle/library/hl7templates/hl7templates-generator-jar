package net.ihe.gazelle.tempgen.rules.analyzer;

import java.util.Set;

import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IsClosedElWithDistGenerator implements ConstraintGenerator {
	
	private String subElement;
	
	private Set<String> setDistinguishers;
	
	public Set<String> getSetDistinguishers() {
		return setDistinguishers;
	}

	public void setSetDistinguishers(Set<String> setDistinguishers) {
		this.setDistinguishers = setDistinguishers;
	}

	public String getSubElement() {
		return subElement;
	}

	public void setSubElement(String subElement) {
		this.subElement = subElement;
	}

	@Override
	public String generateOCLConstraint(RuleDefinition ruleDefinition) {
		String res = "";
		if (ruleDefinition.getParentObject() instanceof TemplateDefinition) {
			res = "self."+ this.subElement +  "->size()=self." + this.subElement + "->select(";
			res = addDistinguisher(res);
			res = res + ")->size()";
		}
		else {
			res = (new OCLGenerator()).generateRuleContextToRuleDefinition(ruleDefinition) + 
					"->forAll( " + this.subElement + "->size()=" + this.subElement + "->select(";
			res = addDistinguisher(res);
			res = res + ")->size() )";
		}
		return res;
	}

	private String addDistinguisher(String resParam) {
		StringBuilder sb = new StringBuilder(resParam);
		if (this.setDistinguishers != null) {
			if (this.setDistinguishers.size()==1) {
				sb.append(this.setDistinguishers.iterator().next());
			}
			else {
				int i = 0;
				for (String dist : this.setDistinguishers) {
					if (i++>0) {
						sb.append(" or ");
					}
					sb.append("(" + dist + ")");
				}
			}
		}
		return sb.toString();
	}

	@Override
	public String generateCommentConstraint(RuleDefinition ruleDefinition) {
		String templateName = RuleDefinitionUtil.getParentTemplateDefinition(ruleDefinition).getDisplayName();
		DParent dparent = RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition);
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		ElementClosed ec = ElementClosed.elementOrTemplateClosed(ruleDefinition);
		String elementOrTemplateClosed = ec != null?ec.toString().toLowerCase():"";
		return "In " + templateName + ", the sub-element " + this.subElement + " of " + parentPath + " SHALL NOT be provided "
				+ "(the " + elementOrTemplateClosed + " is closed), unless those "
				+ "specified in the template definition. Some of the sub-element(s) " + this.subElement + " provided are illegal.";
	}

	@Override
	public String getProcessIdentifier() {
		return AnalyzerEnum.RD_ISCLOSED_PROCESS.getValue();
	}
	
}
