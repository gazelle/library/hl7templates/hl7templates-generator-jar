package net.ihe.gazelle.tempgen.rules.analyzer;

import net.ihe.gazelle.tempgen.action.OCLCleaner;
import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IsClosedElementGenerator implements ConstraintGenerator {
	
	private String subElement;
	
	public String getSubElement() {
		return subElement;
	}

	public void setSubElement(String subElement) {
		this.subElement = subElement;
	}

	@Override
	public String generateOCLConstraint(RuleDefinition ruleDefinition) {
		String res = "";
		if (ruleDefinition.getParentObject() instanceof TemplateDefinition) {
			res = "self."+ this.subElement + OCLCleaner.generateRejectOcl(true) + "->size()=0";
		}
		else {
			res = (new OCLGenerator()).generateRuleContextToRuleDefinition(ruleDefinition) + 
					"." + this.subElement + 
					OCLCleaner.generateRejectOcl(true) + "->size()=0";
		}
		return res;
	}

	@Override
	public String generateCommentConstraint(RuleDefinition ruleDefinition) {
		String templateName = RuleDefinitionUtil.getParentTemplateDefinition(ruleDefinition).getDisplayName();
		DParent dparent = RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition);
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		ElementClosed ec = ElementClosed.elementOrTemplateClosed(ruleDefinition);
		String elementOrTemplateClosed = ec != null?ec.toString().toLowerCase():"";
		return "In " + templateName + ", the sub-element " + this.subElement + " of " + parentPath + " SHALL NOT be provided (the " + 
				elementOrTemplateClosed + " is closed)";
	}

	@Override
	public String getProcessIdentifier() {
		return AnalyzerEnum.RD_ISCLOSED_PROCESS.getValue();
	}

}
