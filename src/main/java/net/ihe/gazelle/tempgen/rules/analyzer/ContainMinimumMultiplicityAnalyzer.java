package net.ihe.gazelle.tempgen.rules.analyzer;

import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempgen.action.OwnedRuleManager;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AttributeUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ContainDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

import java.util.List;

public class ContainMinimumMultiplicityAnalyzer implements ConstraintGenerator {
    @Override
    public String generateOCLConstraint(RuleDefinition ruleDefinition) {

        String res = "";
        List<ContainDefinition> contains = RuleDefinitionUtil.getContains(ruleDefinition);
        RuleDefinition templateId = null;
        if (contains != null && !contains.isEmpty()) {
            templateId = ContainDefinitionUtil.getTemplateIdElement(contains.get(0));
            if (templateId != null) {
                Attribute attr = templateId.getAttribute().get(0);
                String contextRule = (new OCLGenerator()).generateRuleToParentFromPathNF(attr);
                if (contextRule != null && !contextRule.isEmpty()) {
                    res = contextRule + "->size()>" + (contains.get(0).getMinimumMultiplicity() - 1);
                }
            }
        }
        return res;
    }

    @Override
    public String generateCommentConstraint(RuleDefinition ruleDefinition) {
        List<ContainDefinition> contains = RuleDefinitionUtil.getContains(ruleDefinition);
        RuleDefinition templateId = null;
        Integer minimumMultiplicity = null;
        if (contains != null && !contains.isEmpty()) {
            templateId = ContainDefinitionUtil.getTemplateIdElement(contains.get(0));
            minimumMultiplicity = contains.get(0).getMinimumMultiplicity();
            String templateName = ContainDefinitionUtil.getParentTemplateDefinition(contains.get(0)).getDisplayName();
            Attribute attribute = templateId.getAttribute().get(0);
            DParent dparent = AttributeUtil.getDParentOfTheParent(attribute);

            String parentPath = DPathExtractor.createPathFromDParent(dparent);

            String elementName = DPathExtractor.createPathFromDParent(dparent.getTail()).substring(1);
            return "In " + templateName + ", " + parentPath + " SHALL contain at least " +
                    OwnedRuleManager.convertMultiplicity(minimumMultiplicity) + " " + elementName;
        }
        return null;
    }

    @Override
    public String getProcessIdentifier() {
        return AnalyzerEnum.CONTAIN_MIN_MULTIPLICITY_PROCESS.getValue();
    }
}
