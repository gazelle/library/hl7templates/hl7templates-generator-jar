package net.ihe.gazelle.tempgen.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;


/**
 * 
 * @author Abderrazek Boufahja
 *
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
public @interface UsedByArtDecor { }