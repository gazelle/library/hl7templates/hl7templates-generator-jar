package net.ihe.gazelle.tempgen.nodes.check;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import net.ihe.gazelle.tempgen.nodes.check.NodeCirc.Color;

/**
 * 
 * @author Abderrazek Boufahja
 * not final because of extensions in tests
 *
 */
public class NodesChecker {
	
	public static Map<String, Set<String>> extractCircularCall(
			Map<String, Set<String>> mapOriginalNodes,
			Map<String, Set<String>> mapCircularNodesParam) {
		Map<String, Set<String>> mapCircularNodes = mapCircularNodesParam;
		if (mapCircularNodes == null) {
			mapCircularNodes = new HashMap<>();
		}
		List<NodeCirc> listConcernedTemplateNodes = createNodesForParsing(mapOriginalNodes);
		boolean completeListRegressed = true;
		while (completeListRegressed){
			completeListRegressed = false;
			for (NodeCirc node : listConcernedTemplateNodes) {
				boolean nodeRegressed = processNodeColor(node);
				completeListRegressed = completeListRegressed || nodeRegressed;
			}
		}
		List<NodeCirc> whiteOrGray = findNodesByColor(listConcernedTemplateNodes, Arrays.asList(Color.WHITE, Color.GRAY));
		for (NodeCirc node : whiteOrGray) {
			List<NodeCirc> whiteOrGrayChildren = findNodesByColor(node.getListChildNodes(), Arrays.asList(Color.WHITE, Color.GRAY));
			for (NodeCirc nodeChild : whiteOrGrayChildren) {
				if (mapCircularNodes.get(node.getNodeName()) == null){
					mapCircularNodes.put(node.getNodeName(), new TreeSet<String>());
				}
				mapCircularNodes.get(node.getNodeName()).add(nodeChild.getNodeName());
			}
		}
		return mapCircularNodes;
	}
	
	protected static boolean processNodeColor(NodeCirc node) {
		if (node.getColor() == Color.BLACK){
			return false;
		}
		List<NodeCirc> whiteOrGrayChild = findNodesByColor(node.getListChildNodes(), Arrays.asList(Color.WHITE, Color.GRAY));
		if (!whiteOrGrayChild.isEmpty()){
			node.setColor(Color.GRAY);
			return false;
		}
		else {
			node.setColor(Color.BLACK);
			return true;
		}
	}

	protected static List<NodeCirc> findNodesByColor(List<NodeCirc> ln, List<Color> colors){
		List<NodeCirc> res = new ArrayList<>();
		for (NodeCirc node : ln) {
			if (colors.contains(node.getColor())){
				res.add(node);
			}
		}
		return res;
	}
	
	protected static List<NodeCirc> createNodesForParsing(Map<String, Set<String>> mapIncludedTemplates){
		List<NodeCirc> ln = new ArrayList<>();
		Set<String> lt = extractListConcernedTemplates(mapIncludedTemplates);
		for (String string : lt) {
			NodeCirc node = new NodeCirc();
			node.setNodeName(string);
			ln.add(node);
		}
		if (mapIncludedTemplates != null) {
			for (Entry<String, Set<String>> entry : mapIncludedTemplates.entrySet()) {
				NodeCirc nodeParent = findNodeByName(ln, entry.getKey());
				if (nodeParent != null) {
					for (String childNodeName : entry.getValue()) {
						nodeParent.getListChildNodes().add(findNodeByName(ln, childNodeName));
					}
				}
			}
		}
		return ln;
	}

	protected static Set<String> extractListConcernedTemplates(Map<String, Set<String>> mapIncludedTemplates) {
		Set<String> settemp = new HashSet<>();
		if (mapIncludedTemplates != null) {
			for (Entry<String, Set<String>> entry : mapIncludedTemplates.entrySet()) {
				settemp.add(entry.getKey());
				settemp.addAll(entry.getValue());
			}
		}
		return settemp;
	}
	
	protected static NodeCirc findNodeByName(List<NodeCirc> ln, String name){
		if (ln != null) {
			for (NodeCirc node : ln) {
				if (node.getNodeName()!= null && node.getNodeName().equals(name)){
					return node;
				}
			}
		}
		return null;
	}

}
