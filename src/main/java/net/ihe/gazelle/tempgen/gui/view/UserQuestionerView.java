package net.ihe.gazelle.tempgen.gui.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Abderrazek Boufahja
 * not final because it is extended by tests
 */
public class UserQuestionerView {
	
	
	private static Logger log = LoggerFactory.getLogger(UserQuestionerView.class);
	
	public static boolean getAcceptanceFromUserForAnAction(String question) {
		 if (question == null || question.trim().equals("")) {
			log.info("The request to the viewer is not understandable !");
			 return false;
		 }
		 log.info("{} [y/n] (y) ", question);
		 BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		 try {
			String resp = br.readLine();
			return treatResp(resp);
		} catch (IOException e) {
			log.error("Problem to read the input from user.", e);
		}
		 
		 return false;
	}

	protected static boolean treatResp(String resp) {
		if (resp == null || resp.trim().equals("y") || resp.trim().equals("")) {
			return true;
		}
		else if (resp.trim().equals("n")) {
			return false;
		}
		else {
			log.info("your response is not understood, we take it for no.");
			return false;
		}
	}

}
