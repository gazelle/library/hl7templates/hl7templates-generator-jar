package net.ihe.gazelle.tempgen.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.xml.namespace.QName;

import net.ihe.gazelle.goc.xmm.OwnedRuleType;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.PackageableElement;

import net.ihe.gazelle.goc.uml.utils.CDAClassesMap;
import net.ihe.gazelle.goc.uml.utils.OwnedRuleUtil;
import net.ihe.gazelle.goc.uml.utils.UMLLoader;
import net.ihe.gazelle.goc.uml.utils.UMLPrimitiveType;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.OwnedRuleKind;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.tempapi.impl.AttributeProcessorImpl;
import net.ihe.gazelle.tempgen.attrs.analyzer.ATConstraintGenerator;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrAnalyzerEnum;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrClassCodeAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrContextConductionIndAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrContextControlCodeAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrDatatypeAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrDeterminerCodeAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrExtensionAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrIndependentIndAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrInstitutionSpecifiedAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrInversionIndAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrMediaTypeAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrMoodCodeAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrMultiplicityAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrNegationIndAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrNullFlavorAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrOperatorAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrOptionalityAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrProhibitedAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrQualifierAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrRepresentationAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrRootAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrTypeCodeAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrUnitAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrUseAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrValueAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrVocabAnalyzer;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrVocabEnumVocabAnalyzer;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempmodel.decor.dt.model.AttDatatype;
import net.ihe.gazelle.tempmodel.handler.util.AttributeCheckerUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AttributeUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ItemUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.VocabularyUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttributeAnalyzer extends AttributeProcessorImpl {
	
	private static final String DETERMINER_CODE = "determinerCode";

	private static final String NEGATION_IND = "negationInd";

	private static final String CLASS_CODE = "classCode";

	private static final String MOOD_CODE = "moodCode";

	private static final String OPERATOR2 = "operator";

	private static final String QUALIFIER2 = "qualifier";

	private static final String REPRESENTATION2 = "representation";

	private static final String TYPE_CODE = "typeCode";

	protected Attribute selectedAttribute;
	
	protected List<OwnedRule> listGeneratedOwnedRule = new ArrayList<>();
	
	protected PackagedElement currentPackagedElement;
	
	@Override
	public void process(Attribute attribute, Object... objects) {
		if (objects.length<1 || !(objects[0] instanceof PackagedElement)) {
			ProblemHandler.handleError("The method AttributeAnalyzer::process is called with a bad objects attribute", 
					AnalyzerEnum.RD_TEXT_PROCESS.getValue());
			return;
		}
		this.currentPackagedElement = (PackagedElement) objects[0];
		this.selectedAttribute = attribute;
		if (!(this.selectedAttribute.getParentObject() instanceof RuleDefinition)){
			ProblemHandler.handleAttributeError(this.selectedAttribute, "anyAttribute", null, 
					"The attribute attribute is not defined in the right place", 
					AttrAnalyzerEnum.ATTR_PROCESS.getValue());
			return;
		}
		processMultiplicity(this.selectedAttribute.getDatatype());
		super.process(this.selectedAttribute, objects);
		
//		-- already processed :
//		processValue(attribute.getValue());
//		processClassCode(attribute.getClassCode());
//		processContextConductionInd(attribute.isContextConductionInd());
//		processContextControlCode(attribute.getContextControlCode());
//		processDeterminerCode(attribute.getDeterminerCode());
//		processExtension(attribute.getExtension());
//		processIndependentInd(attribute.isIndependentInd());
//		processInstitutionSpecified(attribute.isInstitutionSpecified());
//		processInversionInd(attribute.isInversionInd());
//		processMediaType(attribute.getMediaType());
//		processMoodCode(attribute.getMoodCode());
//		processNegationInd(attribute.getNegationInd());
//		processNullFlavor(attribute.getNullFlavor());
//		processOperator(attribute.getOperator());
//		processQualifier(attribute.getQualifier());
//		processRepresentation(attribute.getRepresentation());
//		processRoot(attribute.getRoot());
//		processTypeCode(attribute.getTypeCode());
//		processUnit(attribute.getUnit());
//		processUse(attribute.getUse());
//		processProhibited(attribute.isProhibited());
//		processDatatype(attribute.getDatatype());
//		processMultiplicity(attribute.getDatatype());
//		processIsOptional(attribute.isIsOptional());
//		processVocabularys(attribute.getVocabulary());
//		processConstraints(attribute.getConstraint());
//		processItem(attribute.getItem());
	}

	private void processMultiplicity(QName datatype) {
		if (datatype != null && !"set_cs".equals(datatype.getLocalPart())){
			if (this.attributeIsCDAAttribute(this.selectedAttribute)) {
				Integer max = AttributeUtil.getUMLMaxAttribute(selectedAttribute);
				if (max == -1) {
					fulfillOwnedRuleForConstraintGenerator(new AttrMultiplicityAnalyzer(), this.currentPackagedElement, OwnedRuleKind.CARDINALITY);
				}
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, AttributeUtil.getBetterMatchingName(this.selectedAttribute), null, 
						"There is no " + AttributeUtil.getBetterMatchingName(this.selectedAttribute) + " attribute in the parent element", 
						AttrAnalyzerEnum.ATTR_MULTI_PROCESS.getValue());
			}
		}
	}

	/**
	 * The constraint is not processed because it cannot generate an error
	 */
	@Override
	public void processConstraints(List<FreeFormMarkupWithLanguage> constraint) {
//		if (constraint != null){
//			List<FreeFormMarkupWithLanguage> lff = AttrConstraintAnalyzer.extractListUtilConstraints(constraint);
//			if (lff != null && lff.size()>0){
//				for (FreeFormMarkupWithLanguage freeFormMarkupWithLanguage : lff) {
//					AttrConstraintAnalyzer aa = new AttrConstraintAnalyzer();
//					aa.setConstraint(freeFormMarkupWithLanguage);
//					fulfillOwnedRuleForConstraintGenerator(aa, this.currentPackagedElement);
//				}
//			}
//		}
	}

	@Override
	public void processVocabularys(List<Vocabulary> vocabulary) {
		if (VocabularyUtil.isVocabularyListUseful(vocabulary, 
				AttributeUtil.getParentTemplateDefinition(this.selectedAttribute).getParentObject().getParentObject())){
			if (this.attributeIsCDAAttribute(this.selectedAttribute)) {
				String attrName = AttributeUtil.getBetterMatchingName(this.selectedAttribute);
				String attrValue = AttributeUtil.getAttributeValue(selectedAttribute, attrName);
				if (attrValue!=null){
					return;
				}
				if (attrName == null){
					ProblemHandler.handleAttributeError(selectedAttribute, null, null, 
							"There is not name for this attribute, however a vocabulary is used", 
							AttrAnalyzerEnum.ATTR_VOCAB_PROCESS.getValue());
					return;
				}
				processAttributeName(attrName);
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, AttributeUtil.getBetterMatchingName(this.selectedAttribute), null, 
						"There is no " + AttributeUtil.getBetterMatchingName(this.selectedAttribute) + " attribute in the parent element", 
						AttrAnalyzerEnum.ATTR_VOCAB_PROCESS.getValue());
			}
		}
	}

	private void processAttributeName(String attrName) {
		String type = AttributeUtil.getUMLTypeName(selectedAttribute, attrName);
		if (type.equals(UMLPrimitiveType.STRING.getValue())){
			AttrVocabAnalyzer attrVocabAnalyzer = new AttrVocabAnalyzer();
			if (attrVocabAnalyzer.generateConstraintOwnedRuleType(this.selectedAttribute) != null){
				fulfillOwnedRuleForConstraintGenerator(attrVocabAnalyzer, this.currentPackagedElement, OwnedRuleKind.VOCABULARY,
						attrVocabAnalyzer.generateConstraintOwnedRuleType(this.selectedAttribute));
			}
		}
		else if (!UMLPrimitiveType.listUMLPrimitives().contains(type) && UMLLoader.isEnumeration(type)){
			AttrVocabEnumVocabAnalyzer attrVocabEnumVocabAnalyzer = new AttrVocabEnumVocabAnalyzer();
			if (attrVocabEnumVocabAnalyzer.generateConstraintOwnedRuleType(this.selectedAttribute) != null){
				fulfillOwnedRuleForConstraintGenerator(new AttrVocabEnumVocabAnalyzer(), this.currentPackagedElement, OwnedRuleKind.VOCABULARY,
						attrVocabEnumVocabAnalyzer.generateConstraintOwnedRuleType(this.selectedAttribute));
			}
		}
		else {
			ProblemHandler.handleAttributeError(selectedAttribute, selectedAttribute.getName(), null, 
					"There is a vocabulary, however the attribute is not of type String, nor enumeration", 
					AttrAnalyzerEnum.ATTR_VOCAB_PROCESS.getValue());
		}
	}

	/**
	 * item processing shall be the last step to be done
	 * @param item item
	 */
	@Override
	public void processItem(Item item) {
		Set<String> setItems = ItemUtil.extractRelatedItems(this.selectedAttribute);
		if (setItems != null){
			for (OwnedRule or : this.listGeneratedOwnedRule) {
				TamlHandler.handleAllTAML(setItems, or);
			}
		}
	}

	@Override
	public void processIsOptional(Boolean isOptional) {
		if (isOptional == null || !isOptional){
			List<String> listAttrNames = AttributeUtil.getListAttributesNames(selectedAttribute);
			for (String attrName : listAttrNames) {
				if (AttributeUtil.assertAttributeIsNotRequiredByDefault(this.selectedAttribute, attrName)){
					if (this.attributeIsCDAAttribute(this.selectedAttribute, attrName)) {
						AttrOptionalityAnalyzer aoa = new AttrOptionalityAnalyzer();
						aoa.setAttrName(attrName);
						fulfillOwnedRuleForConstraintGenerator(aoa, this.currentPackagedElement, OwnedRuleKind.CARDINALITY);
					}
					else {
						ProblemHandler.handleAttributeError(selectedAttribute, attrName, null, 
								"There is no " + attrName + " attribute in the parent element", 
								AttrAnalyzerEnum.ATTR_VOCAB_PROCESS.getValue());
					}
				}
			}
		}
	}

	@Override
	public void processDatatype(QName datatype) {
		if (datatype != null){
			AttDatatype dt = AttDatatype.getAttDatatypeByName(datatype.getLocalPart());
			if (dt != null && dt.isTestable())  {
				if (this.selectedAttribute.getName() == null){
					if (AttributeUtil.getBetterMatchingName(selectedAttribute) == null) {
						ProblemHandler.handleAttributeError(selectedAttribute, this.selectedAttribute.getName(), null, 
								"The attribute specify a datatype, but no name is specified", 
								AttrAnalyzerEnum.ATTR_DT_PROCESS.getValue());
					}
					return;
				}
				if (AttributeUtil.assertDatatypeIsForValidAttribute(selectedAttribute, selectedAttribute.getName()) &&
						AttributeUtil.assertDatatypeDifferent(this.selectedAttribute, this.selectedAttribute.getName() , dt)){
					if (AttributeCheckerUtil.verifyThatDatatypeIsSupported(this.selectedAttribute, dt)){
						AttrDatatypeAnalyzer ada = new AttrDatatypeAnalyzer();
						ada.setDatatype(dt);
						fulfillOwnedRuleForConstraintGenerator(ada, this.currentPackagedElement, OwnedRuleKind.DATATYPE);
					}
					else {
						ProblemHandler.handleAttributeError(selectedAttribute, this.selectedAttribute.getName(), null, 
								"The attribute specify a wrong type of datatype", 
								AttrAnalyzerEnum.ATTR_DT_PROCESS.getValue());
						return;
					}
				}
			}
		}
	}

	@Override
	public void processProhibited(Boolean prohibited) {
		if (prohibited != null && prohibited.equals(true)){
			if (this.selectedAttribute.getName() == null){
				ProblemHandler.handleAttributeError(selectedAttribute, "null", null, 
						"There is no name attribute in the Attribute specified, however prohibited is specified : " 
						+ AttributeUtil.getJavaPath(selectedAttribute, null), 
						AttrAnalyzerEnum.ATTR_PROHIBITED_PROCESS.getValue());
				return;
			}
			if (this.attributeIsCDAAttribute(this.selectedAttribute, this.selectedAttribute.getName())) {
				AttrProhibitedAnalyzer analyzer = new AttrProhibitedAnalyzer();
				analyzer.setAttrName(this.selectedAttribute.getName());
				fulfillOwnedRuleForConstraintGenerator(analyzer, this.currentPackagedElement, OwnedRuleKind.CARDINALITY);
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, this.selectedAttribute.getName(), null, 
						"There is no " + this.selectedAttribute.getName() + " attribute in the parent element", 
						AttrAnalyzerEnum.ATTR_VOCAB_PROCESS.getValue());
			}
		}
	}

	@Override
	public void processUse(String use) {
		if (use != null){
			if (this.attributeIsCDAAttribute(this.selectedAttribute, "use")) {
				if (AttributeCheckerUtil.isUseValueAcceptableForAttribute(this.selectedAttribute, use)) {
					fulfillOwnedRuleForConstraintGenerator(new AttrUseAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
				}
				else {
					ProblemHandler.handleAttributeError(selectedAttribute, "use", use, 
							"The use value specified is not a valid value", (new AttrUseAnalyzer()).getProcessIdentifier());
				}
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, "use", use, 
						"There is no use attribute in the parent element", 
						AttrAnalyzerEnum.ATTR_USE_PROCESS.getValue());
			}
		}
	}

	@Override
	public void processUnit(String unit) {
		if (unit != null){
			String type = RuleDefinitionUtil.getConstrainedUMLTypeName((RuleDefinition)this.selectedAttribute.getParentObject());
			if (type != null && UMLLoader.getPropertyTypeName(type + ".unit")!=null) {
				fulfillOwnedRuleForConstraintGenerator(new AttrUnitAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
			}
			else {
				ProblemHandler.handleAttributeError(this.selectedAttribute, "unit", unit, 
						"The unit attribute is not handled, it is defined in a wrong datatype", 
						AttrAnalyzerEnum.ATTR_UNIT_PROCESS.getValue());
			}
		}
	}

	@Override
	public void processTypeCode(String typeCode) {
		if (typeCode != null){
			if (DistinguisherFollower.verifyIfAttributeAndValueAreDistinguisher(selectedAttribute, TYPE_CODE, typeCode)){
				return;
			}
			if (this.attributeIsCDAAttributeAndValueExists(this.selectedAttribute, TYPE_CODE, typeCode)) {
				fulfillOwnedRuleForConstraintGenerator(new AttrTypeCodeAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, TYPE_CODE, typeCode, 
						"There is no typeCode attribute in the parent element, or the typeCode specified is wrong", 
						AttrAnalyzerEnum.ATTR_TYPE_CODE_PROCESS.getValue());
			}
		}
	}
	
	public boolean attributeIsCDAAttribute(Attribute attribute) {
		return AttributeUtil.getUMLTypeName(attribute, null) != null;
	}
	
	public boolean attributeIsCDAAttribute(Attribute attribute, String attrName) {
		return AttributeUtil.getUMLTypeName(attribute, attrName) != null;
	}

	/**
	 * if type unknown, return false
	 * if type known, and type is not enum, return true
	 * id type known and type is enum, return if value is port of the enum
	 * @param attribute attribute
	 * @param attrName attribute name
	 * @param attrValue attribute value
	 * @return boolean
	 */
	public boolean attributeIsCDAAttributeAndValueExists(Attribute attribute, String attrName, String attrValue) {
		String type = AttributeUtil.getUMLTypeName(attribute, attrName);
		if (type != null) {
			PackageableElement cdaenum = CDAClassesMap.lookForPackageableElementFromName(type);
			if (!(cdaenum instanceof Enumeration)){
				return true;
			}
			else {
				return UMLLoader.checkIfEnumerationContainsValue(type, attrValue);
			}
		}
		return false;
	}
	
	@Override
	public void processRoot(String root) {
		if (root != null){
			if (DistinguisherFollower.verifyIfAttributeAndValueAreDistinguisher(selectedAttribute, 
					"root", root)){
				return;
			}
			String type = RuleDefinitionUtil.getConstrainedUMLTypeName((RuleDefinition)this.selectedAttribute.getParentObject());
			if (type != null && UMLLoader.getPropertyTypeName(type + ".root")!=null){
				fulfillOwnedRuleForConstraintGenerator(new AttrRootAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
			}
			else {
				ProblemHandler.handleAttributeError(this.selectedAttribute, "root", root, 
						"The root attribute is not handled, it is defined in a wrong datatype (" + type + ")", 
						AttrAnalyzerEnum.ATTR_ROOT_PROCESS.getValue());
			}
		}
	}

	@Override
	public void processRepresentation(String representation) {
		if (representation != null){
			if (DistinguisherFollower.verifyIfAttributeAndValueAreDistinguisher(selectedAttribute, 
					REPRESENTATION2, representation)){
				return;
			}
			if (this.attributeIsCDAAttributeAndValueExists(this.selectedAttribute, REPRESENTATION2, representation)) {
				fulfillOwnedRuleForConstraintGenerator(new AttrRepresentationAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, REPRESENTATION2, representation, 
						"There is no representation attribute in the parent element, or representation value is wrong", 
						AttrAnalyzerEnum.ATTR_REPRESENTATION_PROCESS.getValue());
			}
		}
	}

	@Override
	public void processQualifier(String qualifier) {
		if (qualifier != null){
			if (DistinguisherFollower.verifyIfAttributeAndValueAreDistinguisher(selectedAttribute, 
					QUALIFIER2, qualifier)){
				return;
			}
			if (this.attributeIsCDAAttribute(this.selectedAttribute, QUALIFIER2)) {
				fulfillOwnedRuleForConstraintGenerator(new AttrQualifierAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, QUALIFIER2, qualifier, 
						"There is no qualifier attribute in the parent element", 
						AttrAnalyzerEnum.ATTR_QUALIFIER_PROCESS.getValue());
			}
		}
	}

	@Override
	public void processOperator(String operator) {
		if (operator != null){
			if (DistinguisherFollower.verifyIfAttributeAndValueAreDistinguisher(selectedAttribute, 
					OPERATOR2, operator)){
				return;
			}
			if (this.attributeIsCDAAttributeAndValueExists(this.selectedAttribute, OPERATOR2, operator)) {
				fulfillOwnedRuleForConstraintGenerator(new AttrOperatorAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, OPERATOR2, operator, 
						"There is no operator attribute in the parent element, or the operator value is wrong", 
						AttrAnalyzerEnum.ATTR_OPERATOR_PROCESS.getValue());
			}
		}
	}

	@Override
	public void processNullFlavor(String nullFlavor) {
		if (nullFlavor != null){
			if (this.attributeIsCDAAttributeAndValueExists(this.selectedAttribute, "nullFlavor", nullFlavor)) {
				fulfillOwnedRuleForConstraintGenerator(new AttrNullFlavorAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, "nullFlavor", nullFlavor, 
						"There is no nullFlavor attribute in the parent element, or the nullFlavor value is wrong", 
						AttrAnalyzerEnum.ATTR_NULLFLAVOR_PROCESS.getValue());
			}
		}
	}

	@Override
	public void processNegationInd(String negationInd) {
		if (negationInd != null){
			boolean acceptableValue = "true".equals(negationInd) || "false".equals(negationInd);
			if (!acceptableValue){
				ProblemHandler.handleAttributeError(selectedAttribute, NEGATION_IND, negationInd, 
						"The negationInd attribtue shall have the value true or false, the actual one is : " + negationInd, 
						AttrAnalyzerEnum.ATTR_NEGATION_IND_PROCESS.getValue());
				return;
			}
			if (this.attributeIsCDAAttribute(this.selectedAttribute, NEGATION_IND)) {
				fulfillOwnedRuleForConstraintGenerator(new AttrNegationIndAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, NEGATION_IND, negationInd, 
						"There is no negationInd attribute in the parent element", 
						AttrAnalyzerEnum.ATTR_NEGATION_IND_PROCESS.getValue());
			}
		}
	}

	@Override
	public void processMoodCode(String moodCode) {
		if (moodCode != null){
			if (DistinguisherFollower.verifyIfAttributeAndValueAreDistinguisher(selectedAttribute, 
					MOOD_CODE, moodCode)){
				return;
			}
			if (this.attributeIsCDAAttributeAndValueExists(this.selectedAttribute, MOOD_CODE, moodCode)) {
				fulfillOwnedRuleForConstraintGenerator(new AttrMoodCodeAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, MOOD_CODE, moodCode, 
						"There is no moodCode attribute in the parent element", 
						AttrAnalyzerEnum.ATTR_MOODCODE_PROCESS.getValue());
			}
		}
	}

	@Override
	public void processMediaType(String mediaType) {
		if (mediaType != null){
			if (this.attributeIsCDAAttribute(this.selectedAttribute, "mediaType")) {
				fulfillOwnedRuleForConstraintGenerator(new AttrMediaTypeAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, "mediaType", mediaType, 
						"There is no mediaType attribute in the parent element", 
						AttrAnalyzerEnum.ATTR_MEDIA_TYPE_PROCESS.getValue());
			}
		}
		
	}

	@Override
	public void processInversionInd(Boolean inversionInd) {
		if (inversionInd != null){
			if (this.attributeIsCDAAttribute(this.selectedAttribute, "inversionInd")) {
				fulfillOwnedRuleForConstraintGenerator(new AttrInversionIndAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, "inversionInd", String.valueOf(inversionInd), 
						"There is no inversionInd attribute in the parent element", 
						AttrAnalyzerEnum.ATTR_INVERSION_IND_PROCESS.getValue());
			}
		}
	}

	@Override
	public void processInstitutionSpecified(Boolean institutionSpecified) {
		if (institutionSpecified != null){
			if (this.attributeIsCDAAttribute(this.selectedAttribute, "institutionSpecified")) {
				fulfillOwnedRuleForConstraintGenerator(new AttrInstitutionSpecifiedAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, "institutionSpecified", String.valueOf(institutionSpecified), 
						"There is no institutionSpecified attribute in the parent element", 
						AttrAnalyzerEnum.ATTR_INSTRITUTION_SPECIFIED_PROCESS.getValue());
			}
		}
	}

	@Override
	public void processIndependentInd(Boolean independentInd) {
		if (independentInd != null){
			if (this.attributeIsCDAAttribute(this.selectedAttribute, "independentInd")) {
				fulfillOwnedRuleForConstraintGenerator(new AttrIndependentIndAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, "independentInd", String.valueOf(independentInd), 
						"There is no independentInd attribute in the parent element", 
						AttrAnalyzerEnum.ATTR_INDEPENDENTIND_PROCESS.getValue());
			}
		}
	}

	@Override
	public void processExtension(String extension) {
		if (extension != null){
			if (DistinguisherFollower.verifyIfAttributeAndValueAreDistinguisher(selectedAttribute, 
					"extension", extension)){
				return;
			}
			String type = RuleDefinitionUtil.getConstrainedUMLTypeName((RuleDefinition)this.selectedAttribute.getParentObject());
			if (type != null && (type.equals("II")|| type.equals("POCDMT000040InfrastructureRootTypeId"))){
				fulfillOwnedRuleForConstraintGenerator(new AttrExtensionAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
			}
			else {
				ProblemHandler.handleAttributeError(this.selectedAttribute, "extension", extension, 
						"The extension attribute is not handled, it is defined in a wrong datatype (" + type + ")", 
						AttrAnalyzerEnum.ATTR_EXTENSION_PROCESS.getValue());
			}
		}
	}

	@Override
	public void processDeterminerCode(String determinerCode) {
		if (determinerCode != null){
			if (DistinguisherFollower.verifyIfAttributeAndValueAreDistinguisher(selectedAttribute, 
					DETERMINER_CODE, determinerCode)){
				return;
			}
			if (this.attributeIsCDAAttributeAndValueExists(this.selectedAttribute, DETERMINER_CODE, determinerCode)) {
				fulfillOwnedRuleForConstraintGenerator(new AttrDeterminerCodeAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, DETERMINER_CODE, determinerCode, 
						"There is no determinerCode attribute in the parent element, or the value of determinerCode is wrong", 
						AttrAnalyzerEnum.ATTR_DETERMINERCODE_PROCESS.getValue());
			}
		}
	}

	@Override
	public void processContextControlCode(String contextControlCode) {
		if (contextControlCode != null){
			if (this.attributeIsCDAAttributeAndValueExists(this.selectedAttribute, 
					"contextControlCode", contextControlCode)) {
				fulfillOwnedRuleForConstraintGenerator(new AttrContextControlCodeAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, "contextControlCode", contextControlCode, 
						"There is no contextControlCode attribute in the parent element, or the value is wrong", 
						AttrAnalyzerEnum.ATTR_CONTEXTCONTROLCODE_PROCESS.getValue());
			}
		}
		
	}

	@Override
	public void processContextConductionInd(Boolean contextConductionInd) {
		if (contextConductionInd != null){
			if (this.attributeIsCDAAttribute(this.selectedAttribute, "contextConductionInd")) {
				fulfillOwnedRuleForConstraintGenerator(new AttrContextConductionIndAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, "contextConductionInd", String.valueOf(contextConductionInd), 
						"There is no contextConductionInd attribute in the parent element", 
						AttrAnalyzerEnum.ATTR_CONTEXTCONDUCTIONIND_PROCESS.getValue());
			}
		}
	}

	@Override
	public void processClassCode(String classCode) {
		if (classCode != null){
			if (DistinguisherFollower.verifyIfAttributeAndValueAreDistinguisher(selectedAttribute, 
					CLASS_CODE, classCode)){
				return;
			}
			if (this.attributeIsCDAAttributeAndValueExists(this.selectedAttribute, CLASS_CODE, classCode)) {
				fulfillOwnedRuleForConstraintGenerator(new AttrClassCodeAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, CLASS_CODE, classCode, 
						"There is no classCode attribute in the parent element, or the value is wrong", 
						AttrAnalyzerEnum.ATTR_CLASSCODE_PROCESS.getValue());
			}
		}
	}

	@Override
	public void processValue(String value) {
		if (value != null){
			if (DistinguisherFollower.verifyIfAttributeAndValueAreDistinguisher(selectedAttribute, 
					this.selectedAttribute.getName(), value)){
				return;
			}
			if (this.selectedAttribute.getName() != null){
				if (this.attributeIsCDAAttributeAndValueExists(this.selectedAttribute, 
						this.selectedAttribute.getName(), value)) {
					fulfillOwnedRuleForConstraintGenerator(new AttrValueAnalyzer(), this.currentPackagedElement, OwnedRuleKind.FIXEDVAL);
				}
				else {
					ProblemHandler.handleAttributeError(selectedAttribute, this.selectedAttribute.getName(), value, 
							"The Attribute has a name that does not match any attribute name of the parent element", 
							AttrAnalyzerEnum.ATTR_VALUE_PROCESS.getValue());
				}
			}
			else {
				ProblemHandler.handleAttributeError(selectedAttribute, "notSpecified", value, 
					"The Attribute has a value but not a name", AttrAnalyzerEnum.ATTR_VALUE_PROCESS.getValue());
			}
		}
		
	}
	
	private void fulfillOwnedRuleForConstraintGenerator(ATConstraintGenerator constraintGenerator, PackagedElement pe, OwnedRuleKind ownedRuleKind){
		fulfillOwnedRuleForConstraintGenerator(constraintGenerator, pe, ownedRuleKind, null);
	}

	private void fulfillOwnedRuleForConstraintGenerator(ATConstraintGenerator constraintGenerator, PackagedElement pe, OwnedRuleKind ownedRuleKind, OwnedRuleType ownedRuleType){
		OwnedRule ownedRule = OwnedRuleUtil.initOwnedRule();
		ownedRule.setConstrainedElement(pe.getId());
		String specification = constraintGenerator.generateOCLConstraint(this.selectedAttribute);
		String comment = constraintGenerator.generateCommentConstraint(this.selectedAttribute) +
				ItemCommentGenerator.generateItemComment(selectedAttribute);
		ownedRule.getSpecification().setBody(specification);
		ownedRule.getOwnedComment().setBody(comment);
		ownedRule.setOwnedRuleKind(ownedRuleKind);
		if (ownedRuleType != null){
			ownedRule.setOwnedRuleType(ownedRuleType);
		}
		listGeneratedOwnedRule.add(ownedRule);
		pe.getOwnedRule().add(ownedRule);
	}

}
