package net.ihe.gazelle.tempgen.action;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;

public interface AssertVerifier {
	
	boolean acceptToUseAssert(Assert assertion);

}
