package net.ihe.gazelle.tempgen.action;	

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.goc.uml.utils.OwnedRuleUtil;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.OwnedRuleKind;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.handler.util.PropertyProblemUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Property;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.PropertyUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class PropertyAnalyzer implements Processor<List<Property>> {
	
	
	private static final String AND = " and ";

	private static Logger log = LoggerFactory.getLogger(PropertyAnalyzer.class);
	
	private List<Property> listSelectedProperty;
	
	public List<Property> getListSelectedProperty() {
		return listSelectedProperty;
	}

	public void setListSelectedProperty(List<Property> listSelectedProperty) {
		this.listSelectedProperty = listSelectedProperty;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void process(List<Property> properties, Object... objects) {
		if (objects == null || objects.length < 2 || 
				!(objects[0] instanceof PackagedElement) || !(objects[1] instanceof List)) {
			log.error("problem to process list properties ! (PropertyAnalyzer::process)");
			return;
		}
		PackagedElement pe = (PackagedElement) objects[0];
		List<OwnedRule> listGeneratedOwnedRule = (List<OwnedRule>)objects[1];
		this.listSelectedProperty = extractValidProperties(properties);
		if (this.listSelectedProperty!= null && !this.listSelectedProperty.isEmpty()){
			RuleDefinition constrainedRD = this.listSelectedProperty.get(0).getParentObject();
			OwnedRule ownedRule = OwnedRuleUtil.initOwnedRule();
			ownedRule.setConstrainedElement(pe.getId());
			String specification = this.generateOCLConstraint();
			String comment = this.generateCommentConstraint() + ItemCommentGenerator.generateItemComment(constrainedRD);
			OwnedRuleKind ork = this.generateOwnedRuleKind();
			ownedRule.getSpecification().setBody(specification);
			ownedRule.getOwnedComment().setBody(comment);
			ownedRule.setOwnedRuleKind(ork);
			pe.getOwnedRule().add(ownedRule);
			listGeneratedOwnedRule.add(ownedRule);
		}
	}

	private OwnedRuleKind generateOwnedRuleKind() {
		for (Property prop : this.listSelectedProperty) {
			if (PropertyUtil.isUnitAttributePresent(prop) || PropertyUtil.isCurrencyAttributePresent(prop) || PropertyUtil.isValueAttributePresent(prop))  {
				return OwnedRuleKind.FIXEDVAL;
			}
			if (PropertyUtil.isMinIncludeAttributePresent(prop) || PropertyUtil.isMaxIncludeAttributePresent(prop)) {
				return OwnedRuleKind.CARDINALITY;
			}
			if (PropertyUtil.isMinLengthAttributePresent(prop) || PropertyUtil.isMaxLengthAttributePresent(prop) || 
					PropertyUtil.isFractionDigitsAttributePresent(prop)) {
				return OwnedRuleKind.LENGTH;
			}
		}
		return null;
	}

	protected String generateOCLConstraint() {
		RuleDefinition constrainedRD = this.listSelectedProperty.get(0).getParentObject();
		RuleDefinition tmp = new RuleDefinition();
		tmp.setParentObject(constrainedRD);
		String contextRule = (new OCLGenerator()).generateRuleToParentFromPath(tmp);
		StringBuilder sb = new StringBuilder(contextRule + OCLCleaner.generateRejectOcl(true) + "->forAll(");
		int i = 0;
		for (Property prop : this.listSelectedProperty) {
			if (i>0){
				sb.append(" or ");
			}
			sb.append("(" + extractOCLFromProp(prop) + ")");
			i++;
		}
		sb.append(")");
		return sb.toString();
	}

	protected String extractOCLFromProp(Property prop) {
		String res = "";
		if (PropertyUtil.isUnitAttributePresent(prop)) {
			res = processUnit(prop, res);
		}
		if (PropertyUtil.isCurrencyAttributePresent(prop)) {
			res = processCurrency(prop, res);
		}
		if (PropertyUtil.isMinIncludeAttributePresent(prop)) {
			res = processMinInclude(prop, res);
		}
		if (PropertyUtil.isMaxIncludeAttributePresent(prop)) {
			res = processMaxInclude(prop, res);
		}
		if (PropertyUtil.isMinLengthAttributePresent(prop)) {
			res = processMinLength(prop, res);
		}
		if (PropertyUtil.isMaxLengthAttributePresent(prop)) {
			res = processMaxLength(prop, res);
		}
		if (PropertyUtil.isValueAttributePresent(prop)) {
			res = processValue(prop, res);
		}
		if (PropertyUtil.isFractionDigitsAttributePresent(prop)){
			res = processFractionDigits(prop, res);
		}
		return res;
	}

	protected String processFractionDigits(Property prop, String resParam) {
		String res = resParam;
		Integer digitNumber = PropertyUtil.extractDigitNumber(prop.getFractionDigits());
		boolean digistStrict = prop.getFractionDigits().contains("!");
		if (digitNumber != null){
			if (!res.isEmpty()) {
				res = res + AND;
			}
			res = res + "(not value.oclIsUndefined()) and ";
			if (digitNumber != 0){
				if (digistStrict){
					res = res + "CommonOperationsStatic::extractNumberDigits(value)=" + digitNumber;
				}
				else {
					res = res + "CommonOperationsStatic::extractNumberDigits(value)>=" + digitNumber;
				}
			}
			else {
				res = res + "CommonOperationsStatic::extractFractionDigits(value)='0'";
			}
		}
		return res;
	}

	protected String processValue(Property prop, String resParam) {
		String res = resParam;
		if (!res.isEmpty()) {
			res = res + AND;
		}
		res = res + "(not value.oclIsUndefined()) and value='" + prop.getValue() + "'";
		return res;
	}

	protected String processMaxLength(Property prop, String resParam) {
		String res = resParam;
		if (!res.isEmpty()) {
			res = res + AND;
		}
		res = res + "getListStringValues()->forAll(st : String | st.size()<=" + prop.getMaxLength() + ")";
		return res;
	}

	protected String processMinLength(Property prop, String resParam) {
		String res = resParam;
		if (!res.isEmpty()) {
			res = res + AND;
		}
		res = res + "getListStringValues()->forAll(st : String | st.size()>=" + prop.getMinLength() + ")";
		return res;
	}

	protected String processMaxInclude(Property prop, String resParam) {
		String res = resParam;
		if (!res.isEmpty()) {
			res = res + AND;
		}
		res = res + "(not value.oclIsUndefined()) and CommonOperationsStatic::isLowerOrEqual(value, '" + prop.getMaxInclude() + "')";
		return res;
	}

	protected String processMinInclude(Property prop, String resParam) {
		String res = resParam;
		if (!res.isEmpty()) {
			res = res + AND;
		}
		res = res + "(not value.oclIsUndefined()) and CommonOperationsStatic::isGreaterOrEqual(value, '" + prop.getMinInclude() + "')";
		return res;
	}

	protected String processCurrency(Property prop, String resParam) {
		String res = resParam;
		if (!res.isEmpty()) {
			res = res + AND;
		}
		res = res + "(not currency.oclIsUndefined()) and currency='" + prop.getCurrency() + "'";
		return res;
	}

	protected String processUnit(Property prop, String resParam) {
		String res = resParam;
		if (!res.isEmpty()) {
			res = res + AND;
		}
		res = res + "(not unit.oclIsUndefined()) and unit='" + prop.getUnit() + "'";
		return res;
	}

	protected String generateCommentConstraint() {
		RuleDefinition constrainedRD = this.listSelectedProperty.get(0).getParentObject();
		String templateName = RuleDefinitionUtil.getParentTemplateDefinition(constrainedRD).getDisplayName();
		DParent dparent = RuleDefinitionUtil.getDParentOfRuleDefinition(constrainedRD);
		String path = DPathExtractor.createPathFromDParent(dparent);
		StringBuilder sb = new StringBuilder("In " + templateName + ", in " + path + ", ");
		int i = 0;
		for (Property prop : this.listSelectedProperty) {
			if (i>0){
				sb.append(" OR ");
			}
			int size = this.listSelectedProperty.size();
			if (size>1){
				sb.append("(" + generateDescrForProp(prop) + ")");
			}
			else {
				sb.append(generateDescrForProp(prop));
			}
			i++;
		}
		return sb.toString();
	}

	protected String generateDescrForProp(Property prop) {
		String res = "";
		int i = 0;
		if (PropertyUtil.isUnitAttributePresent(prop)) {
			if (i>0){
				res = res + AND;
			}
			res = res + "the unit attribute shall have the value " + prop.getUnit();
			i++;
		}
		if (PropertyUtil.isCurrencyAttributePresent(prop)) {
			if (i>0){
				res = res + AND;
			}
			res = res + "the currency attribute shall have the value " + prop.getCurrency();
			i++;
		}
		if (PropertyUtil.isMinIncludeAttributePresent(prop)) {
			if (i>0){
				res = res + AND;
			}
			res = res + "the value attribute shall be bigger than " + prop.getMinInclude();
			i++;
		}
		if (PropertyUtil.isMaxIncludeAttributePresent(prop)) {
			if (i>0){
				res = res + AND;
			}
			res = res + "the value attribute shall be lower than " + prop.getMaxInclude();
			i++;
		}
		if (PropertyUtil.isMinLengthAttributePresent(prop)) {
			if (i>0){
				res = res + AND;
			}
			res = res + "the string value shall a length lower than " + prop.getMinLength();
			i++;
		}
		if (PropertyUtil.isMaxLengthAttributePresent(prop)) {
			if (i>0){
				res = res + AND;
			}
			res = res + "the string value shall a length bigger than " + prop.getMaxLength();
			i++;
		}
		if (PropertyUtil.isValueAttributePresent(prop)) {
			if (i>0){
				res = res + AND;
			}
			res = res + "the value attribute shall have the value " + prop.getValue();
			i++;
		}
		if (PropertyUtil.isFractionDigitsAttributePresent(prop)){
			if (i>0){
				res = res + AND;
			}
			Integer digitNumber = PropertyUtil.extractDigitNumber(prop.getFractionDigits());
			boolean digistStrict = prop.getFractionDigits().contains("!");
			if (digistStrict){
				res = res + "the value attribute shall have exactly " + digitNumber + " digits";
			}
			else {
				res = res + "the value attribute shall have at least " + digitNumber + " digits";
			}
		}
		return res;
	}

	protected List<Property> extractValidProperties(List<Property> properties) {
		List<Property> lvp = new ArrayList<>();
		if (properties != null && !properties.isEmpty()) {
			RuleDefinition constrainedRD = properties.get(0).getParentObject();
			for (Property property : properties) {
				if (PropertyProblemUtil.verifyIfPropertyContainValidCombination(property)) {
					if (PropertyProblemUtil.verifyIfPropertyDeclaredForTheRightElement(property)) {
						lvp.add(property);
					}
					else {
						ProblemHandler.handleRuleDefinitionError(constrainedRD, "A property declared not for a right Element", 
								AnalyzerEnum.RD_PROP_PROCESS.getValue());
					}
				}
				else {
					ProblemHandler.handleRuleDefinitionError(constrainedRD, "A property does not contain a valid combination", 
							AnalyzerEnum.RD_PROP_PROCESS.getValue());
				}
			}
		}
		return lvp;
	}

}
