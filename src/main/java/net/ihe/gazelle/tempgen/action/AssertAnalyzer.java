package net.ihe.gazelle.tempgen.action;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import net.ihe.gazelle.goc.uml.utils.OwnedRuleUtil;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.OwnedRuleKind;
import net.ihe.gazelle.goc.xmm.OwnedRuleType;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.tempapi.impl.AssertProcessorImpl;
import net.ihe.gazelle.tempapi.impl.RuleDefinitionProcessorImpl;
import net.ihe.gazelle.tempapi.interfaces.RuleDefinitionProcessor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempapi.visitor.RulesVisitor;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AssertRole;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Let;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AssertUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AssertAnalyzer extends AssertProcessorImpl {
	
	private static final String CDA = "cda";

	private static final String HL7 = "hl7";
	
	protected Assert currentAssert;
	
	protected PackagedElement currentPackagedElement;
	
	protected List<OwnedRule> listGeneratedOwnedRule;
	
	@SuppressWarnings("unchecked")
	@Override
	public void process(Assert t, Object... objects) {
		this.currentAssert = t;
		this.flattenAssert(currentAssert, 0);
		this.currentPackagedElement = (PackagedElement) objects[0];
		if (objects.length>1) {
			this.listGeneratedOwnedRule = (List<OwnedRule>) objects[1];
		}
		if (!GenerationProperties.ignoreAssertionGeneration && verifyThatAssertIsProcessable(this.currentAssert)){
			super.process(t, objects);
		}
	}
	
	@Override
	public void processTest(String test) {
		if (test != null) {
			if (GenerationProperties.userSupportForAssertClass != null) {
				if (GenerationProperties.userSupportForAssertClass.acceptToUseAssert(this.currentAssert)) {
					this.fulfillOwnedRuleForConstraintGenerator();
				}
			}
			else {
				this.fulfillOwnedRuleForConstraintGenerator();
			}
		}
	}

	/**
	 * 
	 * @param currentAssert2 an assert
	 * @param iParam : an index to search for circular let references
	 */
	public void flattenAssert(Assert currentAssert2, int iParam) {
		int i = iParam;
		if (currentAssert2 != null && currentAssert2.getTest() != null && currentAssert2.getParentObject() != null) {
			if (i>5) {
				ProblemHandler.handleError("there are a circular let references used by the assert : " + currentAssert2.getTest(), 
						AnalyzerEnum.RD_ASSSERT_PROCESS.getValue());
				return;
			}
			if (currentAssert2.getTestFlatten() == null) {
				currentAssert2.setTestFlatten(currentAssert2.getTest());
			}
			if (currentAssert2.getParentObject() instanceof RuleDefinition) {
				RuleDefinition parent = (RuleDefinition)currentAssert2.getParentObject();
				List<Let> llets = RuleDefinitionUtil.getLets(parent);
				if (llets != null) {
					String oldTestFlattenValue = currentAssert2.getTestFlatten();
					for (Let let : llets) {
						if (let.getName() != null && let.getValue() != null) {
							currentAssert2.setTestFlatten(currentAssert2.getTestFlatten().replace("$" + let.getName(), let.getValue()));
						}
						else {
							ProblemHandler.handleRuleDefinitionError(parent, "there are a let element with a missing name/value attribute", 
									AnalyzerEnum.RD_ASSSERT_PROCESS.getValue());
						}
					}
					if (!oldTestFlattenValue.equals(currentAssert2.getTestFlatten())) {
						flattenAssert(currentAssert2, ++i);
					}
				}
			}
			else if (currentAssert2.getParentObject() instanceof TemplateDefinition) {
				ProblemHandler.handleError("The flattened TemplateDefinition should not contains an assert element : " + 
						((TemplateDefinition)currentAssert2.getParentObject()).getId(), 
						AnalyzerEnum.RD_ASSSERT_PROCESS.getValue());
			}
		}
		
	}

	protected boolean verifyThatAssertIsProcessable(Assert currentAssert2) {
		if (currentAssert2 != null && currentAssert2.getTestFlatten() != null) {
			if (GenerationProperties.ignoreXPathAxes) {
				List<String> listProhibited = Arrays.asList("ancestor::", "ancestor-or-self::", "following::", "following-sibling::", "parent::", 
						"preceding::", "preceding-sibling::");
				for (String proh : listProhibited) {
					if (currentAssert2.getTestFlatten().contains(proh)) {
						ProblemHandler.handleError("The flattened assert contrains reference to prohibited axis : '" + proh + 
								"', for assert : " + currentAssert2.getTest(), AnalyzerEnum.RD_ASSSERT_PROCESS.getValue());
						return false;
					}
				}
			}
			Set<String> prefixes = getListNamespacesPrefix(AssertUtil.getParentTemplateDefinition(currentAssert2).getParentObject());
			for (String prefix : prefixes) {
				if (!prefix.equals(HL7) && !prefix.equals(CDA) && currentAssert2.getTestFlatten().contains(prefix + ":")) {
					ProblemHandler.handleError("The flattened assert contrains reference to unknown prefix : '" + prefix + 
							"', for assert : " + currentAssert2.getTest(), AnalyzerEnum.RD_ASSSERT_PROCESS.getValue());
					return false;
				}
			}
			return true;
		}
		ProblemHandler.handleError("The flattened assert is null !", AnalyzerEnum.RD_ASSSERT_PROCESS.getValue());
		return false;
	}
	
	protected void fulfillOwnedRuleForConstraintGenerator(){
		OwnedRule ownedRule = OwnedRuleUtil.initOwnedRule();
		ownedRule.setConstrainedElement(this.currentPackagedElement.getId());
		String specification = this.generateOCLConstraint();
		String comment = this.generateCommentConstraint();
		ownedRule.getSpecification().setBody(specification);
		ownedRule.getOwnedComment().setBody(comment);
		ownedRule.setOwnedRuleKind(OwnedRuleKind.CONTEXT);
		ownedRule.setOwnedRuleType(extractOwnedRuleType());
		this.currentPackagedElement.getOwnedRule().add(ownedRule);
		if (this.listGeneratedOwnedRule != null) {
			this.listGeneratedOwnedRule.add(ownedRule);
		}
	}

	protected OwnedRuleType extractOwnedRuleType() {
		if (this.currentAssert != null && this.currentAssert.getRole() != null) {
			switch (this.currentAssert.getRole()) {
			case ERROR:
			case FATAL:
				return OwnedRuleType.ERROR;
			case WARNING:
				return OwnedRuleType.WARNING;
			case HINT:
				return OwnedRuleType.INFO;
			default:
				break;
			}
		}
		return null;
	}

	protected String generateCommentConstraint() {
		String templateName = null;
		String path = null;
		String itemComment = "";
		if (this.currentAssert.getParentObject() instanceof RuleDefinition) {
			RuleDefinition constrainedRD = (RuleDefinition)this.currentAssert.getParentObject();
			templateName = RuleDefinitionUtil.getParentTemplateDefinition(constrainedRD).getDisplayName();
			DParent dparent = RuleDefinitionUtil.getDParentOfRuleDefinition(constrainedRD);
			path = DPathExtractor.createPathFromDParent(dparent);
			itemComment = ItemCommentGenerator.generateItemComment(constrainedRD);
		}
		else if (this.currentAssert.getParentObject() instanceof TemplateDefinition) {
			TemplateDefinition parent = (TemplateDefinition)this.currentAssert.getParentObject();
			templateName = parent.getDisplayName();
			DParent dp= RuleDefinitionUtil.getDParentOfRuleDefinition(TemplateDefinitionUtil.getFirstElement(parent));
			path = DPathExtractor.createPathFromDParent(dp);
			itemComment = ItemCommentGenerator.generateItemComment(parent);
		}
		
		String comment = AssertUtil.getStringValue(this.currentAssert);
		String tail;
		if (StringUtils.trimToNull(comment) != null) {
			tail = comment;
		}
		else {
			tail = this.currentAssert.getTest();
		}
		return "In " + templateName + ", the element defined by " + path + " shall verify this requirement : " + tail + itemComment;
	}

	protected String generateOCLConstraint() {
		String res = "";
		if (this.currentAssert.getParentObject() instanceof RuleDefinition) {
			RuleDefinition parent = (RuleDefinition)this.currentAssert.getParentObject();
			String xpath = this.currentAssert.getTestFlatten().replace("hl7:", "cda:");
			xpath = xpath.replace("\\", "\\\\").replace("'", "\\\"");
			String contextRule = (new OCLGenerator()).generateRuleToParentFromPath(parent);
			if (parent.getParentObject() instanceof TemplateDefinition) {
				res = "CommonOperationsStatic::validateByXPATHV2(self, '" + this.getFormatedXPATH(xpath) + "')";
			}
			else if ((parent.getParentObject() instanceof RuleDefinition) ||
						(parent.getParentObject() instanceof ChoiceDefinition && 
							((ChoiceDefinition)parent.getParentObject()).getParentObject() instanceof RuleDefinition
						)
					){
				res = contextRule + "." + RuleDefinitionUtil.getRealNameOfRuleDefinition(parent) + 
						OCLGenerator.getExtraSelectionIfNeeded(parent) + OCLCleaner.generateRejectOcl(true) + 
						"->forAll(aa | " + 
						"CommonOperationsStatic::validateByXPATHV2(aa, '" + this.getFormatedXPATH(xpath) + "')" + 
						" )";
			}
			ImportElementHandler.handleAddingElement("CommonOperationsStatic");	
		}
		return res;
	}
	
	public static Set<String> getListNamespacesPrefix(Rules t) {
		final Set<String> res = new HashSet<>();
		class RuleDefinitionNSParser extends RuleDefinitionProcessorImpl {
			@Override
			public void processName(String name) {
				String[] nn = name.split(":");
				if (nn.length>1) {
					res.add(nn[0]);
				}
			}
		}
		ImplProvider implProvider = new ImplProvider() {
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			@Override
			public <T extends Processor> T provideImpl(Class<T> t) {
				if (t.equals(RuleDefinitionProcessor.class)){
					return (T) new RuleDefinitionNSParser();
				}
				return null;
			}
		};
		RulesVisitor.INSTANCE.visitAndProcess(t, implProvider);
		return res;
	}
	
	public String getFormatedXPATH(String xpath) {
		String res = xpath;
		if (xpath != null) {
			res = res.replace("\\\"", "\"");
			res = res.replace("\"", "\\u0027");
		}
		return res;
	}
	
}
