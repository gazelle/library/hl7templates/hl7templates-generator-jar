package net.ihe.gazelle.tempgen.action;

import net.ihe.gazelle.goc.uml.utils.UMLLoader;
import net.ihe.gazelle.goc.uml.utils.UMLPrimitiveType;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempmodel.dpath.model.DElement;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.*;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Allow to generate the OCL for the root path
 * ie : for example : //hl7:author[templateId/@root=2.16.840.1.113883.2.4.3.11.60.22.10.102]/hl7:assignedAuthor/hl7:assignedPerson
 *
 * @author Abderrazek Boufahja
 */
public class OCLGenerator {

    public String generateRuleToParentFromPath(Attribute attribute) {
        DParent dparent = AttributeUtil.getDParentOfTheParent(attribute);
        if (dparent == null) {
            return null;
        }
        return this.generateRuleFromPath(dparent, attribute.getParentObject());

    }

    public String generateRuleContextToRuleDefinition(RuleDefinition ruleDefinition) {
        DParent dparent = RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition);
        return this.generateRuleFromPath(dparent, ruleDefinition);

    }

    public String generateRuleToParentFromPath(RuleDefinition ruleDefinition) {
        DParent dparent = RuleDefinitionUtil.getDParentOfTheParent(ruleDefinition);
        return this.generateRuleFromPath(dparent, ruleDefinition.getParentObject());

    }

    protected String generateRuleFromPath(DParent parentPath, Object ruleParent) {
        return this.generateContextRule(parentPath, parentPath.size() - 1, ruleParent);
    }

    protected String generateContextRule(DParent parentPath, Integer index, Object ruleParent) {
        return generateContextRule(parentPath, index, null, ruleParent);
    }

    public String generateRuleToParentFromPathNF(RuleDefinition ruleDefinition){
        DParent dparent = RuleDefinitionUtil.getDParentOfTheParent(ruleDefinition);
        List<String> rules = new ArrayList<>();
        Object parent = ruleDefinition.getParentObject();
        for (int i = dparent.size()-1 ; i >= 0 ; i--){
           rules.add(0, generateContextRule(dparent, i, parent));
           parent = getParent(parent);
        }
        return addNullFlavorChecks(rules);
    }

    private Object getParent(Object object){
        Object parent = null;
        if (object instanceof RuleDefinition) {
            parent = ((RuleDefinition) object).getParentObject();
        } else if (object instanceof TemplateDefinition) {
            parent = ((TemplateDefinition) object).getParentObject();
        } else if (object instanceof ChoiceDefinition) {
            parent = ((ChoiceDefinition) object).getParentObject();
        }
        return parent;
    }

    public String getMinimumMultiplicityRuleForRD(String rule, RuleDefinition ruleDefinition){
        StringBuilder result = new StringBuilder();
        if (rule.equals("(not self.nullFlavor.oclIsUndefined()) or ")){
                result.append("self.");
                result.append(RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition));
                result.append(OCLGenerator.getExtraSelectionIfNeeded(ruleDefinition));
                result.append("->size()>");
                result.append(ruleDefinition.getMinimumMultiplicity()-1);
        } else {
            int max = (new UMLElementsManager()).getMaxAttribute(RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition), null);
            result.append("->forAll(");
            result.append(extractEliminationOfNullFlavor(ruleDefinition));
            if (max == 1){
                result.append("not ");
                result.append(RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition));
                result.append(".oclIsUndefined())");
            }
            else{
                result.append(RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition));
                result.append(OCLGenerator.getExtraSelectionIfNeeded(ruleDefinition));
                result.append("->size()>");
                result.append(ruleDefinition.getMinimumMultiplicity()-1);
                result.append(")");
            }
        }
        return result.toString();
    }

    private String addNullFlavorChecks(List<String> contextRules){
        StringBuilder result = new StringBuilder();
        int nbrRules = contextRules.size();
        for(int i = 0 ; i < nbrRules ; i++) {
            String contextRule = contextRules.get(i);
            if (contextRule.equals("self")){
                result.append("(not self.nullFlavor.oclIsUndefined()) or ");
            } else {
                if (i != nbrRules -1){
                    StringBuilder modifiedNextRule = new StringBuilder();
                    modifiedNextRule.append(contextRule);
                    modifiedNextRule.append("->reject(nullFlavor.oclIsUndefined())");
                    String nextContextRule = contextRules.get(i + 1);
                    modifiedNextRule.append(nextContextRule.replace(contextRule.replace("->reject(nullFlavor.oclIsUndefined())", "")
                            , ""));
                    contextRules.set(i + 1, modifiedNextRule.toString());
                } else {
                    result.append(contextRule);
                }
            }
        }
        return result.toString();
    }

    private String extractEliminationOfNullFlavor(RuleDefinition ruleDefinition) {
        RuleDefinitionUtil.getParentTemplateDefinition(ruleDefinition);
        Object parent = ruleDefinition.getParentObject();
        if (parent instanceof RuleDefinition) {
            RuleDefinition pRD = (RuleDefinition)parent;
            String typeName = RuleDefinitionUtil.getConstrainedUMLTypeName(pRD);
            typeName = typeName + ".nullFlavor";
            if (UMLLoader.isAnAttribute(typeName)) {
                return "(not nullFlavor.oclIsUndefined()) or ";
            }
        }
        else if (parent instanceof ChoiceDefinition) {
            ChoiceDefinition cd = (ChoiceDefinition) parent;
            if (cd.getParentObject() instanceof RuleDefinition) {
                RuleDefinition pRD = (RuleDefinition)cd.getParentObject();
                String typeName = RuleDefinitionUtil.getConstrainedUMLTypeName(pRD);
                typeName = typeName + ".nullFlavor";
                if (UMLLoader.isAnAttribute(typeName)) {
                    return "(not nullFlavor.oclIsUndefined()) or ";
                }
            }
        }
        return "";
    }

    /**
     * @param parentPath         : the DParent of the contextRule
     * @param indexParam         : the index of the element for which we are looking for the contextRule
     * @param stoppingIndexParam : the index of the element for which we will stop.
     * @param ruleParent         parent rule
     * @return index shall be greater than stoppingIndex. Here we extract the context : going from steppIngIndex to index
     * If index = stopingIndex, we return ""
     */
    protected String generateContextRule(DParent parentPath, Integer indexParam, Integer stoppingIndexParam, Object ruleParent) {
        Integer index = (indexParam == null) ? (parentPath.size() - 1) : indexParam;
        Integer stoppingIndex = (stoppingIndexParam == null) ? -1 : stoppingIndexParam;
        if (index <= stoppingIndex) {
            return "";
        }
        if (parentPath.size() <= index) {
            ProblemHandler.handleError("OCLGenerator::generateContextRule is handling a non sense value", "OCLGenerator::generateContextRule");
            return "";
        }
        if (index > 0) {
            String parent = generateParentContext(parentPath, index, stoppingIndex, ruleParent);
            String elementName = parentPath.get(index).getName() + addTypeConstrainedIfNeeded(parentPath.get(index));
            int max = (new UMLElementsManager()).getMaxAttribute(parentPath, index);
            String res = addElementNameToParentContext(parent, elementName, max);
            return processRuleParentForExtraSelectionIfNeeded(stoppingIndex, ruleParent, res);

        }
        return "self";
    }

    private String generateParentContext(DParent parentPath, Integer index, Integer stoppingIndex, Object ruleParent) {
        String parent = null;
        if (stoppingIndex < index - 1) {
            if (ruleParent instanceof RuleDefinition) {
                parent = this.generateContextRule(parentPath, index - 1, stoppingIndex, ((RuleDefinition) ruleParent).getParentObject());
            } else if (ruleParent instanceof ChoiceDefinition) {
                parent = generateParentContextForChoiceDefinition(parentPath, index, stoppingIndex, (ChoiceDefinition) ruleParent);
            } else {
                parent = this.generateContextRule(parentPath, index - 1, stoppingIndex, null);
            }
        }
        return parent;
    }

    private String generateParentContextForChoiceDefinition(DParent parentPath, Integer index, Integer stoppingIndex, ChoiceDefinition cdParent) {
        RuleDefinition parentRuleDefinition = (RuleDefinition) cdParent.getParentObject();
        return this.generateContextRule(parentPath, index - 1, stoppingIndex, parentRuleDefinition.getParentObject());
    }

    private String addElementNameToParentContext(String parent, String elementName, int max) {
        String res;
        if (max == 1) {
            if (parent != null) {
                res = parent + "." + elementName;
            } else {
                res = elementName;
            }
        } else {
            res = processContextRuleWithRejectOCLForSetElement(parent, elementName);
        }
        return res;
    }

    private String processContextRuleWithRejectOCLForSetElement(String parent, String attrName) {
        String res;
        if (parent != null) {
            res = parent + "." + attrName + OCLCleaner.generateRejectOcl(false);
        } else {
            res = attrName + OCLCleaner.generateRejectOcl(false);
        }
        return res;
    }

    private String processRuleParentForExtraSelectionIfNeeded(Integer stoppingIndex, Object ruleParent, String resParam) {
        String res = resParam;
        if (stoppingIndex == -1 && res.contains(".")) {
            if (ruleParent instanceof RuleDefinition) {
                res = res + getExtraSelectionIfNeeded((RuleDefinition) ruleParent);
            } else if (ruleParent instanceof ChoiceDefinition) {
                ChoiceDefinition cd = (ChoiceDefinition) ruleParent;
                res = res + getExtraSelectionIfNeeded((RuleDefinition) cd.getParentObject());
            } else {
                ProblemHandler.handleError("The kind of parent is unknown", AnalyzerEnum.RULE_DEF_GENERAL_PROCESS.getValue());
            }
        }
        return res;
    }

    public static String addTypeConstrainedIfNeeded(DParent dParent) {
        if (dParent instanceof DElement) {
            String res = "";
            DElement dElement = (DElement) dParent;
            if (dElement.getExtendedType() != null && !dElement.getExtendedType().trim().equals("")) {
                res = "->select(oclIsKindOf(" + dElement.getExtendedType() + ")).oclAsType(" + dElement.getExtendedType() + ")";
            }
            return res;
        }
        return "";
    }

    /**
     * return the max cardinality of a path :
     * example : act.templateId.root -&gt; -1
     * act.classCode -&gt; 1
     * @param parentPath path to parent
     * @param indexParam         : the index of the element for which we are looking for the contextRule
     * @param stoppingIndexParam : the index of the element for which we will stop.
     * @return int
     */
    protected int generateMaxCardinalityOfPath(DParent parentPath, Integer indexParam, Integer stoppingIndexParam) {
        int index = (indexParam == null) ? parentPath.size() - 1 : indexParam;
        int stoppingIndex = (stoppingIndexParam == null) ? -1 : stoppingIndexParam;
        if (index == stoppingIndex) {
            return 1;
        }
        if (index > 0) {
            int parent;
            parent = this.generateMaxCardinalityOfPath(parentPath, index - 1, stoppingIndex);
            int max = (new UMLElementsManager()).getMaxAttribute(parentPath, index);
            return Math.min(parent, max);
        } else {
            return 1;
        }
    }

    /**
     * create a select for the attribute to be tested if there are a need to
     *
     * @param ruleDefinition rule
     * @return string
     */
    public static String getExtraSelectionIfNeeded(RuleDefinition ruleDefinition) {
        if (!RuleDefinitionProblemUtil.ruleDefinitionNeedDistinguisherOrCanHaveNegatifDistinguisher(ruleDefinition)) {
            return "";
        }
        String addenda = getExtraDistinguisherIfNeeded(ruleDefinition);
        if (addenda != null && !addenda.trim().equals("")) {
            return "->select(" + addenda + ")";
        }
        return "";
    }

    /**
     * return the content of a distinguisher if exists for a RD
     *
     * @param ruleDefinition rule
     * @return string
     */
    public static String getExtraDistinguisherIfNeeded(RuleDefinition ruleDefinition) {
        if (ruleDefinition == null) {
            return "";
        }
        Pair<String, String> pairDisting = RuleDefinitionDistinguisherUtil.extractUniqueDistinguisher(ruleDefinition);
        if (pairDisting != null) {
            String addenda = getExtraSelectionByDistinguisherForRuleDefinition(ruleDefinition, pairDisting);
            if (!StringUtils.isEmpty(addenda)) {
                return addenda;
            }
        } else if (RuleDefinitionProblemUtil.verifyThatRuleDefinitionCanHaveNegativeDistinguisher(ruleDefinition)) {
            Map<RuleDefinition, Pair<String, String>> ldis = RuleDefinitionDistinguisherUtil.extractListDistinguishersForBrodhers(ruleDefinition);
            StringBuilder sb = new StringBuilder("");
            for (Entry<RuleDefinition, Pair<String, String>> rdAndPair : ldis.entrySet()) {
                if (rdAndPair.getValue() != null) {
                    String addenda = getExtraSelectionByDistinguisherForRuleDefinition(rdAndPair.getKey(), rdAndPair.getValue());
                    sb = addANDIfStringNotEmpty(sb);
                    sb.append("(not (" + addenda + "))");
                }
            }
            return sb.toString();
        }
        return "";
    }

    private static StringBuilder addANDIfStringNotEmpty(StringBuilder sb) {
        if (sb != null && sb.length() > 0) {
            sb.append(" and ");
        }
        return sb;
    }

    /**
     * return a distinguisher string based on distinguisher description
     *
     * @param ruleDefinition rule
     * @param pairDisting pair of distinguisher
     * @return a distinguisher string based on distinguisher description
     */
    protected static String getExtraSelectionByDistinguisherForRuleDefinition(RuleDefinition ruleDefinition, Pair<String, String> pairDisting) {
        if (pairDisting != null && pairDisting.getObject1() != null && pairDisting.getObject2() != null) {
            DParent dElement = DPathExtractor.extractDElementFromDPath(pairDisting.getObject1());
            String valueDPath = pairDisting.getObject2();
            DParent ruleDefinitionDParent = RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition);
            int sizeoriginal = ruleDefinitionDParent.size();
            if (ruleDefinitionDParent.get(sizeoriginal - 1) instanceof DElement) {
                ((DElement) ruleDefinitionDParent.get(sizeoriginal - 1)).setFollowingAttributeOrElement(dElement);

                String typeName = (new PathManager()).findPathTypeName(ruleDefinitionDParent);
                String valueDPathModified = "'" + valueDPath + "'";
                if (typeName != null && UMLLoader.isEnumeration(typeName)) {
                    valueDPathModified = typeName + "::" + valueDPath;
                    ImportElementHandler.handleAddingElement(typeName);
                } else if (UMLPrimitiveType.BOOLEAN.getValue().equals(typeName) ||
                        UMLPrimitiveType.DOUBLE.getValue().equals(typeName) ||
                        UMLPrimitiveType.INTEGER.getValue().equals(typeName)) {
                    valueDPathModified = valueDPath;
                }

                return createDistinguisherString(ruleDefinition, ruleDefinitionDParent, sizeoriginal, typeName, valueDPathModified);
            }
        }
        return null;
    }

    private static String createDistinguisherString(RuleDefinition ruleDefinition, DParent ruleDefinitionDParent,
                                                    int sizeoriginal, String typeName, String valueDPathModified) {
        String res = (new OCLGenerator()).generateContextRule(ruleDefinitionDParent, null, sizeoriginal - 1, ruleDefinition);
        int maxTotal = (new OCLGenerator()).generateMaxCardinalityOfPath(ruleDefinitionDParent, null, sizeoriginal - 1);
        if (maxTotal == -1) {
            res = res + "->exists(aa : " + typeName + "| aa = " + valueDPathModified + ")";
            if (!UMLPrimitiveType.listUMLPrimitives().contains(typeName)) {
                ImportElementHandler.handleAddingElement(typeName);
            }
        } else {
            res = "(not " + res + ".oclIsUndefined()) and " + res + "=" + valueDPathModified + "";
        }
        return res;
    }

    public String generateRuleToParentFromPath(ChoiceDefinition choiceDefinition) {
        DParent dparent = RuleDefinitionUtil.getDParentOfRuleDefinition((RuleDefinition) choiceDefinition.getParentObject());
        return this.generateRuleFromPath(dparent, (RuleDefinition) choiceDefinition.getParentObject());
    }

    public String generateRuleToParentFromPathNF(ChoiceDefinition choiceDefinition) {
        RuleDefinition parent = new RuleDefinition();
        parent.setParentObject(choiceDefinition.getParentObject());
        return this.generateRuleToParentFromPathNF(parent);
    }

    public String generateRuleToParentFromPathNF(Attribute attribute){
        DParent dparent = AttributeUtil.getDParentOfTheParent(attribute);
        if (dparent == null) {
            return null;
        }
        List<String> rules = new ArrayList<>();
        Object parent = attribute.getParentObject();
        for (int i = dparent.size()-1 ; i >= 0 ; i--){
            rules.add(0, generateContextRule(dparent, i, parent));
            parent = getParent(parent);
        }
        return addNullFlavorChecks(rules);
    }
}
