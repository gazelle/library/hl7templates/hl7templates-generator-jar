package net.ihe.gazelle.tempgen.action;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AttributeUtil;

public class DistinguisherFollower {
	
	private static Map<String, String> mapListDistinguishers = new HashMap<>();
	
	private DistinguisherFollower() {}
	
	public static void followDistinguisher(String path, String value) {
		if (path != null && value != null) {
			mapListDistinguishers.put(path, value);
		}
	}
	
	public static void unFollowDistinguisher(String path) {
		if (path != null) {
			mapListDistinguishers.remove(path);
		}
	}
	
	public static boolean verifyIfPathAndValueAreDistinguisher(String path, String value) {
		return path != null && value != null && mapListDistinguishers.get(path) != null && mapListDistinguishers.get(path).equals(value);
	}
	
	public static boolean verifyIfAttributeAndValueAreDistinguisher(Attribute attr, String attributeName, String value) {
		if (attr != null) {
			DParent dpath = AttributeUtil.getDParentOfAttibute(attr, attributeName, value);
			String path = DPathExtractor.createPathFromDParent(dpath);
			return verifyIfPathAndValueAreDistinguisher(path, value);
		}
		return false;
	}
	
	public static String showDifferentDistinguisher(){
		StringBuilder sb = new StringBuilder("");
		for (Entry<String, String> pathAndValue : mapListDistinguishers.entrySet()) {
			sb.append("distinguisher : " + pathAndValue.getKey() + " = " + pathAndValue.getValue() + "\n");
		}
		sb.append("total number distinguisher : " + mapListDistinguishers.size());
		return sb.toString();
	}
	
	/**
	 * to be used for test urpose only !
	 */
	public static void cleanDistinguishers() {
		mapListDistinguishers.clear();
	}

}
