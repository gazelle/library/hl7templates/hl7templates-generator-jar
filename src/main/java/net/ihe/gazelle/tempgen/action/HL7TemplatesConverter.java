package net.ihe.gazelle.tempgen.action;


import net.ihe.gazelle.goc.uml.utils.XMIUtil;
import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.goc.xmi.XMIMarshaller;
import net.ihe.gazelle.tempgen.flatten.action.GeneralFlattenDecor;
import net.ihe.gazelle.tempgen.flatten.action.RulesCleaner;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.inc.action.IncludeFlattener;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorUtil;
import net.ihe.gazelle.validation.Notification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Abderrazek Boufahja
 */
public class HL7TemplatesConverter {

    private static Logger log = LoggerFactory.getLogger(HL7TemplatesConverter.class);

    public String convertArtDecorXMI(String bbrpath) throws JAXBException, FileNotFoundException {
        return convertArtDecorXMI(bbrpath, false, null);
    }

    public String convertArtDecorXMI(String bbrpath, boolean ignoreTemplateIdRequirements, String versionLabel)
            throws JAXBException, FileNotFoundException {
        log.info("Converting BBR to UML Model");
        Decor decor = DecorMarshaller.loadDecor(bbrpath);
        if (versionLabel != null) {
            log.info("Clean-up for version label : " + versionLabel);
            decor = DecorUtil.cleanupForVersionLabel(decor, versionLabel);
        }

        log.info("Remove older version of duplicated templates");
        decor = DecorUtil.cleanupForEffectiveDate(decor);


        log.info("Flattening Decor");
        decor = flattenDecor(decor);
        log.info("Convert Decor to XMI");
        XMI xmi = convertFlattenedDecorToXMI(decor, ignoreTemplateIdRequirements);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        XMIMarshaller.printXMI(xmi, baos);
        String res = baos.toString();
        res = res.replaceAll("\\<xmi:XMI.*\\>", "<xmi:XMI xmi:version=\"2.1\" xmlns:xmi=\"http://schema.omg.org/spec/XMI/2.1\"\n" +
                "	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                "	xmlns:Ecore=\"http://www.eclipse.org/uml2/schemas/Ecore/5\"\n" +
                "	xmlns:TemplateDefiner=\"http:///schemas/TemplateDefiner/_L1F1MLk9EeadT_oVto_uPg/43\"\n" +
                "	xmlns:ecore=\"http://www.eclipse.org/emf/2002/Ecore\"\n" +
                "	xmlns:uml=\"http://www.eclipse.org/uml2/3.0.0/UML\"\n" +
                "	xsi:schemaLocation=\"http://www.eclipse.org/uml2/schemas/Ecore/5 " +
                "pathmap://UML_PROFILES/Ecore.profile.uml#_z1OFcHjqEdy8S4Cr8Rc_NA " +
                "http:///schemas/TemplateDefiner/_L1F1MLk9EeadT_oVto_uPg/43 ../../common-models/models/common-profile" +
                ".uml#_L1HqYLk9EeadT_oVto_uPg\">");
        return res;
    }

    Decor flattenDecor(Decor decorParam) throws JAXBException, FileNotFoundException {
        Decor decor = decorParam;
        log.info("Clean rules of the decor to flatten");
        RulesCleaner.cleanRules(decor.getRules());
        //Transform include and contains to contain
        log.info("GeneralFlattenDecor.generalFlattenDecor");
        decor = GeneralFlattenDecor.generalFlattenDecor(decor);
        //Transform contain to ContainsDefinitions
        log.info("GeneralFlattenDecor.generalFlattenBisDecor");
        decor = GeneralFlattenDecor.generalFlattenBisDecor(decor);
        // ? Includes are transformed to contains what does it do here ?
        log.info("IncludeFlattener.flattenIncludeInDecor");
        decor = IncludeFlattener.flattenIncludeInDecor(decor);
        return decor;
    }


    XMI convertFlattenedDecorToXMI(Decor decor, boolean ignoreTemplateIdRequirements) {
        String prefix = DecorUtil.extractPrefixFromDecor(decor);
        XMI xmi = XMIUtil.createXMI(prefix);
        (new DecorAnalyzer()).process(decor, xmi, ignoreTemplateIdRequirements);
        return xmi;
    }

    public static void logProblems(String ps_folderOutput) {
        int i = 0;
        StringBuilder lo_errorReport = new StringBuilder(
                "Here are listed all inconsistency between the BBR and the CDA model used by the validator:\n\n\"-------------\n");
        for (Notification not : ProblemHandler.getDiagnostic()) {
            if (not instanceof net.ihe.gazelle.validation.Error) {
                //log in output file in logs/ in outputFolder.
                log.info("Type        : " + not.getClass().getSimpleName());
                log.info("Process     : " + not.getTest());
                log.info("Location    : " + not.getLocation());
                log.info("Description : " + not.getDescription());
                log.info("-------------");

                lo_errorReport.append("Type        : " + not.getClass().getSimpleName() + "\n");
                lo_errorReport.append("Process     : " + not.getTest()+ "\n");
                lo_errorReport.append("Location    : " + not.getLocation()+ "\n");
                lo_errorReport.append("Description : " + not.getDescription()+ "\n");
                lo_errorReport.append("-------------\n");
                i++;
            }
        }

        // This seems never used ??
        List<String> listLocation = new ArrayList<>();
        for (Notification not : ProblemHandler.getDiagnostic()) {
            if (not instanceof net.ihe.gazelle.validation.Error) {
                listLocation.add(not.getDescription());
            }
        }

        log.info("number of incorformity to CDA model : {}", i);
        log.info("--");

        lo_errorReport.append("Number of inconsistency with CDA model :" + i + "\n");
        lo_errorReport.append("---------");
        createModelErrorLogs(ps_folderOutput, lo_errorReport.toString());
    }

    public static void createModelErrorLogs(String ps_folderOutput, String ps_reportContent){

        File file = new File(ps_folderOutput);
        String ls_reportPath = ps_folderOutput + File.separator + "cdaModelErrors.txt";

        String checkoutScriptPath = file.getAbsolutePath() + File.separator + "checkout.sh";
        try {
            FileReadWrite.printDoc(ps_reportContent, ls_reportPath);
        } catch (IOException e){
            log.error("Could not create report on inconsistency with CDA model : {}", e);
        }
    }
}
