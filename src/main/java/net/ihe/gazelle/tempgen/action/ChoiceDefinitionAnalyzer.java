package net.ihe.gazelle.tempgen.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import net.ihe.gazelle.goc.uml.utils.OwnedRuleUtil;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.OwnedRuleKind;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.tempapi.impl.ChoiceDefinitionProcessorImpl;
import net.ihe.gazelle.tempgen.choice.analyzer.ChoiceConstraintGenerator;
import net.ihe.gazelle.tempgen.choice.analyzer.ChoiceMaximumMultiplicityANalyzer;
import net.ihe.gazelle.tempgen.choice.analyzer.ChoiceMinimumMultiplicityANalyzer;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempmodel.choices.model.ChoiceDefault;
import net.ihe.gazelle.tempmodel.choices.utils.ChoiceDefaultUtil;
import net.ihe.gazelle.tempmodel.handler.util.ChoiceDefinitionProblemUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ItemUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ChoiceDefinitionAnalyzer extends ChoiceDefinitionProcessorImpl {
	
	protected ChoiceDefinition currentChoiceDefinition;
	
	protected PackagedElement currentPackagedElement;
	
	protected Boolean ignoreTemplateIdRequirements = false;
	
	private List<OwnedRule> listGeneratedOwnedRule = new ArrayList<>();
	
	/**
	 *   objects[0] is the PackagedElement
	 */
	@Override
	public void process(ChoiceDefinition t, Object... objects) {
		if (objects == null || objects.length<1 || !(objects[0] instanceof PackagedElement)) {
			ProblemHandler.handleError("The method ChoiceDefinitionAnalyzer::process is called with a bad objects attribute", 
					AnalyzerEnum.CHOICE_PROCESS.getValue());
			return;
		}
		if (objects.length>1) {
			this.ignoreTemplateIdRequirements = (Boolean)objects[1];
		}
		this.currentChoiceDefinition = t;
		this.currentPackagedElement = (PackagedElement) objects[0];
		if (verifyThatChoiceIsProcessable(this.currentChoiceDefinition)){
			super.process(t, objects);
		}
		
//		-- to be done

//		-- processed
//		this.processConstraints
//		this.processDescs
//		this.processElements
//		this.processIncludes : if there are includes : abord the process
//		this.processMaximumMultiplicity
//		this.processMinimumMultiplicity
//		this.processItem
//		processContains : nothing to do, processed during flattening
	}
	
	@Override
	public void processItem(Item item) {
		Set<String> setItems = ItemUtil.extractRelatedItems(this.currentChoiceDefinition);
		if (setItems != null){
			for (OwnedRule or : this.listGeneratedOwnedRule) {
				TamlHandler.handleAllTAML(setItems, or);
			}
		}
	}
	
	@Override
	public void processMaximumMultiplicity(String maximumMultiplicityString) {
		Integer maximumMultiplicity = MultiplicityUtil.extractMaximumMultiplicity(maximumMultiplicityString);
		if (processingMaximumMultiplicityIsPossible(maximumMultiplicity)) {
			fulfillOwnedRuleForConstraintGenerator(new ChoiceMaximumMultiplicityANalyzer(), OwnedRuleKind.CARDINALITY);
		}
	}
	
	private boolean processingMaximumMultiplicityIsPossible(Integer maximumMultiplicity) {
		if (maximumMultiplicity == null || maximumMultiplicity>=MultiplicityUtil.MAX_INTEGER) {
			return false;
		}
		ChoiceDefault choiceDefault = extractChoiceDefaultFromChoiceDefinition(this.currentChoiceDefinition);
		if (choiceDefault != null && choiceDefault.getMax() != null) {
			if (maximumMultiplicity < choiceDefault.getMax()) {
				return true;
			}
			else if (maximumMultiplicity > choiceDefault.getMax()) {
				ProblemHandler.handleError("The choice contains a problem, the maximum specified is bigger than "
						+ "the default maximum choice value : " + choiceDefault.getMax() + ", specified for the type " +
						choiceDefault.getType(), 
						AnalyzerEnum.CHOICE_PROCESS.getValue());
				return false;
			}
			return false;
		}
		return true;
	}

	@Override
	public void processMinimumMultiplicity(Integer minMultiplicity) {
		if (processingMinimumMultiplicityIsPossible(minMultiplicity)) {
			fulfillOwnedRuleForConstraintGenerator(new ChoiceMinimumMultiplicityANalyzer(), OwnedRuleKind.CARDINALITY);
		}
	}
	
	protected boolean processingMinimumMultiplicityIsPossible(Integer minMultiplicity) {
		if (minMultiplicity==null || minMultiplicity<=0) {
			return false;
		}
		ChoiceDefault choiceDefault = extractChoiceDefaultFromChoiceDefinition(this.currentChoiceDefinition);
		if (choiceDefault != null && choiceDefault.getMin() != null) {
			if (minMultiplicity > choiceDefault.getMin()) {
				return true;
			}
			else if (minMultiplicity < choiceDefault.getMin()) {
				ProblemHandler.handleError("The choice contains a problem, the minimum specified is lower than "
						+ "the default minimum choice value : " + choiceDefault.getMin() + ", specified for the type " +
						choiceDefault.getType(), 
						AnalyzerEnum.CHOICE_PROCESS.getValue());
				return false;
			}
			return false;
		}
		return true;
	}

	protected void fulfillOwnedRuleForConstraintGenerator(ChoiceConstraintGenerator choiceConstraintGenerator, OwnedRuleKind ownedRuleKind){
		OwnedRule ownedRule = OwnedRuleUtil.initOwnedRule();
		ownedRule.setConstrainedElement(this.currentPackagedElement.getId());
		String specification = choiceConstraintGenerator.generateOCLConstraint(this.currentChoiceDefinition);
		String comment = choiceConstraintGenerator.generateCommentConstraint(this.currentChoiceDefinition) + 
				ItemCommentGenerator.generateItemComment(currentChoiceDefinition);
		ownedRule.getSpecification().setBody(specification);
		ownedRule.getOwnedComment().setBody(comment);
		ownedRule.setOwnedRuleKind(ownedRuleKind);
		this.currentPackagedElement.getOwnedRule().add(ownedRule);
		this.listGeneratedOwnedRule.add(ownedRule);
	}
	
	@Override
	public void processElements(List<RuleDefinition> elements) {
		if (elements != null) {
			for (RuleDefinition ruleDefinition : elements) {
				(new RuleDefinitionAnalyzer()).process(ruleDefinition, this.currentPackagedElement, this.ignoreTemplateIdRequirements);
			}
		}
	}
	
	public static ChoiceDefault extractChoiceDefaultFromChoiceDefinition(ChoiceDefinition cd) {
		String type = null;
		if (cd.getParentObject() instanceof RuleDefinition) {
			type = RuleDefinitionUtil.getConstrainedUMLTypeName((RuleDefinition)cd.getParentObject());
		}
		List<String> listElementsName = new ArrayList<>();
		for (RuleDefinition rd : ChoiceDefinitionUtil.getElements(cd)) {
			String name = RuleDefinitionUtil.getRealNameOfRuleDefinition(rd);
			if (name != null) {
				listElementsName.add(name);
			}
		}
		return ChoiceDefaultUtil.findChoiceDefaultByTypeAndSubelements(type, listElementsName);
	}

	protected boolean verifyThatChoiceIsProcessable(ChoiceDefinition cd) {
		if (cd != null) {
			List<IncludeDefinition> incs = ChoiceDefinitionUtil.getIncludes(cd);
			if (incs != null && !incs.isEmpty()) {
				ProblemHandler.handleError("The choice is not processable because it contains some includes : " + incs.size(), 
						AnalyzerEnum.CHOICE_PROCESS.getValue());
				return false;
			}
			if (!ChoiceDefinitionProblemUtil.choiceContainsValidsubElements(cd)) {
				ProblemHandler.handleRuleDefinitionError((RuleDefinition)cd.getParentObject(), 
						"There are a problem : the choice contains sub elements which are not from CDA", 
						AnalyzerEnum.CHOICE_PROCESS.getValue());
				return false;
			}
			return true;
		}
		return false;
	}

}
