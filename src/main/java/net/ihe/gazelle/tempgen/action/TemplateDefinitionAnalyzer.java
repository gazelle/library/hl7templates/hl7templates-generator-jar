package net.ihe.gazelle.tempgen.action;

import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import net.ihe.gazelle.goc.template.definer.model.TemplateSpec;
import net.ihe.gazelle.goc.uml.utils.PackagedElementUtil;
import net.ihe.gazelle.goc.uml.utils.TemplateSpecUtil;
import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.tempapi.impl.TemplateDefinitionProcessorImpl;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ItemUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;
import net.ihe.gazelle.tempmodel.temp.mapping.utils.TemplatesTypeMappingUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class TemplateDefinitionAnalyzer extends TemplateDefinitionProcessorImpl {
	
	private PackagedElement currentPackagedElement;
	
	private TemplateDefinition currentTemplateDefinition;
	
	private Boolean ignoreTemplateIdRequirements = false;
	
	@Override
	public void process(TemplateDefinition t, Object... objects) {
		if (objects== null || objects.length<1 || !(objects[0] instanceof XMI)) {
			ProblemHandler.handleError("The method TemplateDefinitionAnalyzer::process is called with a bad objects attribute", 
					AnalyzerEnum.RULE_DEF_GENERAL_PROCESS.getValue());
			return;
		}
		XMI currentXMI = (XMI) objects[0];
		if (objects.length>1) {
			this.ignoreTemplateIdRequirements = (Boolean) objects[1];
		}
		this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
		this.currentTemplateDefinition = t;
		String nam = this.currentTemplateDefinition.getName().replace("-", "").replace(".", "");
		this.currentPackagedElement.setName(nam);
		super.process(this.currentTemplateDefinition, objects);
		
		generateGeneralizationForPackagedElement(this.currentPackagedElement, this.currentTemplateDefinition);
		attachTemplateSpec(this.currentTemplateDefinition, currentXMI, this.currentPackagedElement);
		if (currentXMI != null && currentXMI.getModel() != null) {
			currentXMI.getModel().getPackagedElement().add(this.currentPackagedElement);
		}
	}
	
	@Override
	public void processAttributes(List<Attribute> attributes) {
		if (attributes != null && !attributes.isEmpty()) {
			ProblemHandler.handleError("There are no way to have such situation : the attributes elements are ignored in TemplateDefinition : id=" + 
					this.currentTemplateDefinition.getId(), 
					AnalyzerEnum.TEMPLATE_PROCESS.getValue());
		}
	}
	
	@Override
	public void processIncludes(List<IncludeDefinition> includes) {
		if (includes != null && !includes.isEmpty()) {
			ProblemHandler.handleError("There are no way to have such situation : the include elements are ignored in TemplateDefinition : id=" + 
					this.currentTemplateDefinition.getId(), 
					AnalyzerEnum.TEMPLATE_PROCESS.getValue());
		}
	}
	
	@Override
	public void processChoices(List<ChoiceDefinition> choices) {
		if (choices != null && !choices.isEmpty()) {
			ProblemHandler.handleError("There are no way to have such situation : the choice element are ignored in TemplateDefinition : id=" + 
					this.currentTemplateDefinition.getId(), 
					AnalyzerEnum.TEMPLATE_PROCESS.getValue());
		}
	}

	@Override
	public void processElements(List<RuleDefinition> ruleDefinitions) {
		for (RuleDefinition ruleDefinition : ruleDefinitions) {
			(new RuleDefinitionAnalyzer()).process(ruleDefinition, this.currentPackagedElement, this.ignoreTemplateIdRequirements);
		}
		
	}
	
	//TODO missing : how to process Item related to distinguisher ?
	@Override
	public void processItem(Item item) {
		Set<String> setItems = ItemUtil.extractRelatedItems(this.currentTemplateDefinition);
		if (setItems != null){
			TamlHandler.handleAllTAML(setItems, this.currentPackagedElement);
		}
	}

	protected void attachTemplateSpec(TemplateDefinition templateDefinition,
			XMI xmi, PackagedElement pe) {
		String templateId = TemplateDefinitionUtil.extractTemplateId(templateDefinition);
		if (!StringUtils.isEmpty(templateId)){
			String nameTDUML = TemplateDefinitionUtil.getTypeNameOfTemplateDefinition(templateDefinition);
			String templateIdPath = TemplatesTypeMappingUtil.extractTemplatePath(nameTDUML);
			TemplateSpec ts = TemplateSpecUtil.createTemplateSpec(pe, templateId, templateIdPath);
			xmi.getTemplateSpec().add(ts);
		}
	}


	protected void generateGeneralizationForPackagedElement(PackagedElement pe, TemplateDefinition templateDefinition) {
		String tdName = TemplateDefinitionUtil.getTypeNameOfTemplateDefinition(templateDefinition);
		PackagedElementUtil.addGeneralizationToPackagedElement(pe, tdName);
	}
	

}
