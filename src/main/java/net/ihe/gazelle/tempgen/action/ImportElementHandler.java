package net.ihe.gazelle.tempgen.action;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.eclipse.emf.ecore.util.EcoreUtil;

import net.ihe.gazelle.goc.uml.utils.CDAClassesMap;
import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.goc.xmm.ElementImport;
import net.ihe.gazelle.goc.xmm.ImportedElement;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ImportElementHandler {
	
	private ImportElementHandler() {}
	
	private static Map<String, ElementImport> mapImportedToUML = new TreeMap<>();
	
	public static Map<String, ElementImport> getMapImportedToUML() {
		return mapImportedToUML;
	}

	public static Set<String> getSetImported() {
		return mapImportedToUML.keySet();
	}

	public static void handleAddingElement(String element){
		if (mapImportedToUML.keySet().contains(element)){
			return;
		}
		ElementImport ei = new ElementImport();
		ei.setId(EcoreUtil.generateUUID());
		ei.setImportedElement(new ImportedElement());
		if (CDAClassesMap.getRelativeXMILink(element) != null){
			ei.getImportedElement().setType(CDAClassesMap.getXMIType(element));
			ei.getImportedElement().setHref(CDAClassesMap.getRelativeXMILink(element));
		}
		else {
			ProblemHandler.handleRuleDefinitionError(null, "problem to find an import for the type : " + element, AnalyzerEnum.IMPORT_PROCESS.getValue());
			return;
		}
		mapImportedToUML.put(element, ei);
	}
	
	public static void addElementsToBeImported(XMI xmi) {
		if (xmi == null || xmi.getModel() == null){
			ProblemHandler.handleRuleDefinitionError(null, "There are no model in the XMI !", AnalyzerEnum.IMPORT_PROCESS.getValue());
			return;
		}
		for (Entry<String, ElementImport> entry : mapImportedToUML.entrySet()) {
			xmi.getModel().getElementImport().add(entry.getValue());
		}
	}

}
