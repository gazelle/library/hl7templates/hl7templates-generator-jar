package net.ihe.gazelle.tempgen.action;

import net.ihe.gazelle.tempmodel.org.decor.art.behavior.HasItem;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class ItemCommentGenerator {
	
	private ItemCommentGenerator() {}
	
	public static String generateItemComment(HasItem hasitem){
		if (hasitem != null && hasitem.getItem() != null && hasitem.getItem().getLabel() != null &&
				!hasitem.getItem().getLabel().trim().isEmpty()) {
			return " (Item : " + hasitem.getItem().getLabel().trim() + ")";
		}
		return "";
	}
	
}
