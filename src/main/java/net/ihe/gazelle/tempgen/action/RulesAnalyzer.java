package net.ihe.gazelle.tempgen.action;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.tempapi.impl.RulesProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class RulesAnalyzer extends RulesProcessorImpl {
	
	
	private static Logger log = LoggerFactory.getLogger(RulesAnalyzer.class);
	
	protected Rules currentRules;
	
	protected XMI xmi;
	
	protected List<TemplateDefinition> listCDATemplates;
	
	protected Boolean ignoreTemplateIdRequirements = false;
	
	@Override
	public void process(Rules t, Object... objects) {
		this.xmi = (XMI) objects[0];
		if (objects.length>1) {
			this.ignoreTemplateIdRequirements = (Boolean) objects[1];
		}
		this.currentRules = t;
		List<TemplateDefinition> ltd = RulesUtil.getTemplates(this.currentRules);
		log.info("complete templates size : {}", ltd.size());
		this.listCDATemplates = TemplateDefinitionUtil.extractListCDATemplate(ltd);
		log.info("listCDATemplate size : {}", this.listCDATemplates.size());
		
		super.process(t, objects);
		(new RootElementsAnalyzer()).process(currentRules, this.xmi, this.listCDATemplates);
	}
	
	// TODO add the IncludeSpec classes
	@Override
	public void processTemplates(List<TemplateDefinition> templates) {
		for (TemplateDefinition templateDefinition : templates) {
			if ((this.listCDATemplates != null) && this.listCDATemplates.contains(templateDefinition)) {
				(new TemplateDefinitionAnalyzer()).process(templateDefinition, this.xmi, this.ignoreTemplateIdRequirements);
			}
		}
	}		

}
