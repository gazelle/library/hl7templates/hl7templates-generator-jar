package net.ihe.gazelle.tempgen.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.goc.uml.utils.UMLLoader;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class UMLElementsManager {
	
	
	private static Logger log = LoggerFactory.getLogger(UMLElementsManager.class);
	
	/**
	 * return the maximum attribute of the UML element presenting the cdaPath
	 * Example : //cda:observation/cda:code -&gt; 1
	 * Example : //cda:observation/cda:value -&gt; -1
	 * @param cdaPath path to CDA
	 * @param indexParam index
	 * @return int
	 */
	public int getMaxAttribute(DParent cdaPath, Integer indexParam){
		Integer index = indexParam;
		if (index == null){
			index = cdaPath.size()-1;
		}
		String typeName = (new PathManager()).findPathTypeName(cdaPath, index-1);
		if (cdaPath.get(index) != null) {
			String attrName = cdaPath.get(index).getName();
			return UMLLoader.getMaxAttribute(typeName, attrName);
		}
		log.error("The requested index ( {} ) is out of bound regarding the cdaPath", index);
		return -1;
	}
	
	public int getMinAttribute(DParent cdaPath, Integer indexParam){
		Integer index = indexParam;
		if (cdaPath == null) {
			log.error("The cdaPath provided is null!");
			return 0;
		}
		if (index == null){
			index = cdaPath.size()-1;
		}
		String typeName = (new PathManager()).findPathTypeName(cdaPath, index-1);
		if (cdaPath.get(index) != null) {
			String attrName = cdaPath.get(index).getName();
			return UMLLoader.getMinAttribute(typeName, attrName);
		}
		log.error("The requested index ( {} ) is out of bound regarding the cdaPath", index);
		return 0;
	}
	
}
