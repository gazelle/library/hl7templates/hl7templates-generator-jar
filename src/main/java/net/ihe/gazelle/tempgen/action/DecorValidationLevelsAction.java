package net.ihe.gazelle.tempgen.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.ihe.gazelle.tempapi.visitor.DecorVisitor;
import net.ihe.gazelle.tempgen.attrs.flatten.AttrsFlattenImplProvider;
import net.ihe.gazelle.tempgen.flatten.action.GeneralFlattenDecor;
import net.ihe.gazelle.tempgen.flatten.action.RulesCleaner;
import net.ihe.gazelle.tempgen.inc.action.IncludeFlattener;
import net.ihe.gazelle.tempgen.valueset.flatten.VSFlattener;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.CloneUtil;

public final class DecorValidationLevelsAction {
	
	private DecorValidationLevelsAction() {
		// private constructor
	}
	
	// remove unused templates
	public static Decor createDecorLevel1(Decor decorUnflattened) {
		RulesCleaner.cleanRules(decorUnflattened.getRules());
		return decorUnflattened;
	}
	
	// flatten the assert
	// flatten the let
	// flatten the projectName
	// flatten the rules : 
	//		flatten the vocab to attribute when possible
	//		flatten the @contains to <include> or <contain> when needed
	//		flatten the rule name when possible (exp : entryRelationship[@typeCode='COMP']
	public static Decor createDecorLevel2(Decor decorLevel1) {
		return GeneralFlattenDecor.generalFlattenDecor(decorLevel1);
	}
	
	// flatten the contains to elements
	public static Decor createDecorLevel3(Decor decorLevel2) {
		return GeneralFlattenDecor.generalFlattenBisDecor(decorLevel2);
	}
	
	// flatten the includes
	public static Decor createDecorLevel4(Decor decorLevel3) {
		return IncludeFlattener.flattenIncludeInDecor(decorLevel3);
	}
	
	// flatten the attributes (exp : <attribute root="1.2.3"/> => <attribute name="root" value="1.2.3" /> 
	public static Decor createDecorLevel5(Decor decorLevel4) {
		Map<RuleDefinition, List<Attribute>> mapNewAttributes = new HashMap<>();
		DecorVisitor.INSTANCE.visitAndProcess(decorLevel4, new AttrsFlattenImplProvider(), mapNewAttributes);
		
		for (Entry<RuleDefinition, List<Attribute>> rdAndAttribute : mapNewAttributes.entrySet()) {
			rdAndAttribute.getKey().getAttribute().clear();
			rdAndAttribute.getKey().getAttribute().addAll(rdAndAttribute.getValue());
		}
		return CloneUtil.cloneHL7TemplateEl(decorLevel4);
	}
	
	// flatten the valuesets
	public static Decor createDecorLevelFlattenVocab(Decor decorLevel4) {
		return VSFlattener.flattenDecorForValueSets(decorLevel4);
	}

}
