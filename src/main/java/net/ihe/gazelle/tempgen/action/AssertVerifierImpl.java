package net.ihe.gazelle.tempgen.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.tempgen.gui.view.UserQuestionerView;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;

public class AssertVerifierImpl implements AssertVerifier {
	
	
	private static Logger log = LoggerFactory.getLogger(AssertVerifierImpl.class);

	@Override
	public boolean acceptToUseAssert(Assert assertion) {
		if (assertion != null && assertion.getTestFlatten() != null && !assertion.getTestFlatten().trim().isEmpty()) {
			String question = "Do you accept this as assertion : " + assertion.getTestFlatten().trim() + "?";
			return UserQuestionerView.getAcceptanceFromUserForAnAction(question);
		}
		log.error("The system is asking for the user point, but the assertion does not contain a testFlatten !");
		return false;
	}
}
