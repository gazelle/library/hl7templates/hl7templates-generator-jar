package net.ihe.gazelle.tempgen.action;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.goc.template.definer.model.ConstraintsSpec;
import net.ihe.gazelle.goc.uml.utils.OwnedRuleUtil;
import net.ihe.gazelle.goc.uml.utils.PackagedElementUtil;
import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.OwnedRuleKind;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.tempapi.impl.RulesProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;
import net.ihe.gazelle.tempmodel.temp.mapping.utils.TemplatesTypeMappingUtil;

public class RootElementsAnalyzer extends RulesProcessorImpl {
	
	protected Rules currentRules;
	
	protected XMI xmi;
	
	private List<TemplateDefinition> listCDATemplates;
	
	@SuppressWarnings("unchecked")
	@Override
	public void process(Rules t, Object... objects) {
		this.xmi = (XMI) objects[0];
		this.listCDATemplates = (List<TemplateDefinition>) objects[1];
		this.currentRules = t;
		
		if (this.listCDATemplates != null) {
			List<TemplateDefinition> lclin = RootElementsAnalyzer.extractListRootTemplateDefinitions(this.listCDATemplates);
			if (!lclin.isEmpty()) {
				PackagedElement currentPackagedElement = PackagedElementUtil.initPackagedElement();
				currentPackagedElement.setName(this.xmi.getModel().getName().toUpperCase() + "ClinicalDocumentSpec");
				PackagedElementUtil.addGeneralizationToPackagedElement(currentPackagedElement, 
						TemplatesTypeMappingUtil.extractTemplateTypeFromElementNameIfExists("ClinicalDocument", null));
				this.xmi.getModel().getPackagedElement().add(currentPackagedElement);
				OwnedRule or = this.generateOwnedRuleForRoot(lclin, currentPackagedElement.getId());
				currentPackagedElement.getOwnedRule().add(or);
				ConstraintsSpec constraintsSpec = new ConstraintsSpec();
				constraintsSpec.setBaseClass(currentPackagedElement.getId());
				this.xmi.getConstraintsSpec().add(constraintsSpec);
			}
		}
	}
	
	protected OwnedRule generateOwnedRuleForRoot(List<TemplateDefinition> lclin, String contrainedElementId){
		OwnedRule ownedRule = OwnedRuleUtil.initOwnedRule();
		ownedRule.setConstrainedElement(contrainedElementId);
		String specification = this.generateOCLConstraint(lclin);
		String comment = this.generateCommentConstraint(lclin);
		ownedRule.getSpecification().setBody(specification);
		ownedRule.getOwnedComment().setBody(comment);
		ownedRule.setOwnedRuleKind(OwnedRuleKind.FIXEDVAL);
		return ownedRule;
	}

	protected String generateCommentConstraint(List<TemplateDefinition> lclin) {
		if (lclin != null && !lclin.isEmpty()) {
			StringBuilder sb = new StringBuilder("The CDA ClinicalDocument SHALL have a templateId ");
			if (lclin.size()==1) {
				String id = TemplateDefinitionUtil.extractTemplateId(lclin.get(0));
				sb.append("equals to '" + id + "'");
			}
			else {
				sb.append("from this list of ID : { ");
				int i = 0;
				for (TemplateDefinition templateDefinition : lclin) {
					if (i++>0) {
						sb.append(", ");
					}
					String id = TemplateDefinitionUtil.extractTemplateId(templateDefinition);
					sb.append(id);
				}
				sb.append(" }");
			}
			return sb.toString();
		}
		return "";
	}

	protected String generateOCLConstraint(List<TemplateDefinition> lclin) {
		if (lclin != null) {
			StringBuilder sb = new StringBuilder("self.templateId->exists(ii : II | (not ii.root.oclIsUndefined()) and (");
			int i = 0;
			for (TemplateDefinition templateDefinition : lclin) {
				if (i++>0) {
					sb.append(" or ");
				}
				String id = TemplateDefinitionUtil.extractTemplateId(templateDefinition);
				sb.append("(ii.root='" + id + "')");
			}
			sb.append("))");
			ImportElementHandler.handleAddingElement("II");
			return sb.toString();
		}
		return null;
	}

	protected static List<TemplateDefinition> extractListRootTemplateDefinitions(List<TemplateDefinition> listCDATemplates2) {
		List<TemplateDefinition> res = new ArrayList<>();
		if (listCDATemplates2 != null) {
			for (TemplateDefinition templateDefinition : listCDATemplates2) {
				RuleDefinition first = TemplateDefinitionUtil.getFirstElement(templateDefinition);
				if (first.getName().matches("^(.*:)?ClinicalDocument$")) {
					res.add(templateDefinition);
				}
			}
		}
		return res;
	}

}
