package net.ihe.gazelle.tempgen.action;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class FileReadWrite {
	
	private FileReadWrite() {}

	public static void printDoc(String doc, String name) throws IOException {
		try (FileWriter fw = new FileWriter(new File(name))){
			fw.append(doc);
			fw.close();
		}
	}

	public static String readDoc(String name) throws IOException {
		BufferedReader scanner = new BufferedReader(new FileReader(name));
		StringBuilder res = new StringBuilder();
		try {
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		} finally {
			scanner.close();
		}
		return res.toString();
	}

}
