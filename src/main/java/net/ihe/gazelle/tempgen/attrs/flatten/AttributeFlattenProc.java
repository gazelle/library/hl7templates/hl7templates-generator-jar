package net.ihe.gazelle.tempgen.attrs.flatten;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.ihe.gazelle.tempapi.impl.AttributeProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;

/**
 * this class flatten the contain with elements containing the templateId/@root
 * @author Abderrazek Boufahja
 *
 */
public class AttributeFlattenProc extends AttributeProcessorImpl {
	
	private Attribute selectedAttribute;
	
	
	private List<Attribute> listAttributeToAdd = new ArrayList<>();
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void process(Attribute t, Object... objects) {
		if (t != null && t.getParentObject() instanceof RuleDefinition) {
			this.selectedAttribute = t;
			Map<RuleDefinition, List<Attribute>> mapNewAttributes = (Map<RuleDefinition, List<Attribute>>)objects[0];
			super.process(t, objects);
			if (this.selectedAttribute.getName() != null) {
				Attribute newattr = new Attribute();
				newattr.setName(this.selectedAttribute.getName());
				newattr.setValue(this.selectedAttribute.getValue());
				addAttribute(newattr);
			}
			if (!mapNewAttributes.containsKey((RuleDefinition) this.selectedAttribute.getParentObject())) {
				mapNewAttributes.put((RuleDefinition) this.selectedAttribute.getParentObject(), this.listAttributeToAdd);
			}
			else {
				mapNewAttributes.get((RuleDefinition) this.selectedAttribute.getParentObject()).addAll(this.listAttributeToAdd);
			}
		}
		
	}
	
	@Override
	public void processClassCode(String classCode) {
		if (classCode != null) {
			Attribute newattr = new Attribute();
			newattr.setName("classCode");
			newattr.setValue(classCode);
			addAttribute(newattr);
			this.selectedAttribute.setClassCode(null);
		}
	}

	@Override
	public void processContextConductionInd(Boolean contextConductionInd) {
		if (contextConductionInd != null) {
			Attribute newattr = new Attribute();
			newattr.setName("contextConductionInd");
			newattr.setValue(String.valueOf(contextConductionInd));
			addAttribute(newattr);
			this.selectedAttribute.setContextConductionInd(null);
		}
	}
	
	@Override
	public void processContextControlCode(String contextControlCode) {
		if (contextControlCode != null) {
			Attribute newattr = new Attribute();
			newattr.setName("contextControlCode");
			newattr.setValue(String.valueOf(contextControlCode));
			addAttribute(newattr);
			this.selectedAttribute.setContextControlCode(null);
		}
	}
	
	@Override
	public void processDeterminerCode(String determinerCode) {
		if (determinerCode != null) {
			Attribute newattr = new Attribute();
			newattr.setName("determinerCode");
			newattr.setValue(String.valueOf(determinerCode));
			addAttribute(newattr);
			this.selectedAttribute.setDeterminerCode(null);
		}
	}
	
	@Override
	public void processExtension(String extension) {
		if (extension != null) {
			Attribute newattr = new Attribute();
			newattr.setName("extension");
			newattr.setValue(String.valueOf(extension));
			addAttribute(newattr);
			this.selectedAttribute.setExtension(null);
		}
	}
	
	@Override
	public void processIndependentInd(Boolean independentInd) {
		if (independentInd != null) {
			Attribute newattr = new Attribute();
			newattr.setName("independentInd");
			newattr.setValue(String.valueOf(independentInd));
			addAttribute(newattr);
			this.selectedAttribute.setIndependentInd(null);
		}
	}
	
	@Override
	public void processInstitutionSpecified(Boolean institutionSpecified) {
		if (institutionSpecified != null) {
			Attribute newattr = new Attribute();
			newattr.setName("institutionSpecified");
			newattr.setValue(String.valueOf(institutionSpecified));
			addAttribute(newattr);
			this.selectedAttribute.setInstitutionSpecified(null);
		}
	}
	
	@Override
	public void processInversionInd(Boolean inversionInd) {
		if (inversionInd != null) {
			Attribute newattr = new Attribute();
			newattr.setName("inversionInd");
			newattr.setValue(String.valueOf(inversionInd));
			addAttribute(newattr);
			this.selectedAttribute.setInversionInd(null);
		}
	}
	
	@Override
	public void processMediaType(String mediaType) {
		if (mediaType != null) {
			Attribute newattr = new Attribute();
			newattr.setName("mediaType");
			newattr.setValue(String.valueOf(mediaType));
			addAttribute(newattr);
			this.selectedAttribute.setMediaType(null);
		}
	}
	
	@Override
	public void processMoodCode(String moodCode) {
		if (moodCode != null) {
			Attribute newattr = new Attribute();
			newattr.setName("moodCode");
			newattr.setValue(String.valueOf(moodCode));
			addAttribute(newattr);
			this.selectedAttribute.setMoodCode(null);
		}
	}
	
	@Override
	public void processNegationInd(String negationInd) {
		if (negationInd != null) {
			Attribute newattr = new Attribute();
			newattr.setName("negationInd");
			newattr.setValue(String.valueOf(negationInd));
			addAttribute(newattr);
			this.selectedAttribute.setNegationInd(null);
		}
	}
	
	@Override
	public void processNullFlavor(String nullFlavor) {
		if (nullFlavor != null) {
			Attribute newattr = new Attribute();
			newattr.setName("nullFlavor");
			newattr.setValue(String.valueOf(nullFlavor));
			addAttribute(newattr);
			this.selectedAttribute.setNullFlavor(null);
		}
	}
	
	@Override
	public void processOperator(String operator) {
		if (operator != null) {
			Attribute newattr = new Attribute();
			newattr.setName("operator");
			newattr.setValue(String.valueOf(operator));
			addAttribute(newattr);
			this.selectedAttribute.setOperator(null);
		}
	}
	
	@Override
	public void processQualifier(String qualifier) {
		if (qualifier != null) {
			Attribute newattr = new Attribute();
			newattr.setName("qualifier");
			newattr.setValue(String.valueOf(qualifier));
			addAttribute(newattr);
			this.selectedAttribute.setQualifier(null);
		}
	}
	
	@Override
	public void processRepresentation(String representation) {
		if (representation != null) {
			Attribute newattr = new Attribute();
			newattr.setName("representation");
			newattr.setValue(String.valueOf(representation));
			addAttribute(newattr);
			this.selectedAttribute.setRepresentation(null);
		}
	}
	
	@Override
	public void processRoot(String root) {
		if (root != null) {
			Attribute newattr = new Attribute();
			newattr.setName("root");
			newattr.setValue(String.valueOf(root));
			addAttribute(newattr);
			this.selectedAttribute.setRoot(null);
		}
	}
	
	@Override
	public void processTypeCode(String typeCode) {
		if (typeCode != null) {
			Attribute newattr = new Attribute();
			newattr.setName("typeCode");
			newattr.setValue(String.valueOf(typeCode));
			addAttribute(newattr);
			this.selectedAttribute.setTypeCode(null);
		}
	}
	
	@Override
	public void processUnit(String unit) {
		if (unit != null) {
			Attribute newattr = new Attribute();
			newattr.setName("unit");
			newattr.setValue(String.valueOf(unit));
			addAttribute(newattr);
			this.selectedAttribute.setUnit(null);
		}
	}
	
	@Override
	public void processUse(String use) {
		if (use != null) {
			Attribute newattr = new Attribute();
			newattr.setName("use");
			newattr.setValue(String.valueOf(use));
			addAttribute(newattr);
			this.selectedAttribute.setUse(null);
		}
	}
	
	private void addAttribute(Attribute newattr) {
		newattr.getVocabulary().addAll(this.selectedAttribute.getVocabulary());
		if (this.selectedAttribute.isIsOptional() != null) {
			newattr.setIsOptional(this.selectedAttribute.isIsOptional());
		}
		if (this.selectedAttribute.isProhibited() != null && this.selectedAttribute.isProhibited()) {
			newattr.setProhibited(this.selectedAttribute.isProhibited());
		}
		if (this.selectedAttribute.getDatatype() != null) {
			newattr.setDatatype(this.selectedAttribute.getDatatype());
		}
		if (this.selectedAttribute.getItem() != null) {
			newattr.setItem(this.selectedAttribute.getItem());
		}
		if (!this.selectedAttribute.getDesc().isEmpty()) {
			newattr.getDesc().clear();
			newattr.getDesc().addAll(this.selectedAttribute.getDesc());
		}
		if (!this.selectedAttribute.getVocabulary().isEmpty()) {
			newattr.getVocabulary().clear();
			newattr.getVocabulary().addAll(this.selectedAttribute.getVocabulary());
		}
		if (!this.selectedAttribute.getConstraint().isEmpty()) {
			newattr.getConstraint().clear();
			newattr.getConstraint().addAll(this.selectedAttribute.getConstraint());
		}
		this.listAttributeToAdd.add(newattr);
	}
	
}
