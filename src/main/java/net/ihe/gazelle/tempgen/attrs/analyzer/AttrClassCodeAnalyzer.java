package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrClassCodeAnalyzer implements ATConstraintGenerator {

	/**
	 *   attribute.classCode not null
	 */
	@Override
	public String generateOCLConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateOCLConstraintForEnumerationAttribute(attribute, "classCode", attribute.getClassCode(), getProcessIdentifier());
	}

	/**
	 *   attribute.classCode not null
	 */
	@Override
	public String generateCommentConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateCommentConstraint(attribute, "classCode", attribute.getClassCode());
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_CLASSCODE_PROCESS.getValue();
	}

}
