package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrInstitutionSpecifiedAnalyzer implements ATConstraintGenerator {

	/**
	 *   attribute.institutionSpecified != null
	 */
	@Override
	public String generateOCLConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateOCLConstraintFromBooleanOrIntegerAttribute(attribute, "institutionSpecified", String.valueOf(attribute.isInstitutionSpecified()));
	}

	@Override
	public String generateCommentConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateCommentConstraint(attribute, "institutionSpecified", attribute.isInstitutionSpecified());
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_INSTRITUTION_SPECIFIED_PROCESS.getValue();
	}

}
