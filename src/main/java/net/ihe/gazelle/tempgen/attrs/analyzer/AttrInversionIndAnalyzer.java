package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrInversionIndAnalyzer implements ATConstraintGenerator {

	/**
	 *   attribute.inversionInd != null
	 */
	@Override
	public String generateOCLConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateOCLConstraintFromBooleanOrIntegerAttribute(attribute, "inversionInd", String.valueOf(attribute.isInversionInd()));
	}
	
	@Override
	public String generateCommentConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateCommentConstraint(attribute, "inversionInd", attribute.isInversionInd());
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_INVERSION_IND_PROCESS.getValue();
	}

}
