package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempgen.action.ImportElementHandler;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AttributeUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.VocabularyUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrVocabEnumVocabAnalyzer extends AttrVocabAnalyzer implements ATConstraintGenerator {

	/**
	 * the attribute is of type string
	 * @param vocabulary {@link Vocabulary}
	 * @param attribute {@link Attribute}
	 * @return string
	 */
	@Override
	protected String extractConstraintForVocabulary(Vocabulary vocabulary, Attribute attribute) {
		String attrName = AttributeUtil.getBetterMatchingName(attribute);
		String res = "";
		if (vocabulary.getValueSet() != null) {
			String oid = VocabularyUtil.getRealValueSetOid(vocabulary.getValueSet(), 
					AttributeUtil.getParentTemplateDefinition(attribute).getParentObject().getParentObject());
			if (oid == null){
				ProblemHandler.handleAttributeError(attribute, attrName, null, 
						"The value set provided in not a valid OID : " + vocabulary.getValueSet(), AttrAnalyzerEnum.ATTR_VOCAB_PROCESS.getValue());
				return null;
			}
			if (vocabulary.getFlexibility() != null && !vocabulary.getFlexibility().equals("dynamic")) {
				oid = oid + "&version=" + vocabulary.getFlexibility();
			}
			res = "CommonOperationsStatic::matchesCodeToValueSet('" + oid + "', CommonOperationsStatic::stringValueOf(aa))";
			ImportElementHandler.handleAddingElement("CommonOperationsStatic");
		}
		else if (vocabulary.getCode() != null){
			res = "CommonOperationsStatic::stringValueOf(aa)='" + vocabulary.getCode() + "'";
			ImportElementHandler.handleAddingElement("CommonOperationsStatic");
		}
		else {
			ProblemHandler.handleAttributeError(attribute, attrName, null,
					"Not able to handle vocabulary (ValueSet and code are null)", AttrAnalyzerEnum.ATTR_VOCAB_PROCESS.getValue());
		}
		return res;
	}

}
