package net.ihe.gazelle.tempgen.attrs.analyzer;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AttributeUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.FreeFormMarkupWithLanguageUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrConstraintAnalyzer implements ATConstraintGenerator {
	
	private FreeFormMarkupWithLanguage constraint;

	public FreeFormMarkupWithLanguage getConstraint() {
		return constraint;
	}

	public void setConstraint(FreeFormMarkupWithLanguage constraint) {
		this.constraint = constraint;
	}

	@Override
	public String generateOCLConstraint(Attribute attribute) {
		return "true --there is not way to test the constraint";
	}

	@Override
	public String generateCommentConstraint(Attribute attribute) {
		String templateName = AttributeUtil.getParentTemplateDefinition(attribute).getDisplayName();
		DParent dparent = AttributeUtil.getDParentOfAttibute(attribute, AttributeUtil.getBetterMatchingName(attribute), 
				AttributeUtil.getBetterMatchingValue(attribute));
		String attrPath = DPathExtractor.createPathFromDParent(dparent);
		return "In " + templateName + ", in " + attrPath + ", " + FreeFormMarkupWithLanguageUtil.extractContentAsString(constraint);
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_CONS_PROCESS.getValue();
	}
	
	public static List<FreeFormMarkupWithLanguage> extractListUtilConstraints(List<FreeFormMarkupWithLanguage> constraint) {
		List<FreeFormMarkupWithLanguage> lff = new ArrayList<>();
		for (FreeFormMarkupWithLanguage freeFormMarkupWithLanguage : constraint) {
			String content = FreeFormMarkupWithLanguageUtil.extractContentAsString(freeFormMarkupWithLanguage);
			if (content != null && !"".equals(content.trim())) {
				lff.add(freeFormMarkupWithLanguage);
			}
		}
		return lff;
	}

}
