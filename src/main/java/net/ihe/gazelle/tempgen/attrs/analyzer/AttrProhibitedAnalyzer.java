package net.ihe.gazelle.tempgen.attrs.analyzer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.tempgen.action.OCLCleaner;
import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AttributeUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrProhibitedAnalyzer implements ATConstraintGenerator {
	
	private static Logger log = LoggerFactory.getLogger(AttrProhibitedAnalyzer.class);
	
	private String attrName;

	public String getAttrName() {
		return attrName;
	}

	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	@Override
	public String generateOCLConstraint(Attribute attribute) {
		String parentOCL = (new OCLGenerator()).generateRuleToParentFromPath(attribute);
		String res = parentOCL;
		Integer max = AttributeUtil.getUMLMaxAttribute(attribute, this.attrName);
		if (max != 1 && max != -1){
			log.error("The maximum attribute is not what is waited (AttrProhibitedAnalyzer-generateOCLConstraint): "
					+ AttributeUtil.getJavaPath(attribute, this.attrName));
		}
		if (max == 1){
			if (res.equals("self")){
				res = "self." + this.attrName + ".oclIsUndefined()";
			}
			else {
				res = res + "->forAll(" + this.attrName + ".oclIsUndefined())";
			}
		}
		else if (max == -1){
			res = res + "." + this.attrName + OCLCleaner.generateRejectOcl(false) + "->isEmpty()";
		}
		return res;
	}

	@Override
	public String generateCommentConstraint(Attribute attribute) {
		String templateName = AttributeUtil.getParentTemplateDefinition(attribute).getDisplayName();
		DParent dparent = AttributeUtil.getDParentOfTheParent(attribute);
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		return "In " + templateName + ", in " + parentPath + ", the attribute " + this.attrName + " SHALL not be provided (prohibited=true)";
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_PROHIBITED_PROCESS.getValue();
	}

}
