package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AttributeUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrOptionalityAnalyzer implements ATConstraintGenerator {
	
	private String attrName;

	public String getAttrName() {
		return attrName;
	}

	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	/**
	 *   attribute.classCode not null
	 */
	@Override
	public String generateOCLConstraint(Attribute attribute) {
		String parentOCL = (new OCLGenerator()).generateRuleToParentFromPath(attribute);
		String res = parentOCL;
		Integer max = AttributeUtil.getUMLMaxAttribute(attribute, this.attrName);
		if (max == null) {
			ProblemHandler.handleAttributeError(attribute, this.attrName, null, 
					"Problem to generate OCL because of null max value", AttrAnalyzerEnum.ATTR_PROCESS.getValue());
			return res;
		}
		if (max == 1){
			res = updateOCLIfAttrUnique(res);
		}
		else if (max == -1){
			res = updateOCLIfAttrMultiple(res);
		}
		return res;
	}

	private String updateOCLIfAttrMultiple(String resParam) {
		String res = resParam;
		if (resParam.equals("self")){
			res = "(not self.nullFlavor.oclIsUndefined()) or (not self." + this.attrName + "->size()>0)";
		}
		else {
			res = res + "->forAll((not nullFlavor.oclIsUndefined()) or " + this.attrName + "->size()>0)";
		}
		return res;
	}

	private String updateOCLIfAttrUnique(String resParam) {
		String res = resParam;
		if (res.equals("self")){
			if (!this.attrName.equals("nullFlavor")) {
				res = "(not self.nullFlavor.oclIsUndefined()) or (not self." + this.attrName + ".oclIsUndefined())";
			}
			else {
				res = "not self." + this.attrName + ".oclIsUndefined()";
			}
		}
		else {
			if (!this.attrName.equals("nullFlavor")) {
				res = res + "->forAll((not nullFlavor.oclIsUndefined()) or (not " + this.attrName + ".oclIsUndefined()))";
			}
			else {
				res = res + "->forAll(not " + this.attrName + ".oclIsUndefined())";
			}
		}
		return res;
	}

	/**
	 *   attribute.classCode not null
	 */
	@Override
	public String generateCommentConstraint(Attribute attribute) {
		String templateName = AttributeUtil.getParentTemplateDefinition(attribute).getDisplayName();
		DParent dparent = AttributeUtil.getDParentOfTheParent(attribute);
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		return "In " + templateName + ", in " + parentPath + ", the attribute " + this.attrName + " SHALL be present";
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_OPT_PROCESS.getValue();
	}

}
