package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempgen.action.ImportElementHandler;
import net.ihe.gazelle.tempgen.action.OCLCleaner;
import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempmodel.decor.dt.model.AttDatatype;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AttributeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Abderrazek Boufahja
 */
public class AttrDatatypeAnalyzer implements ATConstraintGenerator {


    private static Logger log = LoggerFactory.getLogger(AttrDatatypeAnalyzer.class);

    private AttDatatype datatype;

    public AttDatatype getDatatype() {
        return datatype;
    }

    public void setDatatype(AttDatatype datatype) {
        this.datatype = datatype;
    }

    /**
     *  : attr.name not null
     *   this.datatype not null
     */
    @Override
    public String generateOCLConstraint(Attribute attribute) {
        if (this.datatype == null) {
            this.datatype = AttDatatype.getAttDatatypeByName(attribute.getDatatype().getLocalPart());
        }
        String parentOCL = (new OCLGenerator()).generateRuleToParentFromPath(attribute);
        String res = parentOCL;
        Integer max = AttributeUtil.getUMLMaxAttribute(attribute, attribute.getName());
        if (max != 1 && max != -1) {
            log.error("The maximum attribute is not what is waited (AttrDatatypeAnalyzer-generateOCLConstraint): "
                    + AttributeUtil.getJavaPath(attribute, attribute.getName()));
        }
        if (max == 1) {
            if (res.equals("self")) {
                res = "self." + attribute.getName() + ".oclIsUndefined() or CommonOperationsStatic::";
                if (this.datatype.getOperationName() != null) {
                    res = res + this.datatype.getOperationName() + "(self." + attribute.getName() + addToString(attribute) + ")";
                } else if (this.datatype.getRegex() != null) {
                    res = res + "matches(self." + attribute.getName() + addToString(attribute) + ", '" + this.datatype.getRegexForJavafile() + "')";
                }
            } else {
                res = res + "->forAll(" + attribute.getName() + ".oclIsUndefined() or CommonOperationsStatic::";
                if (this.datatype.getOperationName() != null) {
                    res = res + this.datatype.getOperationName() + "(" + attribute.getName() + addToString(attribute) + "))";
                } else if (this.datatype.getRegex() != null) {
                    res = res + "matches(" + attribute.getName() + addToString(attribute) + ", '" + this.datatype.getRegexForJavafile() + "'))";
                }
            }
        } else if (max == -1) {
            res = res + "." + attribute.getName() + OCLCleaner.generateRejectOcl(false) + "->forAll(aa | CommonOperationsStatic::";
            if (this.datatype.getOperationName() != null) {
                res = res + this.datatype.getOperationName() + "(aa" + addToString(attribute) + "))";
            } else if (this.datatype.getRegex() != null) {
                res = res + "matches(aa" + addToString(attribute) + ", '" + this.datatype.getRegexForJavafile() + "'))";
            }
        }
        ImportElementHandler.handleAddingElement("CommonOperationsStatic");
        return res;
    }

    private String addToString(Attribute attribute) {
        String type = AttributeUtil.getUMLTypeName(attribute, attribute.getName());
        if (type != null && !type.equals("String") && !type.equals("Integer") && !type.equals("Double")) {
            return ".toString()";
        }
        return "";
    }

    @Override
    public String generateCommentConstraint(Attribute attribute) {
        if (this.datatype == null) {
            this.datatype = AttDatatype.getAttDatatypeByName(attribute.getDatatype().getLocalPart());
        }
        String templateName = AttributeUtil.getParentTemplateDefinition(attribute).getDisplayName();
        DParent dparent = AttributeUtil.getDParentOfTheParent(attribute);
        String parentPath = DPathExtractor.createPathFromDParent(dparent);
        return "In " + templateName + ", in " + parentPath + ", the attribute " + attribute.getName() + " SHALL have the datatype '" +
                datatype.getName() + "' if present";
    }

    @Override
    public String getProcessIdentifier() {
        return AttrAnalyzerEnum.ATTR_DT_PROCESS.getValue();
    }

}
