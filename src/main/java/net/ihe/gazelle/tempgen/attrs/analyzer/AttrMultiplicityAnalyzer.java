package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempgen.action.OCLCleaner;
import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AttributeUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrMultiplicityAnalyzer implements ATConstraintGenerator {

	/**
	 *   attribute.classCode not null
	 */
	@Override
	public String generateOCLConstraint(Attribute attribute) {
		String parentOCL = (new OCLGenerator()).generateRuleToParentFromPath(attribute);
		return parentOCL + "." + AttributeUtil.getBetterMatchingName(attribute) + OCLCleaner.generateRejectOcl(false) + "->size()<2";
	}

	/**
	 *   attribute.classCode not null
	 */
	@Override
	public String generateCommentConstraint(Attribute attribute) {
		String templateName = AttributeUtil.getParentTemplateDefinition(attribute).getDisplayName();
		DParent dparent = AttributeUtil.getDParentOfTheParent(attribute);
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		return "In " + templateName + ", in " + parentPath + ", the attribute " + AttributeUtil.getBetterMatchingName(attribute) + 
				" SHALL have at most one value";
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_MULTI_PROCESS.getValue();
	}

}
