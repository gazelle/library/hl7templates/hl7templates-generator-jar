package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public interface ATConstraintGenerator {
	
	String generateOCLConstraint(Attribute attribute);
	
	String generateCommentConstraint(Attribute attribute);
	
	String getProcessIdentifier();

}
