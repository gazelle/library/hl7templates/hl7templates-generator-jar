package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrMoodCodeAnalyzer implements ATConstraintGenerator {

	/**
	 *   attribute.moodCode not null
	 */
	@Override
	public String generateOCLConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateOCLConstraintForEnumerationAttribute(attribute, "moodCode", attribute.getMoodCode(), getProcessIdentifier());
	}

	/**
	 *   attribute.moodCode not null
	 */
	@Override
	public String generateCommentConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateCommentConstraint(attribute, "moodCode", attribute.getMoodCode());
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_MOODCODE_PROCESS.getValue();
	}

}
