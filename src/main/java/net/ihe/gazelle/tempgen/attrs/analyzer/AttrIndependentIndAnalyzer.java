package net.ihe.gazelle.tempgen.attrs.analyzer;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrIndependentIndAnalyzer implements ATConstraintGenerator {

	/**
	 *   attribute.independentInd != null
	 */
	@Override
	public String generateOCLConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateOCLConstraintFromBooleanOrIntegerAttribute(attribute, "independentInd", String.valueOf(attribute.isIndependentInd()));
	}

	@Override
	public String generateCommentConstraint(Attribute attribute) {
		return AttrProcessorUtil.generateCommentConstraint(attribute, "independentInd", attribute.isIndependentInd());
	}

	@Override
	public String getProcessIdentifier() {
		return AttrAnalyzerEnum.ATTR_INDEPENDENTIND_PROCESS.getValue();
	}

}
