package net.ihe.gazelle.tempgen.flatten.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateStatusCodeLifeCycle;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 * cannot be marked as final because extended by test class
 *
 */
public class RulesCleaner {
	
	
	private static Logger log = LoggerFactory.getLogger(RulesCleaner.class);
	
	private static List<TemplateStatusCodeLifeCycle> listStatus = Arrays.asList(TemplateStatusCodeLifeCycle.ACTIVE, TemplateStatusCodeLifeCycle.PENDING, TemplateStatusCodeLifeCycle.REVIEW, 
			TemplateStatusCodeLifeCycle.DRAFT, TemplateStatusCodeLifeCycle.RETIRED, TemplateStatusCodeLifeCycle.CANCELLED, 
			TemplateStatusCodeLifeCycle.REJECTED);
	
	public static void cleanRules(Rules rules) {
		if (rules != null) {
			List<TemplateDefinition> ltd = RulesUtil.getTemplates(rules);
			if (ltd != null) {
				List<TemplateDefinition> ltdcleaned = new ArrayList<>(100);
				ltdcleaned.addAll(ltd);
				for (TemplateDefinition templateDefinition : ltd) {
					boolean thereAreABetterTemplate = verifyIfThereAreABetterTemplateStatus(templateDefinition, ltdcleaned);
					if (thereAreABetterTemplate) {
						rules.getTemplateAssociationOrTemplate().remove(templateDefinition);
						ltdcleaned.remove(templateDefinition);
						log.info("template duplicated : " + templateDefinition.getId());
					}
				}
			}
		}
	}

	protected static boolean verifyIfThereAreABetterTemplateStatus(TemplateDefinition templateDefinition, List<TemplateDefinition> ltdcleaned) {
		TemplateStatusCodeLifeCycle testedStatus = TemplateDefinitionUtil.getRealStatusCode(templateDefinition);
		List<TemplateDefinition> ltequal = TemplateDefinitionUtil.extractListTemplateDefinitionById(ltdcleaned, templateDefinition.getId());
		if (ltequal != null) {
			for (TemplateDefinition templateDefinition2 : ltequal) {
				TemplateStatusCodeLifeCycle statusCurrent = TemplateDefinitionUtil.getRealStatusCode(templateDefinition2);
				if (listStatus.indexOf(statusCurrent)<listStatus.indexOf(testedStatus)) {
					return true;
				}
				else if (listStatus.indexOf(statusCurrent)==listStatus.indexOf(testedStatus) && templateDefinition != templateDefinition2) {
					ProblemHandler.handleError("There are a problem in template treating, two templates have the same high statusCode, "
							+ "we don't know which one the useful", AnalyzerEnum.TEMPLATE_PROCESS.getValue());
					return true;
				}
			}
		}
		return false;
	}

}
