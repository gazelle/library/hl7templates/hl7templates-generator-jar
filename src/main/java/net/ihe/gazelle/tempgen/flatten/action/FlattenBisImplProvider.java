package net.ihe.gazelle.tempgen.flatten.action;

import net.ihe.gazelle.tempapi.interfaces.ContainDefinitionProcessor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempapi.utils.Processor;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class FlattenBisImplProvider  implements ImplProvider {
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public <T extends Processor> T provideImpl(Class<T> t) {
		if (t.equals(ContainDefinitionProcessor.class)){
			return (T) new ContainFlatten2Proc();
		}
		return null;
	}

}
