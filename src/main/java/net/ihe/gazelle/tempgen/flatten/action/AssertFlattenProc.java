package net.ihe.gazelle.tempgen.flatten.action;

import net.ihe.gazelle.tempapi.impl.AssertProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AssertFlattenProc extends AssertProcessorImpl {
	
	
	@Override
	public void process(Assert t, Object... objects) {
		Assert currentAssert = t;
		if (currentAssert.getParentObject() instanceof TemplateDefinition) {
			TemplateDefinition parent  = (TemplateDefinition)currentAssert.getParentObject();
			if (TemplateDefinitionUtil.templateDefinitionIsCDATemplate(parent)) {
				RuleDefinition first = TemplateDefinitionUtil.getFirstElement(parent);
				parent.getAttributeOrChoiceOrElement().remove(currentAssert);
				first.getLetOrAssertOrReport().add(currentAssert);
				currentAssert.setParentObject(first);
			}
		}
	}

}
