package net.ihe.gazelle.tempgen.flatten.action;

import net.ihe.gazelle.tempapi.impl.RuleDefinitionProcessorImpl;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.statistics.RDNameAsXpath;
import net.ihe.gazelle.tempmodel.dpath.model.DAttribute;
import net.ihe.gazelle.tempmodel.dpath.model.DElement;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.*;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.VocabularyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author Abderrazek Boufahja
 */
public class RuleDefinitionFlattenProc extends RuleDefinitionProcessorImpl {


    private static Logger log = LoggerFactory.getLogger(RuleDefinitionFlattenProc.class);

    protected RuleDefinition currentRuleDefinition;

    protected RDNameAsXpath currentRdNameAsXpath = new RDNameAsXpath();

    public RuleDefinition getCurrentRuleDefinition() {
        return currentRuleDefinition;
    }

    protected void setCurrentRuleDefinition(RuleDefinition currentRuleDefinition) {
        this.currentRuleDefinition = currentRuleDefinition;
    }

    @Override
    public void process(RuleDefinition t, Object... objects) {
        this.currentRuleDefinition = t;
        super.process(t, objects);
    }

    @Override
    public void processVocabularys(List<Vocabulary> vocabularys) {
        if (vocabularys != null && vocabularys.size() == 1 && (vocabularys.get(0).getValueSet() == null) &&
                VocabularyUtil.vocabularyTrimmedCodeIsNotEmpty(vocabularys.get(0))) {
            DParent dpar = RuleDefinitionUtil.getDParentOfRuleDefinition(currentRuleDefinition);
            String dpath = DPathExtractor.createPathFromDParent(dpar);
            if (RuleDefinitionUtil.getAttributeByName(currentRuleDefinition, "code") != null) {
                log.error("A code attribute is already defined, there is no need to define a vocabulary for the code : {}", dpath);
                return;
            }
            if (RuleDefinitionUtil.verifyIfRDIsACodeDT(this.currentRuleDefinition)) {
                Attribute attr = new Attribute();
                attr.setName("code");
                attr.setValue(vocabularys.get(0).getCode());
                attr.setIsOptional(false);
                attr.setParentObject(currentRuleDefinition);
                currentRuleDefinition.getAttribute().add(attr);
                if (vocabularys.get(0).getCodeSystem() != null) {
                    Attribute attr2 = new Attribute();
                    attr2.setName("codeSystem");
                    attr2.setValue(vocabularys.get(0).getCodeSystem());
                    attr2.setIsOptional(true);
                    attr2.setParentObject(currentRuleDefinition);
                    currentRuleDefinition.getAttribute().add(attr2);
                }
                this.currentRuleDefinition.getVocabulary().remove(vocabularys.get(0));
            }
        }
    }

    @Override
    public void processContains(String contains) {
        if (contains != null) {
            TemplateDefinition parentTemplate = RuleDefinitionUtil.getParentTemplateDefinition(currentRuleDefinition);
            String identifier = RulesUtil.getReferenceTemplateIdentifier(parentTemplate.getParentObject(), contains);
            if (identifier != null) {
                if (TemplateDefinitionUtil.referencedTemplateIsCDA(identifier, parentTemplate.getParentObject().getParentObject())) {
                    ContainDefinition cd = new ContainDefinition();
                    cd.setRef(identifier);
                    cd.setParentObject(this.currentRuleDefinition);
                    cd.setMinimumMultiplicity(currentRuleDefinition.getMinimumMultiplicity());
                    cd.setMaximumMultiplicity(currentRuleDefinition.getMaximumMultiplicity());
                    cd.setIsMandatory(currentRuleDefinition.getIsMandatory());
                    this.currentRuleDefinition.getLetOrAssertOrReport().add(cd);
                    this.currentRuleDefinition.setContains(null);
                } else {
                    IncludeDefinition inc = new IncludeDefinition();
                    inc.setRef(contains);
                    inc.setParentObject(this.currentRuleDefinition);
                    this.currentRuleDefinition.getLetOrAssertOrReport().add(inc);
                    this.currentRuleDefinition.setContains(null);
                }
            } else {
                ProblemHandler.handleRuleDefinitionError(currentRuleDefinition, "The contain attribute does not reference any known Template",
                        FlattenAnalyzerEnum.CONTAINS_FLATTEN_PROCESS.getValue());
            }
        }
    }

    @Override
    public void processName(String nameParam) {
        String name = nameParam;
        if (name == null) {
            return;
        }
        name = "/" + name;
        if (name.matches(DPathExtractor.PATH_REGEX_COMPLETE)) {
            this.currentRdNameAsXpath.setValue(name);
            if (name.matches(".*(\\[|\\]|\\@|=).*")) {
                DParent userDParent = DPathExtractor.extractDElementFromDPath(name);
                if (userDParent != null) {
                    this.currentRdNameAsXpath.setProcessed(true);
                    flattenDecorNameForDParent(userDParent);
                } else {
                    this.currentRdNameAsXpath.setProcessed(false);
                    ProblemHandler.handleRuleDefinitionError(currentRuleDefinition,
                            "The name specified cannot be processed by the tool : " + name,
                            FlattenAnalyzerEnum.RD_FLATTEN_PROCESS.getValue());
                }
            }
        } else {
            this.currentRdNameAsXpath.setProcessed(false);
            ProblemHandler.handleRuleDefinitionError(currentRuleDefinition,
                    "The name specified cannot be processed by the tool : " + name,
                    FlattenAnalyzerEnum.RD_FLATTEN_PROCESS.getValue());
        }
    }

    /**
     * @param userDParent : the DParent extracted from the name of the ruleDefinition
     */
    protected void flattenDecorNameForDParent(DParent userDParent) {
        String name = DPathExtractor.createPathFromDParent(userDParent);
        if (userDParent instanceof DAttribute) {
            ProblemHandler.handleRuleDefinitionError(currentRuleDefinition,
                    "This case is not possible, in a ruleDefinition the name shall refer an element not "
                            + "an attribute", FlattenAnalyzerEnum.RD_FLATTEN_PROCESS.getValue());
            return;
        }
        DElement dElement = (DElement) userDParent;
        Boolean verif = RuleDefinitionUtil.verifyIfDElementConvertible(dElement, this.currentRuleDefinition);
        if (verif) {
            this.currentRdNameAsXpath.setPathIsIgnorable(true);
            this.currentRuleDefinition.setName(RuleDefinitionUtil.getRealNameOfRuleDefinition(currentRuleDefinition));
        } else {
            this.currentRdNameAsXpath.setPathIsIgnorable(false);
            if (dElement.getFollowingAttributeOrElement() == null) {
                if (RuleDefinitionUtil.includingDParentInRuleDefinitionIsPossible(
                        dElement.getDistinguisherAttributeOrElement(), this.currentRuleDefinition)) {
                    RuleDefinitionUtil.includeDParentInRuleDefinition(dElement.getDistinguisherAttributeOrElement(),
                            this.currentRuleDefinition);
                    this.currentRdNameAsXpath.setPathIsConvertible(true);
                    this.currentRuleDefinition.setName(RuleDefinitionUtil.getRealNameOfRuleDefinition(currentRuleDefinition));
                } else {
                    this.currentRdNameAsXpath.setPathIsConvertible(false);
                    ProblemHandler.handleRuleDefinitionError(currentRuleDefinition,
                            "The name specified cannot be processed by the tool, because "
                                    + "it contains a path to existing elements : " + name,
                            FlattenAnalyzerEnum.RD_FLATTEN_PROCESS.getValue());
                }
            } else {
                this.currentRdNameAsXpath.setPathIsConvertible(false);
                ProblemHandler.handleRuleDefinitionError(currentRuleDefinition,
                        "The name specified contains a path to a non direct element : " + name,
                        FlattenAnalyzerEnum.RD_FLATTEN_PROCESS.getValue());
            }
        }
    }

}
