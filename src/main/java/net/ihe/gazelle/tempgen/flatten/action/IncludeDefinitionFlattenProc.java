package net.ihe.gazelle.tempgen.flatten.action;

import net.ihe.gazelle.tempapi.impl.IncludeDefinitionProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * @author Abderrazek Boufahja
 */
public class IncludeDefinitionFlattenProc extends IncludeDefinitionProcessorImpl {

    private IncludeDefinition currentIncludeDefinition;

    public IncludeDefinition getCurrentIncludeDefinition() {
        return currentIncludeDefinition;
    }

    protected void setCurrentIncludeDefinition(IncludeDefinition currentIncludeDefinition) {
        this.currentIncludeDefinition = currentIncludeDefinition;
    }

    @Override
    public void process(IncludeDefinition t, Object... objects) {
        this.currentIncludeDefinition = t;
        if (this.currentIncludeDefinition.getParentObject() instanceof TemplateDefinition) {
            TemplateDefinition parentTemplate = (TemplateDefinition) this.currentIncludeDefinition.getParentObject();
            flattenIncludeInTemplateDefinition(parentTemplate);
        } else if (this.currentIncludeDefinition.getParentObject() instanceof RuleDefinition) {
            RuleDefinition parentRD = (RuleDefinition) this.currentIncludeDefinition.getParentObject();
            flattenIncludeInRuleDefinition(parentRD);
        } else if (this.currentIncludeDefinition.getParentObject() instanceof ChoiceDefinition) {
            ChoiceDefinition parentCD = (ChoiceDefinition) this.currentIncludeDefinition.getParentObject();
            flattenIncludeInChoiceDefinition(parentCD);
        }
    }

    protected void flattenIncludeInChoiceDefinition(ChoiceDefinition parentCD) {
        TemplateDefinition parentTemplate = ChoiceDefinitionUtil.getParentTemplateDefinition(parentCD);
        String identifier = RulesUtil.getReferenceTemplateIdentifier(parentTemplate.getParentObject(), this.currentIncludeDefinition.getRef());
        if (TemplateDefinitionUtil.referencedTemplateIsCDA(identifier, parentTemplate.getParentObject().getParentObject())) {
            ContainDefinition cd = new ContainDefinition();
            cd.setRef(identifier);
            cd.setIsMandatory(this.currentIncludeDefinition.getIsMandatory());
            cd.setItem(this.currentIncludeDefinition.getItem());
            cd.setMaximumMultiplicity(this.currentIncludeDefinition.getMaximumMultiplicity());
            cd.setMinimumMultiplicity(this.currentIncludeDefinition.getMinimumMultiplicity());
            cd.setParentObject(parentCD);
            cd.setContain(false);
            parentCD.getIncludeOrElementOrConstraint().add(cd);
            parentCD.getIncludeOrElementOrConstraint().remove(this.currentIncludeDefinition);
        }
    }

    protected void flattenIncludeInRuleDefinition(RuleDefinition parentRD) {
        TemplateDefinition parentTemplate = RuleDefinitionUtil.getParentTemplateDefinition(parentRD);
        String identifier = RulesUtil.getReferenceTemplateIdentifier(parentTemplate.getParentObject(), this.currentIncludeDefinition.getRef());
        if (TemplateDefinitionUtil.referencedTemplateIsCDA(identifier, parentTemplate.getParentObject().getParentObject())) {
            ContainDefinition cd = new ContainDefinition();
            cd.setRef(identifier);
            cd.setIsMandatory(this.currentIncludeDefinition.getIsMandatory());
            cd.setItem(this.currentIncludeDefinition.getItem());
            cd.setMaximumMultiplicity(this.currentIncludeDefinition.getMaximumMultiplicity());
            cd.setMinimumMultiplicity(this.currentIncludeDefinition.getMinimumMultiplicity());
            cd.setParentObject(parentRD);
            cd.setContain(false);
            parentRD.getLetOrAssertOrReport().add(cd);
            parentRD.getLetOrAssertOrReport().remove(this.currentIncludeDefinition);
        }
    }

    protected void flattenIncludeInTemplateDefinition(TemplateDefinition parentTemplate) {
        String identifier = RulesUtil.getReferenceTemplateIdentifier(parentTemplate.getParentObject(), this.currentIncludeDefinition.getRef());
        if (TemplateDefinitionUtil.referencedTemplateIsCDA(identifier, parentTemplate.getParentObject().getParentObject())) {
            ContainDefinition cd = new ContainDefinition();
            cd.setRef(identifier);
            cd.setIsMandatory(this.currentIncludeDefinition.getIsMandatory());
            cd.setItem(this.currentIncludeDefinition.getItem());
            cd.setMaximumMultiplicity(this.currentIncludeDefinition.getMaximumMultiplicity());
            cd.setMinimumMultiplicity(this.currentIncludeDefinition.getMinimumMultiplicity());
            cd.setParentObject(parentTemplate);
            cd.setContain(false);
            parentTemplate.getAttributeOrChoiceOrElement().add(cd);
            parentTemplate.getAttributeOrChoiceOrElement().remove(this.currentIncludeDefinition);
        }
    }


}
