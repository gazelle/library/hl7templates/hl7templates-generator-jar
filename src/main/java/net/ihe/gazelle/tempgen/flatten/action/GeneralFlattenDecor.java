package net.ihe.gazelle.tempgen.flatten.action;

import net.ihe.gazelle.tempapi.visitor.DecorVisitor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.CloneUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class GeneralFlattenDecor {
	
	private GeneralFlattenDecor() {}
	
	/**
	 * transform include and contains to contain 
	 * @param decor {@link Decor}
	 * @return transformation of include and contains to contain
	 */
	public static Decor generalFlattenDecor(Decor decor){
		if (decor != null) {
			DecorVisitor.INSTANCE.visitAndProcess(decor, new GeneralFlattenImplProvider());
			return CloneUtil.cloneHL7TemplateEl(decor);
		}
		return null;
	}
	
	/**
	 * transform contain to RuleDefinition
	 * @param decor {@link Decor}
	 * @return transform of contain to RuleDefinition
	 */
	public static Decor generalFlattenBisDecor(Decor decor){
		if (decor != null) {
			DecorVisitor.INSTANCE.visitAndProcess(decor, new FlattenBisImplProvider());
			return CloneUtil.cloneHL7TemplateEl(decor);
		}
		return null;
	}

}
