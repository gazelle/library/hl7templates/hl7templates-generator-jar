package net.ihe.gazelle.tempgen.flatten.action;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public enum FlattenAnalyzerEnum {
	
	RD_FLATTEN_PROCESS("RD_FLATTEN_PROCESS"), 
	LET_FLATTEN_PROCESS("RD_FLATTEN_PROCESS"),
	ASSERT_FLATTEN_PROCESS("ASSERT_FLATTEN_PROCESS"),
	CONTAIN_FLATTEN_PROCESS("CONTAIN_FLATTEN_PROCESS"),
	CONTAINS_FLATTEN_PROCESS("CONTAINS_FLATTEN_PROCESS");
	private String value;

	private FlattenAnalyzerEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
