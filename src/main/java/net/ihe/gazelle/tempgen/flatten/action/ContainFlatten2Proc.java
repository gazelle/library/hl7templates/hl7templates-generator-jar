package net.ihe.gazelle.tempgen.flatten.action;

import net.ihe.gazelle.tempapi.impl.ContainDefinitionProcessorImpl;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempmodel.org.decor.art.model.*;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ContainDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * this class flatten the contain with elements containing the templateId/@root
 *
 * @author Abderrazek Boufahja
 */
public class ContainFlatten2Proc extends ContainDefinitionProcessorImpl {


    @Override
    public void process(ContainDefinition t, Object... objects) {
        ContainDefinition currentContainDefinition = t;
        if (currentContainDefinition != null && currentContainDefinition.getRef() != null) {
            RuleDefinition replacementRD = new RuleDefinition();
            TemplateDefinition referencedTD = ContainDefinitionUtil.referencedTemplateDefinitionFromParentBBR(currentContainDefinition);
            if (referencedTD == null) {
                ProblemHandler.handleError("This case is not supposed to happen !",
                        FlattenAnalyzerEnum.CONTAIN_FLATTEN_PROCESS.getValue());
                return;
            }
            RuleDefinition firstElementReferenced = TemplateDefinitionUtil.getFirstElement(referencedTD);
            replacementRD.setName(firstElementReferenced.getName());
            replacementRD.setParentObject(currentContainDefinition);
            if (currentContainDefinition.getItem() != null) {
                replacementRD.setItem(new Item());
                replacementRD.getItem().setLabel(currentContainDefinition.getItem().getLabel());
            }
            replacementRD.setIsClosed(false);
            RuleDefinition templateId = new RuleDefinition();
            templateId.setName("hl7:templateId");
            templateId.getAttribute().add(new Attribute());
            templateId.getAttribute().get(0).setName("root");
            templateId.getAttribute().get(0).setValue(TemplateDefinitionUtil.extractTemplateId(referencedTD));
            replacementRD.getLetOrAssertOrReport().add(templateId);
            if (currentContainDefinition.getParentObject() instanceof RuleDefinition &&
                    currentContainDefinition.getContain() != null && !currentContainDefinition.getContain()) {
                replacementRD.setIsMandatory(currentContainDefinition.getIsMandatory());
                replacementRD.setMaximumMultiplicity(currentContainDefinition.getMaximumMultiplicity());
                replacementRD.setMinimumMultiplicity(currentContainDefinition.getMinimumMultiplicity());
                RuleDefinition parent = (RuleDefinition) currentContainDefinition.getParentObject();
                int index = parent.getLetOrAssertOrReport().indexOf(currentContainDefinition);
                parent.getLetOrAssertOrReport().add(index, replacementRD);
                parent.getLetOrAssertOrReport().remove(currentContainDefinition);
            } else if (currentContainDefinition.getParentObject() instanceof RuleDefinition) {
                currentContainDefinition.getLetOrAssertOrReport().add(replacementRD);
            } else if (currentContainDefinition.getParentObject() instanceof TemplateDefinition) {
                replacementRD.setIsMandatory(currentContainDefinition.getIsMandatory());
                replacementRD.setMaximumMultiplicity(currentContainDefinition.getMaximumMultiplicity());
                replacementRD.setMinimumMultiplicity(currentContainDefinition.getMinimumMultiplicity());
                TemplateDefinition parent = (TemplateDefinition) currentContainDefinition.getParentObject();
                int index = parent.getAttributeOrChoiceOrElement().indexOf(currentContainDefinition);
                parent.getAttributeOrChoiceOrElement().add(index, replacementRD);
                parent.getAttributeOrChoiceOrElement().remove(currentContainDefinition);
            } else if (currentContainDefinition.getParentObject() instanceof ChoiceDefinition) {
                replacementRD.setIsMandatory(currentContainDefinition.getIsMandatory());
                replacementRD.setMaximumMultiplicity(currentContainDefinition.getMaximumMultiplicity());
                replacementRD.setMinimumMultiplicity(currentContainDefinition.getMinimumMultiplicity());
                ChoiceDefinition parent = (ChoiceDefinition) currentContainDefinition.getParentObject();
                int index = parent.getIncludeOrElementOrConstraint().indexOf(currentContainDefinition);
                parent.getIncludeOrElementOrConstraint().add(index, replacementRD);
                parent.getIncludeOrElementOrConstraint().remove(currentContainDefinition);
            }
        }
        super.process(t, objects);
    }

}
