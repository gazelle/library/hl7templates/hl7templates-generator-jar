package net.ihe.gazelle.tempgen.choice.analyzer;

import java.util.Set;
import java.util.TreeSet;

import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ChoiceMinimumMultiplicityANalyzer implements ChoiceConstraintGenerator {

	/**
	 *   minimumMultiplicity &gt; 0
	 *   choiceDefinition.getMinimumMultiplicity() != null
	 */
	@Override
	public String generateOCLConstraint(ChoiceDefinition choiceDefinition) {
		String contextRule = (new OCLGenerator()).generateRuleToParentFromPathNF(choiceDefinition);
		if (!contextRule.equals("(not self.nullFlavor.oclIsUndefined()) or ")) {
			StringBuilder sb = new StringBuilder(contextRule);
			sb.append("->reject(nullFlavor.oclIsUndefined())");
			sb.append("->forAll( (");
			int i = 0;
			for (String elName : extractDistinctElementNamesAndDistinguisher(choiceDefinition)) {
				if (i++>0){
					sb.append("+");
				}
				sb.append(elName + "->size()");
			}
			sb.append(")>=" + choiceDefinition.getMinimumMultiplicity());
			sb.append(")");
			return sb.toString();
		}
		else {
			StringBuilder sb = new StringBuilder(contextRule);
			sb.append("(");
			int i = 0;
			for (String elName : extractDistinctElementNamesAndDistinguisher(choiceDefinition)) {
				if (i++>0){
					sb.append("+");
				}
				sb.append(elName + "->size()");
			}
			sb.append(")>=" + choiceDefinition.getMinimumMultiplicity());
			return sb.toString();
		}
	}
	
	private Set<String> extractDistinctElementNamesAndDistinguisher(ChoiceDefinition choiceDefinition) {
		Set<String> res = new TreeSet<>();
		for (RuleDefinition rd : ChoiceDefinitionUtil.getElements(choiceDefinition)) {
			String distinguisher = OCLGenerator.getExtraSelectionIfNeeded(rd);
			res.add(RuleDefinitionUtil.getRealNameOfRuleDefinition(rd) + distinguisher);
		}
		return res;
	}

	@Override
	public String generateCommentConstraint(ChoiceDefinition choiceDefinition) {
		String templateName = ChoiceDefinitionUtil.getParentTemplateDefinition(choiceDefinition).getDisplayName();
		DParent dparent = RuleDefinitionUtil.getDParentOfTheParent((RuleDefinition)choiceDefinition.getParentObject());
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		StringBuilder sb = new StringBuilder("In " + templateName + ", in " + parentPath + ", the number of elements of type ");
		int i = 0;
		for (RuleDefinition rd : ChoiceDefinitionUtil.getElements(choiceDefinition)) {
			if (i++>0){
				sb.append(", ");
			}
			sb.append("'" + RuleDefinitionUtil.getRealNameOfRuleDefinition(rd) + "'");
		}
		sb.append(" SHALL be bigger or equal to " + choiceDefinition.getMinimumMultiplicity());
		return sb.toString();
	}

	@Override
	public String getProcessIdentifier() {
		return ChoiceAnalyzerEnum.CH_MIN_PROCESS.getValue();
	}

}
