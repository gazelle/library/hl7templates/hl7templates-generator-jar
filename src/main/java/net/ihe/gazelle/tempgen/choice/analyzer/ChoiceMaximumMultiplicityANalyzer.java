package net.ihe.gazelle.tempgen.choice.analyzer;

import java.util.Set;
import java.util.TreeSet;

import net.ihe.gazelle.tempgen.action.MultiplicityUtil;
import net.ihe.gazelle.tempgen.action.OCLGenerator;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ChoiceMaximumMultiplicityANalyzer implements ChoiceConstraintGenerator {

	/**
	 *   maximumMultiplicity &lt; *
	 *   choiceDefinition.getMaximumMultiplicity() != null
	 */
	@Override
	public String generateOCLConstraint(ChoiceDefinition choiceDefinition) {
		Integer maximumMultiplicity = MultiplicityUtil.extractMaximumMultiplicity(choiceDefinition.getMaximumMultiplicity());
		String contextRule = (new OCLGenerator()).generateRuleToParentFromPath(choiceDefinition);
		String res = null;
		if (!contextRule.equals("self")) {
			StringBuilder sb = new StringBuilder(contextRule);
			sb.append("->forAll( (");
			int i = 0;
			for (String elName : extractDistinctElementNamesAndDistinguisher(choiceDefinition)) {
				if (i++>0){
					sb.append("+");
				}
				sb.append(elName + "->size()");
			}
			sb.append(")<=" + maximumMultiplicity);
			sb.append(")");
			res = sb.toString();
		}
		else {
			StringBuilder sb = new StringBuilder("(");
			int i = 0;
			for (String elName : extractDistinctElementNamesAndDistinguisher(choiceDefinition)) {
				if (i++>0){
					sb.append("+");
				}
				sb.append(elName + "->size()");
			}
			sb.append(")<=" + maximumMultiplicity);
			res = sb.toString();
		}
		return res;
	}
	
	private Set<String> extractDistinctElementNamesAndDistinguisher(ChoiceDefinition choiceDefinition) {
		Set<String> res = new TreeSet<>();
		for (RuleDefinition rd : ChoiceDefinitionUtil.getElements(choiceDefinition)) {
			String distinguisher = OCLGenerator.getExtraSelectionIfNeeded(rd);
			res.add(RuleDefinitionUtil.getRealNameOfRuleDefinition(rd) + distinguisher);
		}
		return res;
	}

	@Override
	public String generateCommentConstraint(ChoiceDefinition choiceDefinition) {
		Integer maximumMultiplicity = MultiplicityUtil.extractMaximumMultiplicity(choiceDefinition.getMaximumMultiplicity());
		String templateName = ChoiceDefinitionUtil.getParentTemplateDefinition(choiceDefinition).getDisplayName();
		DParent dparent = RuleDefinitionUtil.getDParentOfTheParent((RuleDefinition)choiceDefinition.getParentObject());
		String parentPath = DPathExtractor.createPathFromDParent(dparent);
		StringBuilder desc = new StringBuilder();
		desc.append("In " + templateName + ", in " + parentPath + ", the number of elements of type ");
		int i = 0;
		for (RuleDefinition rd : ChoiceDefinitionUtil.getElements(choiceDefinition)) {
			if (i++>0){
				desc.append(", ");
			}
			desc.append("'" + RuleDefinitionUtil.getRealNameOfRuleDefinition(rd) + "'");
		}
		desc.append(" SHALL be lower or equal to " + maximumMultiplicity);
		return desc.toString();
	}

	@Override
	public String getProcessIdentifier() {
		return ChoiceAnalyzerEnum.CH_MAX_PROCESS.getValue();
	}

}
