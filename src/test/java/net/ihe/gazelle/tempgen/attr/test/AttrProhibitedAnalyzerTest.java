package net.ihe.gazelle.tempgen.attr.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.attrs.analyzer.AttrAnalyzerEnum;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrProhibitedAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrProhibitedAnalyzerTest {

	Decor decorTemplates = null;

	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_custodian.xml");
	}

	@Test
	public void testGetAttrName() {
		AttrProhibitedAnalyzer aa = new AttrProhibitedAnalyzer();
		aa.setAttrName("ee");
		assertTrue(aa.getAttrName().equals("ee"));
	}

	@Test
	public void testGenerateOCLConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition templ = RuleDefinitionUtil.getElementByName(firstRD, "cda:templateId");
		Attribute att = templ.getAttribute().get(1);
		AttrProhibitedAnalyzer aa = new AttrProhibitedAnalyzer();
		aa.setAttrName(att.getName());
		String ocl = aa.generateOCLConstraint(att);
		assertTrue(ocl.equals("self.templateId->select((not root.oclIsUndefined()) and "
				+ "root='2.16.840.1.113883.10.20.22.1.1')->forAll(extension.oclIsUndefined())"));
	}

	@Test
	public void testGenerateCommentConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition templ = RuleDefinitionUtil.getElementByName(firstRD, "cda:templateId");
		Attribute att = templ.getAttribute().get(1);
		AttrProhibitedAnalyzer aa = new AttrProhibitedAnalyzer();
		aa.setAttrName(att.getName());
		String comment = aa.generateCommentConstraint(att);
		System.out.println(comment);
		assertTrue(comment.equals("In US Realm Header, in "
				+ "/hl7:ClinicalDocument[hl7:templateId/@root='2.16.840.1.113883.10.20.22.1.1']/hl7:templateId[@root='2.16.840.1.113883.10.20.22.1.1'], "
				+ "the attribute extension SHALL not be provided (prohibited=true)"));
	}

	@Test
	public void testGetProcessIdentifier() {
		assertTrue(new AttrProhibitedAnalyzer().getProcessIdentifier().equals(AttrAnalyzerEnum.ATTR_PROHIBITED_PROCESS.getValue()));
	}

}
