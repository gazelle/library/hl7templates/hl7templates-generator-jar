package net.ihe.gazelle.tempgen.attr.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.attrs.analyzer.AttrAnalyzerEnum;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrMultiplicityAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrMultiplicityAnalyzerTest {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_multi.xml");
	}

	@Test
	public void testGenerateOCLConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition auth = RuleDefinitionUtil.getElementByName(firstRD, "hl7:author");
		RuleDefinition assauth = RuleDefinitionUtil.getElementByName(auth, "hl7:assignedAuthor");
		RuleDefinition tel = RuleDefinitionUtil.getElementByName(assauth, "hl7:telecom");
		Attribute attr = tel.getAttribute().get(0);
		String ocl = (new AttrMultiplicityAnalyzer()).generateOCLConstraint(attr);
		assertTrue(ocl.equals("self.author.assignedAuthor.telecom.use->size()<2"));
	}

	@Test
	public void testGenerateCommentConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition auth = RuleDefinitionUtil.getElementByName(firstRD, "hl7:author");
		RuleDefinition assauth = RuleDefinitionUtil.getElementByName(auth, "hl7:assignedAuthor");
		RuleDefinition tel = RuleDefinitionUtil.getElementByName(assauth, "hl7:telecom");
		Attribute attr = tel.getAttribute().get(0);
		String comment = (new AttrMultiplicityAnalyzer()).generateCommentConstraint(attr);
		System.out.println(comment);
		assertTrue(comment.equals("In epSOS CDA Supply, in "
				+ "/hl7:supply[hl7:templateId/@root='1.3.6.1.4.1.12559.11.10.1.3.1.3.3']/hl7:author/hl7:assignedAuthor/hl7:telecom, "
				+ "the attribute use SHALL have at most one value"));
	}

	@Test
	public void testGetProcessIdentifier() {
		assertTrue(new AttrMultiplicityAnalyzer().getProcessIdentifier().equals(AttrAnalyzerEnum.ATTR_MULTI_PROCESS.getValue()));
	}

}
