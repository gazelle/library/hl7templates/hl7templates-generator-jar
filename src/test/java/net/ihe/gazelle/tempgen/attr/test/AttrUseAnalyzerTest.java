package net.ihe.gazelle.tempgen.attr.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.attrs.analyzer.AttrAnalyzerEnum;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrUseAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrUseAnalyzerTest {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_section.xml");
	}

	@Test
	public void testGenerateOCLConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition subj = RuleDefinitionUtil.getElementByName(firstRD, "cda:subject");
		RuleDefinition subjrel = RuleDefinitionUtil.getElementByName(subj, "cda:relatedSubject");
		RuleDefinition add = RuleDefinitionUtil.getElementByName(subjrel, "cda:addr");
		Attribute attr = add.getAttribute().get(0);
		String ocl = (new AttrUseAnalyzer()).generateOCLConstraint(attr);
		assertTrue(ocl.equals("self.subject.relatedSubject.addr.use->forAll(aa | aa='HP')"));
	}

	@Test
	public void testGenerateCommentConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition subj = RuleDefinitionUtil.getElementByName(firstRD, "cda:subject");
		RuleDefinition subjrel = RuleDefinitionUtil.getElementByName(subj, "cda:relatedSubject");
		RuleDefinition add = RuleDefinitionUtil.getElementByName(subjrel, "cda:addr");
		Attribute attr = add.getAttribute().get(0);
		String comment = (new AttrUseAnalyzer()).generateCommentConstraint(attr);
		System.out.println(comment);
		assertTrue(comment.equals("In Hospital Discharge Diagnosis Section, in "
				+ "/hl7:section[hl7:templateId/@root='2.16.840.1.113883.10.20.22.2.24']/hl7:subject/hl7:relatedSubject/hl7:addr, "
				+ "the attribute use SHALL have the value 'HP' if present"));
	}

	@Test
	public void testGetProcessIdentifier() {
		assertTrue(new AttrUseAnalyzer().getProcessIdentifier().equals(AttrAnalyzerEnum.ATTR_USE_PROCESS.getValue()));
	}

}
