package net.ihe.gazelle.tempgen.attr.test;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.attrs.analyzer.AttrAnalyzerEnum;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrValueAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrValueAnalyzerTest {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_obs2.xml");
	}

	@Test
	public void testGenerateOCLConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "cda:entryRelationship");
		Attribute attr = entr.getAttribute().get(0);
		String ocl = (new AttrValueAnalyzer()).generateOCLConstraint(attr);
		System.out.println(ocl);
		assertTrue(ocl.equals("self.entryRelationship->select((not typeCode.oclIsUndefined()) and typeCode=XActRelationshipEntryRelationship::SPRT)->forAll(typeCode.oclIsUndefined() or typeCode=XActRelationshipEntryRelationship::SPRT)"));
	}
	
	@Test
	public void testGenerateOCLConstraint1() {
		Decor decorTemplates2 = DecorMarshaller.loadDecor("src/test/resources/decor_comptest.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates2.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "cda:entryRelationship");
		Attribute attr = entr.getAttribute().get(0);
		String ocl = (new AttrValueAnalyzer()).generateOCLConstraint(attr);
		System.out.println(ocl);
		assertTrue(ocl.equals("self.entryRelationship->select((not typeCode.oclIsUndefined()) and typeCode=XActRelationshipEntryRelationship::REFR)->"
				+ "forAll(typeCode.oclIsUndefined() or typeCode=XActRelationshipEntryRelationship::REFR)"));
	}

	@Test
	public void testGenerateCommentConstraint1() throws FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs_xx.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "cda:entryRelationship");
		Attribute attr = entr.getAttribute().get(0);
		String comment = (new AttrValueAnalyzer()).generateCommentConstraint(attr);
		assertTrue(comment.equals("In Text Observation, "
				+ "in /hl7:observation[hl7:templateId/@root='2.16.840.1.113883.10.20.6.2.12']"
				+ "/hl7:entryRelationship[@typeCode='SPRT'], the attribute typeCode SHALL have the value 'SPRT' if present"));
	}
	
	@Test
	public void testGenerateCommentConstraint() throws FileNotFoundException, JAXBException {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "cda:entryRelationship");
		Attribute attr = entr.getAttribute().get(0);
		String comment = (new AttrValueAnalyzer()).generateCommentConstraint(attr);
		System.out.println(comment);
		assertTrue(comment.equals("In Text Observation, "
				+ "in /hl7:observation[hl7:templateId/@root='2.16.840.1.113883.10.20.6.2.12']/hl7:entryRelationship[@typeCode='SPRT'], "
				+ "the attribute typeCode SHALL have the value 'SPRT' if present"));
	}

	@Test
	public void testGetProcessIdentifier() {
		assertTrue(new AttrValueAnalyzer().getProcessIdentifier().equals(AttrAnalyzerEnum.ATTR_VALUE_PROCESS.getValue()));
	}

}
