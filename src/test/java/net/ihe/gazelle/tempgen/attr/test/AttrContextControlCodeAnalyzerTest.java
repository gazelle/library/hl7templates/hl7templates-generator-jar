package net.ihe.gazelle.tempgen.attr.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.attrs.analyzer.AttrAnalyzerEnum;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrContextControlCodeAnalyzer;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrContextControlCodeAnalyzerTest {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_section.xml");
	}

	@Test
	public void testGenerateOCLConstraint1() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition subj = RuleDefinitionUtil.getElementByName(firstRD, "cda:subject");
		Attribute attr = subj.getAttribute().get(0);
		String ocl = (new AttrContextControlCodeAnalyzer()).generateOCLConstraint(attr);
		assertTrue(ocl.equals("self.subject->forAll(contextControlCode.oclIsUndefined() or contextControlCode=NullFlavor::OP)"));
	}
	
	@Test
	public void testGenerateOCLConstraint2() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition subj = RuleDefinitionUtil.getElementByName(firstRD, "cda:subject");
		Attribute attr = subj.getAttribute().get(1);
		String ocl = (new AttrContextControlCodeAnalyzer()).generateOCLConstraint(attr);
		assertTrue(ocl.equals("true"));
		assertTrue(ProblemHandler.findNotification("The value AA is not part of the enumeration type NullFlavor") != null);
	}

	@Test
	public void testGenerateCommentConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition subj = RuleDefinitionUtil.getElementByName(firstRD, "cda:subject");
		Attribute attr = subj.getAttribute().get(0);
		String comment = (new AttrContextControlCodeAnalyzer()).generateCommentConstraint(attr);
		assertTrue(comment.equals("In Hospital Discharge Diagnosis Section, in "
				+ "/hl7:section[hl7:templateId/@root='2.16.840.1.113883.10.20.22.2.24']/hl7:subject, the attribute contextControlCode SHALL "
				+ "have the value 'OP' if present"));
	}

	@Test
	public void testGetProcessIdentifier() {
		assertTrue(new AttrContextControlCodeAnalyzer().getProcessIdentifier().equals(AttrAnalyzerEnum.ATTR_CONTEXTCONTROLCODE_PROCESS.getValue()));
	}

}
