package net.ihe.gazelle.tempgen.attr.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.attrs.analyzer.AttrAnalyzerEnum;
import net.ihe.gazelle.tempgen.attrs.analyzer.AttrConstraintAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrConstraintAnalyzerTest {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_attrdt.xml");
	}


	@Test
	public void testGetConstraint() {
		FreeFormMarkupWithLanguage ffm = new FreeFormMarkupWithLanguage();
		ffm.setLanguage("en-US");
		AttrConstraintAnalyzer aca = new AttrConstraintAnalyzer();
		aca.setConstraint(ffm);
		assertTrue(ffm.equals(aca.getConstraint()));
	}

	@Test
	public void testGenerateOCLConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition id = RuleDefinitionUtil.getElementByName(firstRD, "hl7:id");
		Attribute attr = id.getAttribute().get(0);
		AttrConstraintAnalyzer aca = new AttrConstraintAnalyzer();
		aca.setConstraint(attr.getConstraint().get(0));
		String ocl = aca.generateOCLConstraint(attr);
		assertTrue(ocl.equals("true --there is not way to test the constraint"));
	}

	@Test
	public void testGenerateCommentConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition id = RuleDefinitionUtil.getElementByName(firstRD, "hl7:id");
		Attribute attr = id.getAttribute().get(0);
		AttrConstraintAnalyzer aca = new AttrConstraintAnalyzer();
		aca.setConstraint(attr.getConstraint().get(0));
		String comment = aca.generateCommentConstraint(attr);
		assertTrue(comment.equals("In Section Coded Social History, in "
				+ "/hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/hl7:id/@displayable, test"));
	}

	@Test
	public void testGetProcessIdentifier() {
		assertTrue(new AttrConstraintAnalyzer().getProcessIdentifier().equals(AttrAnalyzerEnum.ATTR_CONS_PROCESS.getValue()));
	}

}
