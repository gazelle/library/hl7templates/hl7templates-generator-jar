package net.ihe.gazelle.tempgen.sequoia.scripts;

import net.ihe.gazelle.common.marshaller.ObjectMarshallerImpl;
import net.ihe.gazelle.svs.ConceptListType;
import net.ihe.gazelle.svs.DescribedValueSet;
import net.ihe.gazelle.svs.RetrieveMultipleValueSetsResponseType;
import net.ihe.gazelle.svs.RetrieveValueSetResponseType;
import net.ihe.gazelle.svs.ValueSetResponseType;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class GenerateSVSFromMultiple {

    private static final Logger LOG = LoggerFactory.getLogger(GenerateSVSFromMultiple.class);

    private static String[] multipleValueSetFiles = {
            "/home/cel/workspace/sequoia-ccda/value-sets-2018-06-15/2018-06-15_VSAC_RetrieveMultipleValueSets-part-1.xml",
            "/home/cel/workspace/sequoia-ccda/value-sets-2018-06-15/2018-06-15_VSAC_RetrieveMultipleValueSets-part-2.xml"
    };

    private static String outputDir = "/home/cel/workspace/sequoia-ccda/value-sets-2018-06-15/value-sets/";

    public static void main(String[] args) {

        StringBuilder valueSetOIDs = new StringBuilder("Extracted value-sets:" + System.lineSeparator());

        for (String multipleValueSetFile : multipleValueSetFiles) {

            InputStream inputStream = null;
            try {
                inputStream = new FileInputStream(multipleValueSetFile);
                ObjectMarshallerImpl<RetrieveMultipleValueSetsResponseType> om = new ObjectMarshallerImpl<>(
                        RetrieveMultipleValueSetsResponseType.class);
                ObjectMarshallerImpl<RetrieveValueSetResponseType> om2 = new ObjectMarshallerImpl<>(RetrieveValueSetResponseType.class);
                RetrieveMultipleValueSetsResponseType rr = om.unmarshall(inputStream);


                int i = 0;

                for (DescribedValueSet dvs : rr.getDescribedValueSet()) {
                    RetrieveValueSetResponseType rvs = new RetrieveValueSetResponseType();
                    rvs.setValueSet(new ValueSetResponseType());
                    rvs.getValueSet().setDisplayName(dvs.getDisplayName());
                    rvs.getValueSet().setId(dvs.getId());
                    rvs.getValueSet().setConceptList(dvs.getConceptList());
                    rvs.getValueSet().setVersion(dvs.getVersion());

                    for (ConceptListType conceptListType : rvs.getValueSet().getConceptList()) {
                        if (dvs.getConceptList().get(0).getLang() != null) {
                            conceptListType.setLang(dvs.getConceptList().get(0).getLang());
                        } else {
                            conceptListType.setLang("en-US");
                        }
                    }

                    OutputStream outputStream = null;
                    try {
                        outputStream = new FileOutputStream(outputDir + rvs.getValueSet().getId() + ".xml");
                        om2.marshall(rvs, outputStream);
                        valueSetOIDs.append("\"" + rvs.getValueSet().getId() + "\", ");
                        if (i++ > 2) {
                            i = 0;
                            valueSetOIDs.append(System.lineSeparator());
                        }
                    } catch (FileNotFoundException | JAXBException e) {
                        LOG.error("Unable to write/marshall output value-set file", e);
                    } finally {
                        IOUtils.closeQuietly(outputStream);
                    }
                }


            } catch (IOException | JAXBException e) {
                LOG.error("Unable to read/unmarshal multiple-value-set file", e);
            } finally {
                IOUtils.closeQuietly(inputStream);
            }

        }

        LOG.info(valueSetOIDs.toString());

    }

}
