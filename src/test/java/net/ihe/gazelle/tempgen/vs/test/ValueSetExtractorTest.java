package net.ihe.gazelle.tempgen.vs.test;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import net.ihe.gazelle.tempgen.action.FileReadWrite;
import net.ihe.gazelle.tempgen.valueset.action.ValueSetExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ValueSetExtractorTest {

	@Test
	public void testExtractValueSetsFromDecor() throws IOException {
		Decor decor = DecorMarshaller.loadDecor("src/test/resources/decor_terminologies.xml");
		File outputFile = new File("src/test/resources/out/vstest/");
		if (!outputFile.exists()) {
			outputFile.mkdirs();
		}
		ValueSetExtractor.extractValueSetsFromDecor(decor, outputFile);
		assertTrue(outputFile.list().length == 6);
		assertTrue(Arrays.asList(outputFile.list()).contains("2.16.840.1.113883.1.11.12212-2013-01-31T000000.xml"));
		assertTrue(Arrays.asList(outputFile.list()).contains("2.16.840.1.113883.1.11.19717-2013-01-31T000000.xml"));
		assertTrue(Arrays.asList(outputFile.list()).contains("1.2.3-2013-01-31T000000.xml"));
		File a123 = new File(outputFile.getAbsoluteFile() + "/" + "1.2.3-2013-01-31T000000.xml");
		String content = FileReadWrite.readDoc(a123.getAbsolutePath());
		assertTrue(StringUtils.countMatches(content, "<Concept ") == 2);
		File inc = new File(outputFile.getAbsoluteFile() + "/" + "2.16.840.1.113883.1.11.19717-2013-01-31T000000.xml");
		String contentInc = FileReadWrite.readDoc(inc.getAbsolutePath());
		System.out.println(StringUtils.countMatches(contentInc, "<Concept "));
		assertTrue(StringUtils.countMatches(contentInc, "<Concept ") == 6);
	}

}
