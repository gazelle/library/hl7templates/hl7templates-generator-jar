package net.ihe.gazelle.tempgen.vs.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempgen.valueset.flatten.VSFlattener;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TerminologyUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class VSFlattenerTest {

	@Test
	public void testFlattenDecorForValueSets() {
		Decor decor = DecorMarshaller.loadDecor("src/test/resources/decor_terminologies.xml");
		decor = VSFlattener.flattenDecorForValueSets(decor);
		ValueSet vs = TerminologyUtil.getValueSetById(decor.getTerminology(), "2.16.840.1.113883.1.11.19717");
		assertTrue(vs.getConceptList().getConceptOrInclude().size()==8);
		vs = TerminologyUtil.getValueSetById(decor.getTerminology(), "2.16.840.1.113883.1.11.12212");
		assertTrue(vs.getConceptList().getConceptOrInclude().size()==9);
		vs = TerminologyUtil.getValueSetById(decor.getTerminology(), "1.2.3");
		assertTrue(vs.getConceptList().getConceptOrInclude().size()==2);
		vs = TerminologyUtil.getValueSetById(decor.getTerminology(), "2.3.5");
		assertTrue(vs.getConceptList().getConceptOrInclude().size()==6);
		System.out.println("---");
	}
	
	@Test
	public void testFlattenDecorForValueSets2() {
		Decor decor = DecorMarshaller.loadDecor("src/test/resources/decor_terminologies2.xml");
		decor = VSFlattener.flattenDecorForValueSets(decor);
		ValueSet vs = TerminologyUtil.getValueSetById(decor.getTerminology(), "2.16.840.1.113883.1.11.12212");
		assertTrue(vs.getConceptList().getConceptOrInclude().size()==9);
		vs = TerminologyUtil.getValueSetById(decor.getTerminology(), "2.3.4");
		assertTrue(vs.getConceptList().getConceptOrInclude().size()==2);
		vs = TerminologyUtil.getValueSetById(decor.getTerminology(), "2.3.5");
		System.out.println(vs.getConceptList().getConceptOrInclude().size());
		assertTrue(vs.getConceptList().getConceptOrInclude().size()==5);
		vs = TerminologyUtil.getValueSetById(decor.getTerminology(), "2.3.6");
		assertTrue(vs.getConceptList().getConceptOrInclude().size()==3);
		System.out.println("---");
	}

}
