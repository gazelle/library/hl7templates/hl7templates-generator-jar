package net.ihe.gazelle.tempgen.vs.test;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import net.ihe.gazelle.tempgen.valueset.flatten.ValueSetConceptListFlatten;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetRef;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ValueSetConceptListFlattenTest extends ValueSetConceptListFlatten {

	@Test
	public void testProcess() {
		Decor decor = DecorMarshaller.loadDecor("src/test/resources/decor_terminologies.xml");
		ValueSetConceptList vscl = decor.getTerminology().getValueSet().get(0).getConceptList();
		(new ValueSetConceptListFlatten()).process(vscl);
		assertTrue(vscl.getConceptOrInclude().size()==9);
		vscl = decor.getTerminology().getValueSet().get(0).getConceptList();
		(new ValueSetConceptListFlatten()).process(vscl);
		System.out.println(vscl.getConceptOrInclude().size());
		assertTrue(vscl.getConceptOrInclude().size()==9);
	}

	@Test
	public void testProcessIncludes() {
		Decor decor = DecorMarshaller.loadDecor("src/test/resources/decor_terminologies.xml");
		this.currentValueSetConceptList = decor.getTerminology().getValueSet().get(0).getConceptList();
		this.cirdularRefs = null;
		List<ValueSetRef> lvr = new ArrayList<ValueSetRef>();
		lvr.add(new ValueSetRef());
		lvr.get(0).setRef("2.3.4");
		this.processIncludes(lvr);
		assertTrue(this.currentValueSetConceptList.getConceptOrInclude().size()==11);
	}

	@Test
	public void testFlattenInclude() throws FileNotFoundException, JAXBException {
		Decor decor = DecorMarshaller.loadDecor("src/test/resources/decor_terminologies.xml");
		this.currentValueSetConceptList = decor.getTerminology().getValueSet().get(0).getConceptList();
		this.cirdularRefs = null;
		ValueSetRef vsr = new ValueSetRef();
		vsr.setRef("2.3.4");
		this.flattenInclude(vsr);
		assertTrue(this.currentValueSetConceptList.getConceptOrInclude().size()==11);
	}

}
