package net.ihe.gazelle.tempgen.proc.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.goc.uml.utils.PackagedElementUtil;
import net.ihe.gazelle.goc.uml.utils.XMIUtil;
import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.goc.xmm.OwnedRuleKind;
import net.ihe.gazelle.tempgen.action.ChoiceDefinitionAnalyzer;
import net.ihe.gazelle.tempgen.action.TamlHandler;
import net.ihe.gazelle.tempgen.choice.analyzer.ChoiceMinimumMultiplicityANalyzer;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempmodel.choices.model.ChoiceDefault;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ChoiceDefinitionAnalyzerTest extends ChoiceDefinitionAnalyzer {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
		TemplateDefinition td = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
		RuleDefinition participantRole = RuleDefinitionUtil.getElementByName(TemplateDefinitionUtil.getFirstElement(td), "hl7:participantRole");
		this.currentChoiceDefinition = RuleDefinitionUtil.getChoices(participantRole).get(0);
		this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
	}

	@Test
	public void testProcess1() {
		this.process(currentChoiceDefinition);
		assertTrue(ProblemHandler.findNotification("The method ChoiceDefinitionAnalyzer::process is called with a bad objects attribute") != null);
	}
	
	@Test
	public void testProcess2() {
		int size = this.currentPackagedElement.getOwnedRule().size();
		this.process(currentChoiceDefinition, this.currentPackagedElement);
		assertTrue(this.currentPackagedElement.getOwnedRule().size()>size);
		this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
	}
	
	@Test
	public void testProcess3() throws FileNotFoundException, JAXBException {
		Decor decort = DecorMarshaller.loadDecor("src/test/resources/decor_choice3.xml");
		TemplateDefinition td = RulesUtil.getTemplates(decort.getRules()).get(0);
		RuleDefinition participantRole = RuleDefinitionUtil.getElementByName(TemplateDefinitionUtil.getFirstElement(td), "hl7:participantRole");
		this.currentChoiceDefinition = RuleDefinitionUtil.getChoices(participantRole).get(0); 
		this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
		int size = this.currentPackagedElement.getOwnedRule().size();
		this.process(currentChoiceDefinition, this.currentPackagedElement, true);
		System.out.println(this.currentPackagedElement.getOwnedRule().size());
		assertTrue(this.currentPackagedElement.getOwnedRule().size()>size);
		assertTrue(PackagedElementUtil.findOwnedRulesByContent(currentPackagedElement, "the attribute root SHALL be present").isEmpty());
	}
	
	@Test
	public void testProcess4() throws FileNotFoundException, JAXBException {
		Decor decort = DecorMarshaller.loadDecor("src/test/resources/decor_choice3.xml");
		TemplateDefinition td = RulesUtil.getTemplates(decort.getRules()).get(0);
		RuleDefinition participantRole = RuleDefinitionUtil.getElementByName(TemplateDefinitionUtil.getFirstElement(td), "hl7:participantRole");
		this.currentChoiceDefinition = RuleDefinitionUtil.getChoices(participantRole).get(0); 
		this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
		int size = this.currentPackagedElement.getOwnedRule().size();
		this.process(currentChoiceDefinition, this.currentPackagedElement, false);
		System.out.println(this.currentPackagedElement.getOwnedRule().size());
		assertTrue(this.currentPackagedElement.getOwnedRule().size()>size);
		assertTrue(!PackagedElementUtil.findOwnedRulesByContent(currentPackagedElement, "the attribute root SHALL be present").isEmpty());
	}

	@Test
	public void testProcessElements() {
		// TODO this method should be deleted later
	}

	@Test
	public void testProcessItem() {
		XMI xmi  = XMIUtil.createXMI("test"); 
		this.processMinimumMultiplicity(this.currentChoiceDefinition.getMinimumMultiplicity());
		this.processItem(null);
		TamlHandler.addTamlToXMI(xmi);
		assertTrue(xmi.getTaml().size()>0);
		int size = xmi.getTaml().size();
		this.processItem(this.currentChoiceDefinition.getItem());
		TamlHandler.addTamlToXMI(xmi);
		assertTrue(xmi.getTaml().size()>size);
	}

	@Test
	public void testProcessMaximumMultiplicity() {
		this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
		// the process will not work because max > default
		this.processMaximumMultiplicity(this.currentChoiceDefinition.getMaximumMultiplicity());
		assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
	}

	@Test
	public void testProcessMinimumMultiplicity() {
		this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
		this.processMinimumMultiplicity(this.currentChoiceDefinition.getMinimumMultiplicity());
		assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
	}

	@Test
	public void testProcessingMinimumMultiplicityIsPossible() {
		assertFalse(this.processingMinimumMultiplicityIsPossible(-1));
		assertFalse(this.processingMinimumMultiplicityIsPossible(0));
		assertTrue(this.processingMinimumMultiplicityIsPossible(1));
	}

	@Test
	public void testFulfillOwnedRuleForConstraintGenerator() {
		this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
		fulfillOwnedRuleForConstraintGenerator(new ChoiceMinimumMultiplicityANalyzer(), OwnedRuleKind.CARDINALITY);
		assertTrue(this.currentPackagedElement.getOwnedRule().size()==1);
	}

	@Test
	public void testExtractChoiceDefaultFromChoiceDefinition() {
		ChoiceDefault aa = extractChoiceDefaultFromChoiceDefinition(this.currentChoiceDefinition);
		assertTrue(aa != null);
		assertTrue(aa.getType().equals("POCDMT000040ParticipantRole"));
	}

	@Test
	public void testVerifyThatChoiceIsProcessable() {
		assertTrue(this.verifyThatChoiceIsProcessable(currentChoiceDefinition));
		this.currentChoiceDefinition.getIncludeOrElementOrConstraint().add(new IncludeDefinition());
		assertFalse(this.verifyThatChoiceIsProcessable(currentChoiceDefinition));
	}

}
