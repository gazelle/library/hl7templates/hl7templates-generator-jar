package net.ihe.gazelle.tempgen.flatten.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.flatten.action.IncludeDefinitionFlattenProc;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * @author Abderrazek Boufahja
 */
public class IncludeDefinitionFlattenProcTest extends IncludeDefinitionFlattenProc {

    Decor decorTemplates = null;

    @Before
    public void setUp() {
        decorTemplates = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_1.xml");
    }

    @Test
    public void testProcess() throws FileNotFoundException, JAXBException {
        Decor decorTemplates2 = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_1.xml");
        TemplateDefinition td = RulesUtil.getTemplates(decorTemplates2.getRules()).get(0);
        IncludeDefinition inc = TemplateDefinitionUtil.getIncludes(td).get(0);
        (new IncludeDefinitionFlattenProc()).process(inc);
        assertTrue(TemplateDefinitionUtil.getIncludes(td).size() == 0);
        assertTrue(TemplateDefinitionUtil.getElements(td).size() == 1);
        assertTrue(TemplateDefinitionUtil.getContains(td).size() == 1);
        assertEquals(false, TemplateDefinitionUtil.getContains(td).get(0).getContain());
    }

    @Test
    public void testFlattenIncludeInChoiceDefinition() {
        TemplateDefinition td = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
        RuleDefinition sec = TemplateDefinitionUtil.getFirstElement(td);
        ChoiceDefinition entry = RuleDefinitionUtil.getChoices(sec).get(0);
        IncludeDefinition inc = ChoiceDefinitionUtil.getIncludes(entry).get(0);
        this.setCurrentIncludeDefinition(inc);
        this.flattenIncludeInChoiceDefinition(entry);
        assertTrue(ChoiceDefinitionUtil.getIncludes(entry).size() == 0);
        assertTrue(entry.getIncludeOrElementOrConstraint().size() == 2);
        assertTrue(ChoiceDefinitionUtil.getContains(entry).size() == 1);
        assertEquals(false, ChoiceDefinitionUtil.getContains(entry).get(0).getContain());
    }

    @Test
    public void testFlattenIncludeInRuleDefinition() {
        TemplateDefinition td = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
        RuleDefinition sec = TemplateDefinitionUtil.getFirstElement(td);
        RuleDefinition entry = RuleDefinitionUtil.getElementByName(sec, "hl7:entry");
        IncludeDefinition inc = RuleDefinitionUtil.getIncludes(entry).get(0);
        this.setCurrentIncludeDefinition(inc);
        this.flattenIncludeInRuleDefinition(entry);
        assertTrue(RuleDefinitionUtil.getIncludes(entry).size() == 0);
        assertTrue(entry.getLetOrAssertOrReport().size() == 1);
        assertTrue(RuleDefinitionUtil.getContains(entry).size() == 1);
        assertEquals(false, RuleDefinitionUtil.getContains(entry).get(0).getContain());
    }

    @Test
    public void testFlattenIncludeInTemplateDefinition() {
        TemplateDefinition td = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
        IncludeDefinition inc = TemplateDefinitionUtil.getIncludes(td).get(0);
        this.setCurrentIncludeDefinition(inc);
        this.flattenIncludeInTemplateDefinition(td);
        assertTrue(TemplateDefinitionUtil.getIncludes(td).size() == 0);
        assertTrue(TemplateDefinitionUtil.getElements(td).size() == 1);
        assertTrue(TemplateDefinitionUtil.getContains(td).size() == 1);
        assertEquals(false, TemplateDefinitionUtil.getContains(td).get(0).getContain());
    }

    @Test
    public void testgetCurrentIncludeDefinition() throws Exception {
        IncludeDefinition id = new IncludeDefinition();
        this.process(id);
        assertTrue(this.getCurrentIncludeDefinition() == id);
    }

}
