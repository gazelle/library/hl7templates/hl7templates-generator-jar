package net.ihe.gazelle.tempgen.flatten.test;

import net.ihe.gazelle.tempgen.flatten.action.RuleDefinitionFlattenProc;
import net.ihe.gazelle.tempmodel.dpath.model.DAttribute;
import net.ihe.gazelle.tempmodel.dpath.model.DElement;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Abderrazek Boufahja
 */
public class RuleDefinitionFlattenProcTest extends RuleDefinitionFlattenProc {

    Decor decorTemplates = null;
    Decor decorTemplates2 = null;

    @Before
    public void setUp() {
        decorTemplates = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_2.xml");
        decorTemplates2 = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_2.xml");
    }

    @Test
    public void testProcess() {
        TemplateDefinition td = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
        RuleDefinition sec = TemplateDefinitionUtil.getFirstElement(td);
        RuleDefinition entry = RuleDefinitionUtil.getElementByName(sec, "hl7:entry");
        (new RuleDefinitionFlattenProc()).process(entry);
        assertTrue(entry.getContains() == null);
        assertTrue(RuleDefinitionUtil.getContains(entry).size() == 1);
        assertEquals(entry.getMinimumMultiplicity(),
                RuleDefinitionUtil.getContains(entry).get(0).getMinimumMultiplicity());
        assertEquals(entry.getMaximumMultiplicity(),
                RuleDefinitionUtil.getContains(entry).get(0).getMaximumMultiplicity());
    }

    @Test
    public void testProcessContains() {
        TemplateDefinition td = RulesUtil.getTemplates(decorTemplates2.getRules()).get(0);
        RuleDefinition sec = TemplateDefinitionUtil.getFirstElement(td);
        RuleDefinition entry = RuleDefinitionUtil.getElementByName(sec, "hl7:entry");
        this.setCurrentRuleDefinition(entry);
        this.processContains(entry.getContains());
        assertTrue(entry.getContains() == null);
        assertTrue(RuleDefinitionUtil.getContains(entry).size() == 1);
        assertEquals(entry.getMinimumMultiplicity(),
                RuleDefinitionUtil.getContains(entry).get(0).getMinimumMultiplicity());
        assertEquals(entry.getMaximumMultiplicity(),
                RuleDefinitionUtil.getContains(entry).get(0).getMaximumMultiplicity());
        assertEquals(entry.getIsMandatory(),
                RuleDefinitionUtil.getContains(entry).get(0).getIsMandatory());
        assertNull(RuleDefinitionUtil.getContains(entry).get(0).getIsMandatory());
    }

    @Test
    public void testProcessContains2() {
        TemplateDefinition td = RulesUtil.getTemplates(DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_3.xml").getRules()).get(0);
        RuleDefinition sec = TemplateDefinitionUtil.getFirstElement(td);
        RuleDefinition entry = RuleDefinitionUtil.getElementByName(sec, "hl7:entry");
        this.setCurrentRuleDefinition(entry);
        this.processContains(entry.getContains());
        assertTrue(entry.getContains() == null);
        assertTrue(RuleDefinitionUtil.getContains(entry).size() == 1);
        assertEquals(entry.getMinimumMultiplicity(),
                RuleDefinitionUtil.getContains(entry).get(0).getMinimumMultiplicity());
        assertEquals(entry.getMaximumMultiplicity(),
                RuleDefinitionUtil.getContains(entry).get(0).getMaximumMultiplicity());
        assertEquals(entry.getIsMandatory(),
                RuleDefinitionUtil.getContains(entry).get(0).getIsMandatory());
        assertEquals(true, RuleDefinitionUtil.getContains(entry).get(0).getIsMandatory());
    }

    @Test
    public void testProcessName1() throws Exception {
        TemplateDefinition td = RulesUtil.getTemplates(decorTemplates2.getRules()).get(0);
        RuleDefinition sec = TemplateDefinitionUtil.getFirstElement(td);
        this.currentRuleDefinition = RuleDefinitionUtil.getElementByName(sec, "hl7:entry");
        this.processName("hl7:entry");
        assertTrue(this.currentRuleDefinition.getName().equals("hl7:entry"));
    }

    @Test
    public void testProcessName2() throws Exception {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_2.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition sec = TemplateDefinitionUtil.getFirstElement(td);
        this.currentRuleDefinition = RuleDefinitionUtil.getElementByName(sec, "hl7:entry");
        this.currentRuleDefinition.setName("hl7:entry[@typeCode='COMP']");
        this.processName("hl7:entry[@typeCode='COMP']");
        assertTrue(this.currentRuleDefinition.getName().equals("entry"));
        assertTrue(this.currentRdNameAsXpath.isPathIsConvertible());
        assertTrue(this.currentRdNameAsXpath.isProcessed());
    }

    @Test
    public void testProcessName3() throws Exception {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_2.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition sec = TemplateDefinitionUtil.getFirstElement(td);
        this.currentRuleDefinition = RuleDefinitionUtil.getElementByName(sec, "hl7:entry");
        this.currentRuleDefinition.setName("hl7:entry/hl7:act[@classCode='ACT']");
        this.processName("hl7:entry/hl7:act[@classCode='ACT']");
        System.out.println(this.currentRuleDefinition.getName());
        assertTrue(this.currentRuleDefinition.getName().equals("hl7:entry/hl7:act[@classCode='ACT']"));
        assertTrue(this.currentRdNameAsXpath.isProcessed());
        assertTrue(!this.currentRdNameAsXpath.isPathIsConvertible());
        assertTrue(!this.currentRdNameAsXpath.isPathIsIgnorable());
    }

    @Test
    public void testProcessName4() throws Exception {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_2.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition sec = TemplateDefinitionUtil.getFirstElement(td);
        this.currentRuleDefinition = RuleDefinitionUtil.getElementByName(sec, "hl7:entry");
        this.currentRuleDefinition.setName("hl7:entry/hl7:act");
        this.processName("hl7:entry/hl7:act");
        System.out.println(this.currentRuleDefinition.getName());
        assertTrue(this.currentRuleDefinition.getName().equals("hl7:entry/hl7:act"));
        assertTrue(!this.currentRdNameAsXpath.isProcessed());
        assertTrue(!this.currentRdNameAsXpath.isPathIsConvertible());
        assertTrue(!this.currentRdNameAsXpath.isPathIsIgnorable());
    }

    @Test
    public void testProcessName5() throws Exception {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_2.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition sec = TemplateDefinitionUtil.getFirstElement(td);
        this.currentRuleDefinition = RuleDefinitionUtil.getElementByName(sec, "hl7:templateId");
        this.currentRuleDefinition.setName("hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']");
        this.processName("hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']");
        assertTrue(this.currentRuleDefinition.getName().equals("templateId"));
        assertTrue(this.currentRdNameAsXpath.isPathIsIgnorable());
        assertTrue(!this.currentRdNameAsXpath.isPathIsConvertible());
        assertTrue(this.currentRdNameAsXpath.isProcessed());
    }

    @Test
    public void testFlattenDecorNameForDParent() throws Exception {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_2.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition sec = TemplateDefinitionUtil.getFirstElement(td);
        this.currentRuleDefinition = RuleDefinitionUtil.getElementByName(sec, "hl7:templateId");
        this.currentRuleDefinition.setName("hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']");
        DElement delement = new DElement();
        delement.setName("templateId");
        delement.setDistinguisherAttributeOrElement(new DAttribute());
        delement.getDistinguisherAttributeOrElement().setName("root");
        delement.getDistinguisherAttributeOrElement().setValue("1.3.6.1.4.1.19376.1.5.3.1.3.16.1");
        this.flattenDecorNameForDParent(delement);
        assertTrue(this.currentRuleDefinition.getName().equals("templateId"));
        assertTrue(this.currentRdNameAsXpath.isPathIsIgnorable());
        assertTrue(!this.currentRdNameAsXpath.isPathIsConvertible());
    }

    @Test
    public void testProcessVocabularys1() throws Exception {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_flat_vocab1.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition sec = TemplateDefinitionUtil.getFirstElement(td);
        RuleDefinition code = RuleDefinitionUtil.getElementByName(sec, "hl7:code");
        this.currentRuleDefinition = code;
        this.processVocabularys(this.currentRuleDefinition.getVocabulary());
        assertTrue(this.currentRuleDefinition.getVocabulary().size() == 0);
        assertTrue(this.currentRuleDefinition.getAttribute().size() == 2);
        assertTrue(RuleDefinitionUtil.getAttributeByName(currentRuleDefinition, "code") != null);
        assertTrue(RuleDefinitionUtil.getAttributeByName(currentRuleDefinition, "code").getValue().equals("123"));
        assertTrue(RuleDefinitionUtil.getAttributeByName(currentRuleDefinition, "codeSystem").getValue().equals("456"));
    }

    @Test
    public void testProcessVocabularys2() throws Exception {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_flat_vocab1.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition sec = TemplateDefinitionUtil.getFirstElement(td);
        this.currentRuleDefinition = sec;
        this.processVocabularys(this.currentRuleDefinition.getVocabulary());
        assertTrue(this.currentRuleDefinition.getVocabulary().size() == 0);
        assertTrue(this.currentRuleDefinition.getAttribute().size() == 0);
    }

    @Test
    public void testProcessVocabularys3() throws Exception {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_flat_vocab2.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition sec = TemplateDefinitionUtil.getFirstElement(td);
        RuleDefinition code = RuleDefinitionUtil.getElementByName(sec, "hl7:code");
        this.currentRuleDefinition = code;
        this.processVocabularys(this.currentRuleDefinition.getVocabulary());
        assertTrue(this.currentRuleDefinition.getVocabulary().size() == 2);
        assertTrue(this.currentRuleDefinition.getAttribute().size() == 0);
        assertTrue(RuleDefinitionUtil.getAttributeByName(currentRuleDefinition, "code") == null);
    }

    @Test
    public void testProcessVocabularys4() throws Exception {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_flat_vocab3.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition sec = TemplateDefinitionUtil.getFirstElement(td);
        RuleDefinition code = RuleDefinitionUtil.getElementByName(sec, "hl7:code");
        this.currentRuleDefinition = code;
        this.processVocabularys(this.currentRuleDefinition.getVocabulary());
        assertTrue(this.currentRuleDefinition.getVocabulary().size() == 1);
        assertTrue(this.currentRuleDefinition.getAttribute().size() == 0);
        assertTrue(RuleDefinitionUtil.getAttributeByName(currentRuleDefinition, "code") == null);
    }

}
