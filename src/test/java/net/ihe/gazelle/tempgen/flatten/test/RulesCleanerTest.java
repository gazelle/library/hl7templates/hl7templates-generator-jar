package net.ihe.gazelle.tempgen.flatten.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.flatten.action.RulesCleaner;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class RulesCleanerTest extends RulesCleaner {
	
	Decor decorTemplates = null;
	Decor decorTemplates2 = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_cleaner.xml");
		decorTemplates2 = DecorMarshaller.loadDecor("src/test/resources/decor_cleaner.xml");
	}

	@Test
	public void testCleanRules() {
		assertTrue(RulesUtil.getTemplates(decorTemplates.getRules()).size()==2);
		RulesCleaner.cleanRules(decorTemplates.getRules());
		assertTrue(RulesUtil.getTemplates(decorTemplates.getRules()).size()==1);
	}
	
	@Test
	public void testVerifyIfThereAreABetterTemplateStatus() throws Exception {
		TemplateDefinition first = RulesUtil.getTemplates(decorTemplates2.getRules()).get(0);
		TemplateDefinition second = RulesUtil.getTemplates(decorTemplates2.getRules()).get(1);
		assertFalse(verifyIfThereAreABetterTemplateStatus(first, RulesUtil.getTemplates(decorTemplates2.getRules())));
		assertTrue(verifyIfThereAreABetterTemplateStatus(second, RulesUtil.getTemplates(decorTemplates2.getRules())));
	}

}
