package net.ihe.gazelle.tempgen.flatten.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempapi.interfaces.AssertProcessor;
import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempgen.flatten.action.GeneralFlattenImplProvider;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class GeneralFlattenImplProviderTest {

	@SuppressWarnings("rawtypes")
	@Test
	public void testProvideImpl() {
		AssertProcessor ap = (new GeneralFlattenImplProvider()).provideImpl(AssertProcessor.class);
		assertTrue(ap != null);
		Processor aproc = (new GeneralFlattenImplProvider()).provideImpl(Processor.class);
		assertTrue(aproc == null);
	}

}
