package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempgen.action.MultiplicityUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class MultiplicityUtilTest {

	@Test
	public void testExtractMaximumMultiplicity() {
		Integer aa = MultiplicityUtil.extractMaximumMultiplicity("1");
		assertTrue(aa.equals(1));
		aa = MultiplicityUtil.extractMaximumMultiplicity("-1");
		assertTrue(aa.equals(-1));
		aa = MultiplicityUtil.extractMaximumMultiplicity("*");
		assertTrue(aa.equals(MultiplicityUtil.MAX_INTEGER));
		aa = MultiplicityUtil.extractMaximumMultiplicity("aaa");
		assertTrue(aa == null);
	}

}
