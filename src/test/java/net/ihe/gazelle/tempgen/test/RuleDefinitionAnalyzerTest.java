package net.ihe.gazelle.tempgen.test;

import net.ihe.gazelle.goc.template.definer.model.Taml;
import net.ihe.gazelle.goc.uml.utils.OwnedRuleUtil;
import net.ihe.gazelle.goc.uml.utils.PackagedElementUtil;
import net.ihe.gazelle.goc.uml.utils.XMIUtil;
import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.OwnedRuleKind;
import net.ihe.gazelle.tempgen.action.DistinguisherFollower;
import net.ihe.gazelle.tempgen.action.MultiplicityUtil;
import net.ihe.gazelle.tempgen.action.RuleDefinitionAnalyzer;
import net.ihe.gazelle.tempgen.action.TamlHandler;
import net.ihe.gazelle.tempgen.flatten.action.GeneralFlattenDecor;
import net.ihe.gazelle.tempgen.flatten.action.RulesCleaner;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempgen.inc.action.IncludeFlattener;
import net.ihe.gazelle.tempgen.rules.analyzer.RDMaximumMultiplicityAnalyzer;
import net.ihe.gazelle.tempmodel.dpath.model.DAttribute;
import net.ihe.gazelle.tempmodel.dpath.model.DElement;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.Pair;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;
import net.ihe.gazelle.validation.Notification;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Abderrazek Boufahja, Cedric Eoche-Duval
 */
public class RuleDefinitionAnalyzerTest extends RuleDefinitionAnalyzer {

    private static final Logger LOG = LoggerFactory.getLogger(RuleDefinitionAnalyzerTest.class);

    Decor decIsClosed = null;

    RuleDefinition firstRDIsClosed = null;

    @Before
    public void setUp() {
        decIsClosed = DecorMarshaller.loadDecor("src/test/resources/rd/decor_closed.xml");
        firstRDIsClosed = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decIsClosed.getRules()).get(0));
    }

    @Test
    public void testProcessIsClosed1() {
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(firstRDIsClosed, "hl7:entry");
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processIsClosed(true);
        List<OwnedRule> lor = PackagedElementUtil.findOwnedRulesByContent(currentPackagedElement,
                "In Section Active Problems, the sub-element encounter of /hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.6']"
                        + "/hl7:entry[hl7:act/hl7:templateId/@root='1.2.3.4'] SHALL NOT be provided (the element is closed)");
        assertTrue(lor.size() == 1); // verify is closed generated for the element parent closed
        lor = PackagedElementUtil.findOwnedRulesByContent(currentPackagedElement,
                "In Section Active Problems, the sub-element typeId ");
        assertTrue(lor.size() == 1); // verify is closed generated for the children elements
    }

    @Test
    public void testProcessIsClosed2() {
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(firstRDIsClosed, "hl7:entry");
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(this.selectedRuleDefinition, "hl7:act");
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processIsClosed(true);
        List<OwnedRule> lor = PackagedElementUtil.findOwnedRulesByContent(currentPackagedElement,
                "unless those specified in the template definition");
        // verify is closed generated for the templateId element : which is already specified as present but with a distinguisher
        assertTrue(lor.size() == 1);
        // verify that the size is well generated
        System.out.println();
        assertTrue(lor.get(0).getSpecification().getBody().contains(" templateId->size()=templateId->select(((not root.oclIsUndefined()) "
                + "and root='1.2.3.33') or ((not root.oclIsUndefined()) and root='1.2.3.4'))->size() "));
        assertTrue(PackagedElementUtil.findOwnedRulesByContent(currentPackagedElement, "effectiveTime").size() == 0);
        assertTrue(PackagedElementUtil.findOwnedRulesByContent(currentPackagedElement, "statusCode").size() == 1);
    }

    @Test
    public void testProcessIsClosed3() {
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementsByName(firstRDIsClosed, "hl7:entry").get(1);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processIsClosed(true);
        List<OwnedRule> lor = PackagedElementUtil.findOwnedRulesByContent(currentPackagedElement,
                "unless");
        // verify there are no generation of requirements related to template Id
        assertTrue(lor.size() == 0);
    }

    @Test
    public void testProcess() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(this.selectedRuleDefinition,
                "hl7:participantRole");
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:addr");
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.process(null, this.currentPackagedElement);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.process(firstRDIsClosed);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.process(firstRDIsClosed, this.currentPackagedElement);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() > 0);
    }

    @Test
    public void testProcessAttributes1() {
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementsByName(firstRDIsClosed, "hl7:entry").get(0);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:act");
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:templateId");
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        DistinguisherFollower.cleanDistinguishers();
        this.processAttributes(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processAttributes(this.selectedRuleDefinition.getAttribute());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() > 0);
        assertTrue(OwnedRuleUtil.containStringDescription(this.currentPackagedElement.getOwnedRule(),
                "the attribute root SHALL have the value '1.2.3.4' if present"));
    }

    @Test
    public void testProcessAttributes2() {
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementsByName(firstRDIsClosed, "hl7:entry").get(0);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:act");
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:templateId");
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processAttributes(new ArrayList<Attribute>());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        Attribute attr = new Attribute();
        attr.setName("root");
        attr.setValue("1111");
        this.processAttributes(Arrays.asList(new Attribute[]{attr}));
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        assertTrue(ProblemHandler.findNotification("attribute attribute is not defined in the right place") != null);
    }

    @Test
    public void testProcessChoices() throws FileNotFoundException, JAXBException {
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementsByName(firstRDIsClosed, "hl7:entry").get(0);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processChoices(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition participantRole = RuleDefinitionUtil.getElementByName(TemplateDefinitionUtil.getFirstElement(td), "hl7:participantRole");
        ChoiceDefinition currentChoiceDefinition = RuleDefinitionUtil.getChoices(participantRole).get(0);
        this.selectedRuleDefinition = participantRole;
        this.processChoices(Arrays.asList(new ChoiceDefinition[]{currentChoiceDefinition}));
        assertTrue(this.currentPackagedElement.getOwnedRule().size() > 0);
        assertTrue(!PackagedElementUtil.findOwnedRulesByContent(currentPackagedElement,
                "the number of elements of type 'playingDevice', 'playingEntity' SHALL be "
                        + "bigger or equal to 1").isEmpty());
    }

    @Test
    public void testProcessDatatype() {
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementsByName(firstRDIsClosed, "hl7:entry").get(0);
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processDatatype(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(firstRDIsClosed, "hl7:templateId");
        this.processDatatype(new QName(null, "II"));
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processDatatype(new QName(null, "II.EPSOS"));
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processDatatype(new QName(null, "AD"));
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
    }

    @Test
    public void testProcessElements() {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.selectedRuleDefinition = firstRDIsClosed;
        this.processElements(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processElements(RuleDefinitionUtil.getElements(firstRDIsClosed));
        assertTrue(this.currentPackagedElement.getOwnedRule().size() > 0);
        assertTrue(!PackagedElementUtil.findOwnedRulesByContent(currentPackagedElement, "at least ONE").isEmpty());
    }

    @Test
    public void testProcessElements2() {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.selectedRuleDefinition = firstRDIsClosed;
        this.ignoreTemplateIdRequirements = false;
        this.processElements(RuleDefinitionUtil.getElements(firstRDIsClosed));
        assertTrue(!PackagedElementUtil.findOwnedRulesByContent(currentPackagedElement,
                "In Section Active Problems, in /hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.6']/"
                        + "hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.6'], the attribute root SHALL be present").isEmpty());
    }

    @Test
    public void testProcessElements3() {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.selectedRuleDefinition = firstRDIsClosed;
        this.ignoreTemplateIdRequirements = true;
        this.processElements(RuleDefinitionUtil.getElements(firstRDIsClosed));
        assertTrue(PackagedElementUtil.findOwnedRulesByContent(currentPackagedElement,
                "In Section Active Problems, in /hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.6']/"
                        + "hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.6'], the attribute root SHALL be present").isEmpty());
    }

    @Test
    public void testProcessIncludes() {
        List<Notification> ln = ProblemHandler.findNotifications("the includes elements are ignored in ruleDefinitions");
        Integer orig = getSizeLN(ln);
        this.processIncludes(null);
        List<Notification> ln2 = ProblemHandler.findNotifications("the includes elements are ignored in ruleDefinitions");
        Integer fin = getSizeLN(ln2);
        assertTrue(orig == fin);
        this.processIncludes(Arrays.asList(new IncludeDefinition[]{}));
        ln2 = ProblemHandler.findNotifications("the includes elements are ignored in ruleDefinitions");
        fin = getSizeLN(ln2);
        assertTrue(orig == fin);
        this.processIncludes(Arrays.asList(new IncludeDefinition[]{new IncludeDefinition()}));
        ln2 = ProblemHandler.findNotifications("the includes elements are ignored in ruleDefinitions");
        fin = getSizeLN(ln2);
        assertTrue(orig >= 0 || orig + 1 == fin);
    }

    private Integer getSizeLN(List<Notification> ln) {
        Integer i = 0;
        if (ln != null) {
            i = ln.size();
        }
        return i;
    }

    @Test
    public void testProcessIsMandatory1() {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processIsMandatory(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementsByName(firstRDIsClosed, "hl7:entry").get(0);
        this.processIsMandatory(false);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processIsMandatory(true);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
        assertTrue(this.currentPackagedElement.getOwnedRule().get(0).getOwnedComment().getBody().
                contains("SHALL not have nullFlavor"));
    }

    @Test
    public void testProcessIsMandatory2() throws FileNotFoundException, JAXBException {
        Decor decIsClosed2 = DecorMarshaller.loadDecor("src/test/resources/decor_text_m.xml");
        RuleDefinition firstRDIsClosed2 = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decIsClosed2.getRules()).get(0));
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementsByName(firstRDIsClosed2, "hl7:text").get(0);
        this.processIsMandatory(false);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processIsMandatory(true);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
    }

    @Test
    public void testProcessIsMandatory3() throws FileNotFoundException, JAXBException {
        Decor decIsClosed2 = DecorMarshaller.loadDecor("src/test/resources/decor_text_m2.xml");
        RuleDefinition firstRDIsClosed2 = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decIsClosed2.getRules()).get(0));
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementsByName(firstRDIsClosed2, "hl7:text").get(0);
        this.processIsMandatory(false);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processIsMandatory(true);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
    }

    @Test
    public void testProcessItem() throws FileNotFoundException, JAXBException {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(this.selectedRuleDefinition,
                "hl7:participantRole");
        this.process(this.selectedRuleDefinition, this.currentPackagedElement);
        XMI xmi = XMIUtil.createXMI("test");
        TamlHandler.addTamlToXMI(xmi);
        System.out.println(xmi.getTaml().size());
        assertTrue(xmi.getTaml().size() > 0);
        boolean containAA = false;
        for (Taml tamlcontent : xmi.getTaml()) {
            if (tamlcontent.getIDs().contains("AA")) {
                containAA = true;
            }
        }
        assertTrue(containAA);
    }

    @Test
    public void testProcessMaximumMultiplicity() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(this.selectedRuleDefinition,
                "hl7:participantRole");
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:addr");
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processMaximumMultiplicity("*");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processMaximumMultiplicity("aaa");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processMaximumMultiplicity("1");
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
    }

    @Test
    public void testProcessMinimumMultiplicity() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(this.selectedRuleDefinition,
                "hl7:participantRole");
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:addr");
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processMinimumMultiplicity(0);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processMinimumMultiplicity(-1);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processMinimumMultiplicity(1);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
        this.processMinimumMultiplicity(2);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 2);
    }

    @Test
    public void testProcessMinimumMultiplicity2() throws FileNotFoundException, JAXBException {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_abrumet_rs.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition comp = RuleDefinitionUtil.getElementByName(firstRD, "hl7:component");
        RuleDefinition structuredBody = RuleDefinitionUtil.getElementByName(comp, "hl7:structuredBody");
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(structuredBody, "hl7:component");
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.processMinimumMultiplicity(1);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
    }

    @Test
    public void testProcessPropertys() throws FileNotFoundException, JAXBException {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.processPropertys(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processPropertys(this.selectedRuleDefinition.getProperty());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        dec = DecorMarshaller.loadDecor("src/test/resources/decor_prop.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(firstRD, "hl7:quantity");
        this.processPropertys(this.selectedRuleDefinition.getProperty());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
    }

    @Test
    public void testProcessTexts() throws FileNotFoundException, JAXBException {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.processTexts(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processTexts(new ArrayList<String>());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processTexts(this.selectedRuleDefinition.getText());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        dec = DecorMarshaller.loadDecor("src/test/resources/decor_section.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(firstRD, "cda:title");
        this.processTexts(this.selectedRuleDefinition.getText());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
    }

    @Test
    public void testProcessVocabularys1() throws FileNotFoundException, JAXBException {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.processVocabularys(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processVocabularys(this.selectedRuleDefinition.getVocabulary());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:participantRole");
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:scopingEntity");
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:code");
        this.processVocabularys(this.selectedRuleDefinition.getVocabulary());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
    }

    @Test
    public void testProcessVocabularys1bis() throws FileNotFoundException, JAXBException {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_vs_ignored.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.processVocabularys(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processVocabularys(this.selectedRuleDefinition.getVocabulary());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:participantRole");
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:scopingEntity");
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:code");
        this.processVocabularys(this.selectedRuleDefinition.getVocabulary());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
    }

    @Test
    public void testProcessVocabularys2() throws FileNotFoundException, JAXBException {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_exception1.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.processVocabularys(null);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.processVocabularys(this.selectedRuleDefinition.getVocabulary());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:participantRole");
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:scopingEntity");
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:code");
        this.processVocabularys(this.selectedRuleDefinition.getVocabulary());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 2);
        assertTrue(!PackagedElementUtil.findOwnedRulesByContent(this.currentPackagedElement, ", the nullFlavor attribute of").isEmpty());
    }

    @Test
    public void testProcessVocabularys3() throws FileNotFoundException, JAXBException {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_exception3.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:participantRole");
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:scopingEntity");
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:code");
        this.processVocabularys(this.selectedRuleDefinition.getVocabulary());
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 2);
        assertTrue(!PackagedElementUtil.findOwnedRulesByContent(this.currentPackagedElement, ", the nullFlavor attribute of").isEmpty());
    }

    @Test
    public void testGetRuleContextDescription() {
        this.setRuleContextDescription("rcd");
        assertTrue(this.getRuleContextDescription().equals("rcd"));
    }

    @Test
    public void testSetRuleContextDescription() {
        this.setRuleContextDescription(null);
        assertTrue(this.ruleContextDescription == null);
    }

    @Test
    public void testGetSelectedRuleDefinition() {
        this.selectedRuleDefinition = new RuleDefinition();
        assertTrue(this.getSelectedRuleDefinition() != null);
    }

    @Test
    public void testSetSelectedRuleDefinition() {
        this.setSelectedRuleDefinition(new RuleDefinition());
        assertTrue(this.selectedRuleDefinition != null);
    }

    @Test
    public void testSubElementHasUsefulDistinguisher1() {
        RuleDefinition entry0 = RuleDefinitionUtil.getElementByName(firstRDIsClosed, "hl7:entry");
        assertFalse(this.subElementHasUsefulDistinguisher("act", entry0));
        assertFalse(this.subElementHasUsefulDistinguisher("alpha", entry0));
        assertFalse(this.subElementHasUsefulDistinguisher("observation", entry0));
        RuleDefinition entry1 = RuleDefinitionUtil.getElementsByName(firstRDIsClosed, "hl7:entry").get(1);
        assertFalse(this.subElementHasUsefulDistinguisher("templateId", entry1));
        RuleDefinition act = RuleDefinitionUtil.getElementByName(entry0, "hl7:act");
        assertTrue(this.subElementHasUsefulDistinguisher("templateId", act));
    }

    @Test
    public void testFulfillOwnedRuleForConstraintGenerator() {
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
        this.fulfillOwnedRuleForConstraintGenerator(null, this.currentPackagedElement, OwnedRuleKind.CARDINALITY);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 0);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(firstRDIsClosed, "hl7:entry");
        this.fulfillOwnedRuleForConstraintGenerator(new RDMaximumMultiplicityAnalyzer(), this.currentPackagedElement, OwnedRuleKind.CARDINALITY);
        assertTrue(this.currentPackagedElement.getOwnedRule().size() == 1);
        assertTrue(this.currentPackagedElement.getOwnedRule().get(0).getOwnedComment().getBody().
                contains("SHALL contain at most ONE hl7:entry[hl7:act/hl7:templateId/@root='1.2.3.4']"));
        assertEquals("self.entry->select(act.templateId.root->exists(aa : String| aa = '1.2.3.4'))->size()<2",
                this.currentPackagedElement.getOwnedRule().get(0).getSpecification().getBody());
    }

    @Test
    public void testExtractElementMaxMultiplicity() throws FileNotFoundException, JAXBException {
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(firstRDIsClosed, "hl7:entry");
        assertTrue(this.extractElementMaxMultiplicity(selectedRuleDefinition) == MultiplicityUtil.MAX_INTEGER);
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(this.selectedRuleDefinition,
                "hl7:participantRole");
        assertTrue(this.extractElementMaxMultiplicity(selectedRuleDefinition) == 1);
    }

    @Test
    public void testIgnoreMinCardinality() throws FileNotFoundException, JAXBException {
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(firstRDIsClosed, "hl7:entry");
        assertFalse(this.ignoreMinCardinality());
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(this.selectedRuleDefinition,
                "hl7:participantRole");
        ChoiceDefinition cd = RuleDefinitionUtil.getChoices(this.selectedRuleDefinition).get(0);
        this.selectedRuleDefinition = ChoiceDefinitionUtil.getElements(cd).get(0);
        assertTrue(this.ignoreMinCardinality());
    }

    @Test
    public void testFollowDistinguisher() throws Exception {
        DElement ruleDefinitionDParent = new DElement();
        ruleDefinitionDParent.setName("addr");
        ruleDefinitionDParent.setFollowingAttributeOrElement(new DAttribute());
        ruleDefinitionDParent.getFollowingAttributeOrElement().setName("use");
        followDistinguisher("test", ruleDefinitionDParent);
        assertTrue(DistinguisherFollower.verifyIfPathAndValueAreDistinguisher("/hl7:addr/@use", "test"));
    }

    @Test
    public void testUpdateDistinguisherFollowerIfNeeded1() throws Exception {
        DistinguisherFollower.cleanDistinguishers();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_choice2.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.updateDistinguisherFollowerIfNeeded();
        RuleDefinition rd = RuleDefinitionUtil.getElementByName(this.selectedRuleDefinition, "hl7:participantRole");
        rd = RuleDefinitionUtil.getElementByName(rd, "hl7:scopingEntity");
        rd = RuleDefinitionUtil.getElementByName(rd, "hl7:code");
        assertTrue(DistinguisherFollower.verifyIfAttributeAndValueAreDistinguisher(rd.getAttribute().get(0), "code", "123"));
        System.out.println(DistinguisherFollower.showDifferentDistinguisher());
        assertTrue(DistinguisherFollower.showDifferentDistinguisher().contains(
                "/hl7:participant/hl7:participantRole/hl7:scopingEntity/hl7:code[@code='123']/@code = 123"));
    }

    @Test
    public void testUpdateDistinguisherFollowerIfNeeded2() throws Exception {
        DistinguisherFollower.cleanDistinguishers();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_choice2.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(this.selectedRuleDefinition, "hl7:participantRole");
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(this.selectedRuleDefinition, "hl7:scopingEntity");
        this.updateDistinguisherFollowerIfNeeded();
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(this.selectedRuleDefinition, "hl7:code");
        assertFalse(DistinguisherFollower.verifyIfAttributeAndValueAreDistinguisher(this.selectedRuleDefinition.getAttribute().get(0), "code", "123"
        ));
        assertTrue(DistinguisherFollower.showDifferentDistinguisher().contains("total number distinguisher : 0"));
    }

    @Test
    public void testUpdateDistinguisherFollowerIfNeeded3() throws Exception {
        DistinguisherFollower.cleanDistinguishers();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs_dist1.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(this.selectedRuleDefinition, "hl7:entryRelationship");
        this.updateDistinguisherFollowerIfNeeded();
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(this.selectedRuleDefinition, "hl7:observation");
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(this.selectedRuleDefinition, "hl7:code");
        assertTrue(DistinguisherFollower.verifyIfAttributeAndValueAreDistinguisher(this.selectedRuleDefinition.getAttribute().get(0),
                "code", "48766-0"));
        System.out.println(DistinguisherFollower.showDifferentDistinguisher());
        assertTrue(DistinguisherFollower.showDifferentDistinguisher().contains(
                "/hl7:act[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']/hl7:entryRelationship"
                        + "[hl7:observation/hl7:code/@code='48766-0']/hl7:observation/hl7:code[@code='48766-0']/@code = 48766-0"));
    }

    @Test
    public void testUpdateDistinguisherFollowerIfNeeded4() throws Exception {
        DistinguisherFollower.cleanDistinguishers();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting6.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.updateDistinguisherFollowerIfNeeded();
        assertTrue(DistinguisherFollower.showDifferentDistinguisher().contains("total number distinguisher : 0"));
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(this.selectedRuleDefinition, "hl7:templateId");
        this.updateDistinguisherFollowerIfNeeded();
        assertTrue(DistinguisherFollower.showDifferentDistinguisher().contains("total number distinguisher : 1"));
        assertTrue(DistinguisherFollower.showDifferentDistinguisher().contains(
                "/hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/"
                        + "hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/@root = 1.3.6.1.4.1.19376.1.5.3.1.3.16.1"));
    }

    @Test
    public void testUpdateListDistinguisherFollower1() throws Exception {
        DistinguisherFollower.cleanDistinguishers();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting6.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(this.selectedRuleDefinition, "hl7:templateId");
        RuleDefinitionAnalyzer.updateListDistinguisherFollower(selectedRuleDefinition, new
                Pair<String, String>("/@root", "1.3.6.1.4.1.19376.1.5.3.1.3.16.1"));
        assertTrue(DistinguisherFollower.showDifferentDistinguisher().contains("total number distinguisher : 1"));
        assertTrue(DistinguisherFollower.showDifferentDistinguisher().contains(
                "/hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/hl7:templateId[@root"
                        + "='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/@root = 1.3.6.1.4.1.19376.1.5.3.1.3.16.1"));
    }

    @Test
    public void testUpdateListDistinguisherFollower2() throws Exception {
        DistinguisherFollower.cleanDistinguishers();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting6.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(this.selectedRuleDefinition, "hl7:templateId");
        RuleDefinitionAnalyzer.updateListDistinguisherFollower(selectedRuleDefinition, new
                Pair<String, String>("aaaa", "1.3.6.1.4.1.19376.1.5.3.1.3.16.1"));
        assertTrue(DistinguisherFollower.showDifferentDistinguisher().contains("total number distinguisher : 0"));
    }

    @Test
    public void testMoveItemDistinguisher1() throws Exception {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_item.xml");
        RuleDefinition section = TemplateDefinitionUtil.getFirstElement(
                RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition entr1 = RuleDefinitionUtil.getElementByName(section, "hl7:entryRelationship");
        RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr1, "hl7:observation");
        RuleDefinition temp = RuleDefinitionUtil.getElementByName(obs, "hl7:templateId");
        Attribute attr = temp.getAttribute().get(0);
        moveItemDistinguisher(attr, entr1);
        assertTrue(entr1.getItem().getLabel().equals("TestItem"));
    }

    @Test
    public void testMoveItemDistinguisher2() throws Exception {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_item.xml");
        RuleDefinition section = TemplateDefinitionUtil.getFirstElement(
                RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition entr1 = RuleDefinitionUtil.getElementByName(section, "hl7:entryRelationship");
        RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr1, "hl7:observation");
        RuleDefinition temp = RuleDefinitionUtil.getElementByName(obs, "hl7:templateId");
        Attribute attr = temp.getAttribute().get(0);
        entr1.setItem(new Item());
        entr1.getItem().setLabel("Hellooo");
        moveItemDistinguisher(attr, entr1);
        assertTrue(entr1.getItem().getLabel().equals("Hellooo / TestItem"));
    }

    @Test
    public void testMoveItemDistinguisher3() throws Exception {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_item.xml");
        RuleDefinition section = TemplateDefinitionUtil.getFirstElement(
                RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition entr1 = RuleDefinitionUtil.getElementByName(section, "hl7:entryRelationship");
        RuleDefinition obs = RuleDefinitionUtil.getElementByName(entr1, "hl7:observation");
        RuleDefinition temp = RuleDefinitionUtil.getElementByName(obs, "hl7:templateId");
        Attribute attr = temp.getAttribute().get(0);
        moveItemDistinguisher(attr, null);
        moveItemDistinguisher(null, entr1);
        assertTrue(entr1.getItem() == null);
        moveItemDistinguisher(null, null);
    }

    /**
     * this test the moveItemDistinguisher when updating list distinguisherFollower
     *
     * @throws Exception
     */
    @Test
    public void testUpdateListDistinguisherFollower3() throws Exception {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_item.xml");
        RuleDefinition section = TemplateDefinitionUtil.getFirstElement(
                RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition entr1 = RuleDefinitionUtil.getElementByName(section, "hl7:entryRelationship");
        updateListDistinguisherFollower(entr1, new Pair<String, String>("/hl7:observation/hl7:templateId/@root", "1.2.3"));
        assertTrue(entr1.getItem().getLabel().equals("TestItem"));
    }

    @Test
    public void testCheckIfConstraintCanBeGeneratedForMinimumMultiplicity1() throws Exception {
        assertFalse(checkIfConstraintCanBeGeneratedForMinimumMultiplicity(null, null));
        assertFalse(checkIfConstraintCanBeGeneratedForMinimumMultiplicity(1, null));
        assertFalse(checkIfConstraintCanBeGeneratedForMinimumMultiplicity(null, new RuleDefinition()));
        assertFalse(checkIfConstraintCanBeGeneratedForMinimumMultiplicity(0, new RuleDefinition()));
        assertTrue(checkIfConstraintCanBeGeneratedForMinimumMultiplicity(1, new RuleDefinition()));
    }

    @Test
    public void testCheckIfConstraintCanBeGeneratedForMinimumMultiplicity2() throws Exception {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_item.xml");
        RuleDefinition section = TemplateDefinitionUtil.getFirstElement(
                RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition entr1 = RuleDefinitionUtil.getElementByName(section, "hl7:entryRelationship");
        assertTrue(checkIfConstraintCanBeGeneratedForMinimumMultiplicity(1, entr1));
        assertFalse(checkIfConstraintCanBeGeneratedForMinimumMultiplicity(0, entr1));
    }

    @Test
    public void testCheckIfConstraintCanBeGeneratedForMinimumMultiplicity3() throws Exception {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_choice2.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        this.selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        this.updateDistinguisherFollowerIfNeeded();
        RuleDefinition rd = RuleDefinitionUtil.getElementByName(this.selectedRuleDefinition, "hl7:participantRole");
        assertFalse(checkIfConstraintCanBeGeneratedForMinimumMultiplicity(0, rd));
        assertFalse(checkIfConstraintCanBeGeneratedForMinimumMultiplicity(1, rd));
        assertTrue(checkIfConstraintCanBeGeneratedForMinimumMultiplicity(2, rd));
    }

    @Test
    public void testCheckIfConstraintCanBeGeneratedForMaximumMultiplicityInteger() {
        // must return true if max is defined and contain an integer
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_ter_max.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entry");
        assertTrue(checkIfConstraintCanBeGeneratedForMaximumMultiplicity("4", entr));
    }

    @Test
    public void testCheckIfConstraintCanBeGeneratedForMaximumMultiplicityWildcard() {
        // must return false if max is defined and contain a '*'
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_ter.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entry");
        assertFalse(checkIfConstraintCanBeGeneratedForMaximumMultiplicity("*", entr));
    }

    @Test
    public void testCheckIfConstraintCanBeGeneratedForMaximumMultiplicityNotDefined() {
        // must return false if max is not defined
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_ter.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entry");
        assertFalse(checkIfConstraintCanBeGeneratedForMaximumMultiplicity(null, entr));
    }

    @Test
    public void testCheckIfConstraintCanBeGeneratedForMaximumMultiplicityError() {
        // must return false and log an error if max is defined but is neither an integer neither a wildcard
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_ter.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entry");
        assertFalse(checkIfConstraintCanBeGeneratedForMaximumMultiplicity("N", entr));
        assertNotNull(ProblemHandler.findNotification("Invalid maximum multiplicity value: N"));
    }


    @Test
    public void testProcessContainMin1() {

        // Init (start from file flattenized level 1, then flattenized level 2)
        currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_2.xml");
        decorTemplates = flattenDecorForTest(decorTemplates);
        TemplateDefinition td = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(td);
        selectedRuleDefinition = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entry");

        // Core test
        this.process(selectedRuleDefinition, currentPackagedElement);
        List<OwnedRule> rules = currentPackagedElement.getOwnedRule();
        OwnedRule containMinRule = PackagedElementUtil.findOwnedRulesByContent(currentPackagedElement, "SHALL contain at least ONE hl7:templateId")
                .get(0);
        assertEquals(
                "In Section Coded Social History, /hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/hl7:entry/hl7:observation" +
                        "/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] SHALL contain at least ONE " +
                        "hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4']",
                containMinRule.getOwnedComment().getBody());
        assertEquals("(not self.nullFlavor.oclIsUndefined()) or self.entry->reject(nullFlavor.oclIsUndefined()).observation" +
                        "->reject(nullFlavor.oclIsUndefined()).templateId->select((not root.oclIsUndefined()) " +
                        "and root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4')->size()>0",
                containMinRule.getSpecification().getBody());
    }

    @Test
    public void testProcessContainNoMin() {

        // Init (strart from file flattenized level 1, then flattenized level 2)
        currentPackagedElement = PackagedElementUtil.initPackagedElement();
        Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_min_zero.xml");
        decorTemplates = flattenDecorForTest(decorTemplates);
        TemplateDefinition td = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(td);
        selectedRuleDefinition = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entry");

        // Core test
        this.process(selectedRuleDefinition, currentPackagedElement);
        List<OwnedRule> rules = currentPackagedElement.getOwnedRule();
        List<OwnedRule> containNoMinRules = PackagedElementUtil
                .findOwnedRulesByContent(currentPackagedElement, "SHALL contain at least ONE hl7:templateId");
        assertEquals("Process should not have generated any minimum constraints for min='0'", 0, containNoMinRules.size());
    }

    @Test
    public void testProcessContainMax() {

        // Init (strart from file flattenized level 1, then flattenized level 2)
        currentPackagedElement = PackagedElementUtil.initPackagedElement();
        selectedRuleDefinition = getHL7EntryRDForContainTests("src/test/resources/contain/decor_contain_min_zero.xml");

        // Core test
        this.process(selectedRuleDefinition, currentPackagedElement);
        List<OwnedRule> rules = currentPackagedElement.getOwnedRule();
        OwnedRule containMaxRules = PackagedElementUtil
                .findOwnedRulesByContent(currentPackagedElement, "SHALL contain at most ONE hl7:templateId").get(0);
        assertEquals(
                "In Section Coded Social History, /hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/hl7:entry/hl7:observation" +
                        "/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] SHALL contain at most ONE " +
                        "hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4']",
                containMaxRules.getOwnedComment().getBody());
        assertEquals("self.entry.observation.templateId->select((not root.oclIsUndefined()) and root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4')->size()<2",
                containMaxRules.getSpecification().getBody());
    }

    @Test
    public void testProcessContainNoMax() {

        // Init (strart from file flattenized level 1, then flattenized level 2)
        currentPackagedElement = PackagedElementUtil.initPackagedElement();
        selectedRuleDefinition = getHL7EntryRDForContainTests("src/test/resources/contain/decor_contain_2.xml");

        // Core test
        this.process(selectedRuleDefinition, currentPackagedElement);
        List<OwnedRule> rules = currentPackagedElement.getOwnedRule();
        List<OwnedRule> containNoMaxRules = PackagedElementUtil
                .findOwnedRulesByContent(currentPackagedElement, "SHALL contain at most");
        assertEquals("Process should not have generated any maximum constraints for max='*'", 0, containNoMaxRules.size());

    }


    @Test
    public void testProcessContainIsMandatory() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_ter_mandatory.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entry");
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();

        this.process(selectedRuleDefinition, currentPackagedElement);
        List<OwnedRule> containIsMandatory = PackagedElementUtil
                .findOwnedRulesByContent(currentPackagedElement, "SHALL not have nullFlavor (mandatory)");
        assertEquals(2, containIsMandatory.size());
    }

    @Test
    public void testProcessContainIsNotMandatory() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/contain/decor_contain_ter_not_mandatory.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        this.selectedRuleDefinition = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entry");
        this.currentPackagedElement = PackagedElementUtil.initPackagedElement();

        this.process(selectedRuleDefinition, currentPackagedElement);
        List<OwnedRule> containIsMandatory = PackagedElementUtil
                .findOwnedRulesByContent(currentPackagedElement, "SHALL not have nullFlavor (mandatory)");
        assertEquals(0, containIsMandatory.size());

    }


    private RuleDefinition getHL7EntryRDForContainTests(String samplePath) {
        Decor decorTemplates = DecorMarshaller.loadDecor(samplePath);
        decorTemplates = flattenDecorForTest(decorTemplates);
        TemplateDefinition td = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(td);
        return RuleDefinitionUtil.getElementByName(firstRD, "hl7:entry");
    }

    private Decor flattenDecorForTest(Decor decor) {
        RulesCleaner.cleanRules(decor.getRules());
        decor = GeneralFlattenDecor.generalFlattenDecor(decor);
        decor = GeneralFlattenDecor.generalFlattenBisDecor(decor);
        decor = IncludeFlattener.flattenIncludeInDecor(decor);
        return decor;
    }

}
