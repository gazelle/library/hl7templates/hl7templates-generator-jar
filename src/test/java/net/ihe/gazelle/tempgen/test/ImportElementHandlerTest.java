package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.goc.uml.Model;
import net.ihe.gazelle.goc.uml.utils.ElementImportUtil;
import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.goc.xmi.XMIMarshaller;
import net.ihe.gazelle.tempgen.action.ImportElementHandler;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ImportElementHandlerTest {
	
	XMI xmi = new XMI();
	
	@Before
	public void setUp(){
		xmi.setModel(new Model());
	}

	@Test
	public void testHandleAddingElement1() throws JAXBException {
		int oldsize = xmi.getModel().getElementImport().size();
		boolean containCD = ImportElementHandler.getSetImported().contains("CD");
		ImportElementHandler.handleAddingElement("CD");
		ImportElementHandler.addElementsToBeImported(xmi);
		if (!containCD) {
			assertTrue(xmi.getModel().getElementImport().size()>oldsize);
		}
		assertTrue(ElementImportUtil.findElementImport(xmi.getModel(), "CD").getImportedElement().getType().equals("uml:Class"));
		
		containCD = ImportElementHandler.getSetImported().contains("POCDMT000040Act");
		oldsize = xmi.getModel().getElementImport().size();
		ImportElementHandler.handleAddingElement("POCDMT000040Act");
		xmi.setModel(new Model());
		ImportElementHandler.addElementsToBeImported(xmi);
		if (!containCD) {
			assertTrue(xmi.getModel().getElementImport().size()==oldsize+1);
		}
		XMIMarshaller.printXMI(xmi, System.out);
		assertTrue(ElementImportUtil.findElementImport(xmi.getModel(), "POCDMT000040Act").getImportedElement().getType().equals("uml:Class"));
		
		containCD = ImportElementHandler.getSetImported().contains("ActClass");
		oldsize = xmi.getModel().getElementImport().size();
		ImportElementHandler.handleAddingElement("ActClass");
		xmi.setModel(new Model());
		ImportElementHandler.addElementsToBeImported(xmi);
		if (!containCD) {
			assertTrue(xmi.getModel().getElementImport().size()==oldsize+1);
		}
		assertTrue(ElementImportUtil.findElementImport(xmi.getModel(), "ActClass").getImportedElement().getType().equals("uml:Enumeration"));
		
	}
	
	@Test
	public void testHandleAddingElement2() throws JAXBException {
		ImportElementHandler.handleAddingElement("CommonOperationsStatic");
		ImportElementHandler.addElementsToBeImported(xmi);
		XMIMarshaller.printXMI(xmi, System.out);
		assertTrue(ElementImportUtil.findElementImport(xmi.getModel(), "CommonOperationsStatic").getImportedElement().getType().equals("uml:Class"));
	}
	
	@Test
	public void testgetMapImportedToUML() throws Exception {
		assertTrue(ImportElementHandler.getMapImportedToUML() != null);
	}
	
	@Test
	public void testAddElementsToBeImported() throws Exception {
		ImportElementHandler.addElementsToBeImported(null);
		assertTrue(ProblemHandler.getDiagnostic().size()>0);
	}
}
