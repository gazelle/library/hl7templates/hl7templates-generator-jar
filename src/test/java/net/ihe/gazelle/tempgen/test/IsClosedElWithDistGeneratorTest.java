package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempgen.rules.analyzer.IsClosedElWithDistGenerator;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IsClosedElWithDistGeneratorTest {
	
	Decor decIsClosed = null;
	
	RuleDefinition firstRDIsClosed = null;
	
	@Before
	public void setUp(){
		decIsClosed = DecorMarshaller.loadDecor("src/test/resources/rd/decor_closed.xml");
		firstRDIsClosed = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decIsClosed.getRules()).get(0));
	}

	@Test
	public void testGetSetDistinguishers() {
		IsClosedElWithDistGenerator ss = new IsClosedElWithDistGenerator();
		ss.setSetDistinguishers(new HashSet<String>());
		assertTrue(ss.getSetDistinguishers() != null);
	}

	@Test
	public void testGetSubElement() {
		IsClosedElWithDistGenerator gen = new IsClosedElWithDistGenerator();
		gen.setSubElement("test");
		assertTrue(gen.getSubElement().equals("test"));
	}

	@Test
	public void testGenerateOCLConstraint1() {
		IsClosedElWithDistGenerator gen = new IsClosedElWithDistGenerator();
		gen.setSubElement("templateId");
		String ocl = gen.generateOCLConstraint(firstRDIsClosed);
		System.out.println(ocl);
		assertTrue(ocl.contains("self.templateId->size()=self.templateId->select()->size()"));
	}
	
	@Test
	public void testGenerateOCLConstraint2() {
		IsClosedElWithDistGenerator gen = new IsClosedElWithDistGenerator();
		gen.setSubElement("templateId");
		gen.setSetDistinguishers(new HashSet<String>());
		gen.getSetDistinguishers().add("root='1.2.3'");
		String ocl = gen.generateOCLConstraint(firstRDIsClosed);
		System.out.println(ocl);
		assertTrue(ocl.contains("self.templateId->size()=self.templateId->select(root='1.2.3')->size()"));
	}
	
	@Test
	public void testGenerateOCLConstraint3() {
		IsClosedElWithDistGenerator gen = new IsClosedElWithDistGenerator();
		gen.setSubElement("templateId");
		gen.setSetDistinguishers(new HashSet<String>());
		gen.getSetDistinguishers().add("root='1.2.3'");
		gen.getSetDistinguishers().add("root='1.2.4'");
		String ocl = gen.generateOCLConstraint(firstRDIsClosed);
		System.out.println(ocl);
		assertTrue(ocl.contains("select((root='1.2.3') or (root='1.2.4'))") || ocl.contains("select((root='1.2.4') or (root='1.2.3'))"));
	}

	@Test
	public void testGenerateCommentConstraint() {
		IsClosedElWithDistGenerator gen = new IsClosedElWithDistGenerator();
		gen.setSubElement("templateId");
		gen.setSetDistinguishers(new HashSet<String>());
		gen.getSetDistinguishers().add("root='1.2.3'");
		String comment = gen.generateCommentConstraint(firstRDIsClosed);
		assertTrue(comment.equals("In Section Active Problems, the sub-element templateId of "
				+ "/hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.6'] SHALL NOT "
				+ "be provided (the element is closed), unless those specified in the template definition. "
				+ "Some of the sub-element(s) templateId provided are illegal."));
	}

	@Test
	public void testGetProcessIdentifier() {
		assertTrue((new IsClosedElWithDistGenerator()).getProcessIdentifier().equals(AnalyzerEnum.RD_ISCLOSED_PROCESS.getValue()));
	}

}
