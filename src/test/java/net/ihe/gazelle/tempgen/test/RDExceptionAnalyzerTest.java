package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempgen.rules.analyzer.RDExceptionAnalyzer;
import net.ihe.gazelle.tempgen.rules.analyzer.RDVocabularyAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public class RDExceptionAnalyzerTest {

	@Test
	public void testGenerateOCLConstraint1() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_exception1.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		RuleDefinition selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
		selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:participantRole");
		selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:scopingEntity");
		selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:code");
		String gen = (new RDExceptionAnalyzer()).generateOCLConstraint(selectedRuleDefinition);
		assertTrue(gen.equals("self.participantRole.scopingEntity.code->"
				+ "forAll(nullFlavor.oclIsUndefined() or nullFlavor=NullFlavor::NI or nullFlavor=NullFlavor::UNK)"));
	}
	
	@Test
	public void testGenerateOCLConstraint2()  {
		String gen = (new RDExceptionAnalyzer()).generateOCLConstraint(null);
		assertTrue(gen.equals(""));
	}
	
	@Test
	public void testGenerateOCLConstraint3() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_exception2.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		RuleDefinition selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
		selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:participantRole");
		selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:scopingEntity");
		selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:code");
		String gen = (new RDExceptionAnalyzer()).generateOCLConstraint(selectedRuleDefinition);
		System.out.println(gen);
		assertTrue(gen.equals("self.participantRole.scopingEntity.code->"
				+ "forAll(nullFlavor.oclIsUndefined() or nullFlavor=NullFlavor::NA or nullFlavor=NullFlavor::NI or nullFlavor=NullFlavor::UNK)"));
	}

	@Test
	public void testGenerateCommentConstraint() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_exception1.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		RuleDefinition selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
		selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:participantRole");
		selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:scopingEntity");
		selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:code");
		String gen = (new RDExceptionAnalyzer()).generateCommentConstraint(selectedRuleDefinition);
		assertTrue(gen.equals("In CDA Participant (Body), the nullFlavor attribute of "
				+ "/hl7:participant/hl7:participantRole/hl7:scopingEntity/hl7:code SHALL have a value from : {NI ,UNK}"));
	}

	@Test
	public void testGetProcessIdentifier() {
		assertTrue(new RDVocabularyAnalyzer().getProcessIdentifier().equals(AnalyzerEnum.RD_VOCAB_PROCESS.getValue()));
	}

}
