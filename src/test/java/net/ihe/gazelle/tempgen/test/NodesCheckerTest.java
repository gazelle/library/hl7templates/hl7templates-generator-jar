package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import net.ihe.gazelle.tempgen.nodes.check.NodeCirc;
import net.ihe.gazelle.tempgen.nodes.check.NodeCirc.Color;
import net.ihe.gazelle.tempgen.nodes.check.NodesChecker;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class NodesCheckerTest extends NodesChecker {

	@Test
	public void testExtractCircularCall() {
		Map<String, Set<String>> mapIncludedTemplates = new HashMap<String, Set<String>>();
		mapIncludedTemplates.put("1.2.3", new HashSet<String>(Arrays.asList(new String[]{"1.2.4", "1.2.5", "1.2.6"})));
		mapIncludedTemplates.put("1.2.4", new HashSet<String>(Arrays.asList(new String[]{"1.2.5", "1.2.7"})));
		mapIncludedTemplates.put("1.2.5", new HashSet<String>(Arrays.asList(new String[]{"1.2.4", "1.2.8"})));
		Map<String, Set<String>> mapCircularTemplates =  NodesChecker.extractCircularCall(mapIncludedTemplates, null);
		System.out.println(mapCircularTemplates);
		assertTrue(mapCircularTemplates.containsKey("1.2.4"));
		assertTrue(mapCircularTemplates.get("1.2.4").contains("1.2.5"));
		assertTrue(mapCircularTemplates.get("1.2.5").contains("1.2.4"));
		
	}

	@Test
	public void testProcessNodeColor() {
		NodeCirc nc = new NodeCirc();
		nc.getListChildNodes().add(new NodeCirc());
		nc.getListChildNodes().add(new NodeCirc());
		assertFalse(processNodeColor(nc));
		nc.getListChildNodes().get(0).setColor(Color.BLACK);
		assertFalse(processNodeColor(nc));
		nc.getListChildNodes().get(1).setColor(Color.BLACK);
		assertTrue(processNodeColor(nc));
	}

	@Test
	public void testFindNodesByColor() {
		NodeCirc nc = new NodeCirc();
		nc.getListChildNodes().add(new NodeCirc());
		nc.getListChildNodes().add(new NodeCirc());
		List<NodeCirc> aa = findNodesByColor(nc.getListChildNodes(), Arrays.asList(new Color[] {Color.WHITE, Color.GRAY}));
		assertTrue(aa.size()==2);
		nc.getListChildNodes().get(0).setColor(Color.BLACK);
		aa = findNodesByColor(nc.getListChildNodes(), Arrays.asList(new Color[] {Color.WHITE}));
		assertTrue(aa.size()==1);
		aa = findNodesByColor(nc.getListChildNodes(), Arrays.asList(new Color[] {Color.GRAY}));
		assertTrue(aa.size()==0);
		aa = findNodesByColor(nc.getListChildNodes(), Arrays.asList(new Color[] {Color.WHITE, Color.BLACK}));
		assertTrue(aa.size()==2);
	}

	@Test
	public void testCreateNodesForParsing() {
		Map<String, Set<String>> mapIncludedTemplates = new HashMap<String, Set<String>>();
		mapIncludedTemplates.put("test1", new HashSet<String>());
		mapIncludedTemplates.get("test1").add("aa");
		mapIncludedTemplates.get("test1").add("bb");
		mapIncludedTemplates.put("test2", new HashSet<String>());
		mapIncludedTemplates.get("test2").add("bb");
		mapIncludedTemplates.get("test2").add("cc");
		List<NodeCirc> ln = createNodesForParsing(mapIncludedTemplates);
		assertTrue(ln.size()==5);
		assertTrue(findNodeByName(ln, "test1") != null);
		assertTrue(findNodeByName(ln, "cc") != null);
		assertTrue(findNodeByName(ln, "test1").getListChildNodes().size()==2);
	}

	@Test
	public void testExtractListConcernedTemplates() {
		Map<String, Set<String>> mapIncludedTemplates = new HashMap<String, Set<String>>();
		mapIncludedTemplates.put("test1", new HashSet<String>());
		mapIncludedTemplates.get("test1").add("aa");
		mapIncludedTemplates.get("test1").add("bb");
		mapIncludedTemplates.put("test2", new HashSet<String>());
		mapIncludedTemplates.get("test2").add("bb");
		mapIncludedTemplates.get("test2").add("cc");
		Set<String> ss = extractListConcernedTemplates(mapIncludedTemplates);
		assertTrue(ss.size()==5);
		assertTrue(ss.containsAll(Arrays.asList(new String[]{"test1", "test2", "aa", "bb", "cc"})));
		assertTrue(extractListConcernedTemplates(null).isEmpty());
	}

	@Test
	public void testFindNodeByName() {
		List<NodeCirc> lnc = new ArrayList<NodeCirc>();
		lnc.add(new NodeCirc());
		lnc.add(new NodeCirc());
		lnc.get(0).setNodeName("test1");
		lnc.get(1).setNodeName("rr");
		NodeCirc nc = findNodeByName(lnc, "rr");
		assertTrue(nc != null);
		assertTrue(findNodeByName(null, "") == null);
	}

}
