package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempgen.statistics.RDNameAsXpath;

public class RDNameAsXpathTest {

	@Test
	public void testHashCode() {
		RDNameAsXpath pp = new RDNameAsXpath();
		pp.setValue("aaa");
		pp.setPathIsIgnorable(true);
		RDNameAsXpath pp2 = new RDNameAsXpath();
		pp2.setValue("aaa");
		pp2.setPathIsIgnorable(true);
		assertTrue(pp.equals(pp2));
	}

	@Test
	public void testEqualsObject() {
		RDNameAsXpath pp = new RDNameAsXpath();
		pp.setValue("aaa");
		pp.setPathIsIgnorable(true);
		RDNameAsXpath pp2 = new RDNameAsXpath();
		pp2.setValue("aaa");
		pp2.setPathIsIgnorable(true);
		assertTrue(pp.hashCode() == pp2.hashCode());
	}
	
	@Test
	public void testGetValue() throws Exception {
		RDNameAsXpath pp = new RDNameAsXpath();
		pp.setValue("aaa");
		assertTrue(pp.getValue().equals("aaa"));
	}
	
	@Test
	public void testEqualsObject2() {
		RDNameAsXpath pp = new RDNameAsXpath();
		assertTrue(pp.equals(new RDNameAsXpath()));
		assertFalse(pp.equals("aa"));
		assertFalse(pp.equals(null));
		RDNameAsXpath pp2 = new RDNameAsXpath();
		pp2.setValue("ee");
		assertFalse(pp2.equals(pp));
		assertFalse(pp.equals(pp2));
	}

}
