package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.tempgen.action.TextAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionProblemUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class TextAnalyzerTest extends TextAnalyzer {
	
Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_section.xml");
	}

	@Test
	public void testProcessText() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition title = RuleDefinitionUtil.getElementByName(firstRD, "cda:title");
		PackagedElement pe = new PackagedElement();
		(new TextAnalyzer()).process(title.getText(), title, pe, null);
		assertTrue(pe.getOwnedRule().size()==1);

	}

	@Test
	public void testIsStringDatatype() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition title = RuleDefinitionUtil.getElementByName(firstRD, "cda:title");
		assertTrue(RuleDefinitionProblemUtil.isStringDatatype(title));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "cda:code");
		assertFalse(RuleDefinitionProblemUtil.isStringDatatype(code));
	}

	@Test
	public void testGenerateCommentConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition title = RuleDefinitionUtil.getElementByName(firstRD, "cda:title");
		this.setSelectedRuleDefinition(title);
		String comment = this.generateCommentConstraint(title.getText());
		assertTrue(comment.equals("In Hospital Discharge Diagnosis Section, "
				+ "in /hl7:section[hl7:templateId/@root='2.16.840.1.113883.10.20.22.2.24']/hl7:title, "
				+ "the string value shall have the value 'test'"));
	}

	@Test
	public void testExtractOCLFromProp() {
		String ss = "ss";
		System.out.println(this.extractOCLFromProp(ss));
		assertTrue(this.extractOCLFromProp(ss).equals("getListStringValues()->forAll(st : String | st='ss')"));
	}

	@Test
	public void testGenerateOCLConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition title = RuleDefinitionUtil.getElementByName(firstRD, "cda:title");
		this.setSelectedRuleDefinition(title);
		String ocl = this.generateOCLConstraint(title.getText());
		assertTrue(ocl.equals("self.title->reject(not nullFlavor.oclIsUndefined())->" +
				"forAll((getListStringValues()->forAll(st : String | st='test')))"));
	}

	@Test
	public void testGenerateDescrForProp() {
		assertTrue(this.generateDescrForProp("ss").equals("the string value shall have the value 'ss'"));
	}

}
