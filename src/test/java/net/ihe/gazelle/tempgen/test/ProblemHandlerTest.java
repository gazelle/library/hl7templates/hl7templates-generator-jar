package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.validation.Notification;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ProblemHandlerTest {

	@Test
	public void testGetDiagnostic() {
		int hh = ProblemHandler.getDiagnostic().size();
		ProblemHandler.handleError("test", "proc");
		assertTrue(ProblemHandler.getDiagnostic().size()==hh+1);
		ProblemHandler.handleError("test", "proc");
		assertTrue(ProblemHandler.getDiagnostic().size()==hh+1);
		ProblemHandler.handleError("test", "proc1");
		assertTrue(ProblemHandler.getDiagnostic().size()==hh+2);
	}

	@Test
	public void testHandleError() {
		int hh = ProblemHandler.getDiagnostic().size();
		ProblemHandler.handleError("zzzz", "proazeazeac");
		assertTrue(ProblemHandler.getDiagnostic().size()==hh+1);
	}

	@Test
	public void testFindNotification() {
		ProblemHandler.handleError("test", "procaaa");
		Notification aa = ProblemHandler.findNotification("test");
		assertTrue(aa != null);
	}
	
	@Test
	public void testEqualsNotif() {
		int hh = ProblemHandler.getDiagnostic().size();
		ProblemHandler.handleError("test1", "proc1");
		ProblemHandler.handleError("test1", "proc1");
		assertTrue(ProblemHandler.getDiagnostic().size()==hh+1);
	}
	@Test
	public void testEqualsNotif2() {
		int hh = ProblemHandler.getDiagnostic().size();
		ProblemHandler.handleError("test1--", "proc1");
		System.out.println("h = " + hh + ", 2 = " + ProblemHandler.getDiagnostic().size());
		ProblemHandler.handleError("test2---", "proc2");
		System.out.println("h = " + hh + ", 2 = " + ProblemHandler.getDiagnostic().size());
		assertTrue(ProblemHandler.getDiagnostic().size()==hh+2);
	}
	
}
