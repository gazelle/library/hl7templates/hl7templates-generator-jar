package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.rules.analyzer.AnalyzerEnum;
import net.ihe.gazelle.tempgen.rules.analyzer.RDVocabularyAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class RDVocabularyTest extends RDVocabularyAnalyzer {
	
	Decor decorVocab1 = null;
	
	Decor decorVocab2 = null;
	
	@Before
	public void setUp(){
		decorVocab1 = DecorMarshaller.loadDecor("src/test/resources/decor_vocab1.xml");
		decorVocab2 = DecorMarshaller.loadDecor("src/test/resources/decor_vocab2.xml");
	}

	@Test
	public void testGenerateOCLConstraint1() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorVocab1.getRules()).get(0));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
		String ocl = (new RDVocabularyAnalyzer()).generateOCLConstraint(code);
		System.out.println(ocl);
		assertEquals(ocl, "self.code->forAll( (not nullFlavor.oclIsUndefined()) or "
				+ "((not code.oclIsUndefined()) and code='29762-2' and ((codeSystem.oclIsUndefined()) or "
				+ "codeSystem='2.16.840.1.113883.6.1')))");
	}
	
	@Test
	public void testGenerateOCLConstraint2() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorVocab2.getRules()).get(0));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
		String ocl = (new RDVocabularyAnalyzer()).generateOCLConstraint(code);
		assertEquals(ocl, "self.code->forAll( (not nullFlavor.oclIsUndefined()) or "
				+ "((not code.oclIsUndefined()) and "
				+ "CommonOperationsStatic::matchesValueSet('1.3.6.1.4.1.12559.11.10.1.3.1.42.9&version=2015-01-01T01:02:03', code, codeSystem, null, null)))");
		RuleDefinition statusCode = RuleDefinitionUtil.getElementByName(firstRD, "hl7:statusCode");
		ocl = (new RDVocabularyAnalyzer()).generateOCLConstraint(statusCode);
		assertEquals(ocl, "self.statusCode->forAll( (not nullFlavor.oclIsUndefined()) or "
				+ "((not code.oclIsUndefined()) and code='completed' and ((codeSystem.oclIsUndefined()) or codeSystem='2.16.840.1.113883.5.14')))");
		RuleDefinition value = RuleDefinitionUtil.getElementByName(firstRD, "hl7:value");
		ocl = (new RDVocabularyAnalyzer()).generateOCLConstraint(value);
		System.out.println(ocl);
		assertEquals(ocl, "self.value->forAll( (not nullFlavor.oclIsUndefined()) or "
				+ "((not oclAsType(CS).code.oclIsUndefined()) and "
				+ "CommonOperationsStatic::matchesCodeToValueSet('1.3.6.1.4.1.12559.11.10.1.3.1.42.10&version=2015-01-01T01:02:02', oclAsType(CS).code)) or "
				+ "(((oclAsType(CS).codeSystem.oclIsUndefined()) or oclAsType(CS).codeSystem='1.2.3')))");
	}

	@Test
	public void testExtractConstraintForVocabulary1() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorVocab1.getRules()).get(0));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
		String oclFragment = this.extractConstraintForVocabulary(code.getVocabulary().get(0), code, "CE", false);
		assertEquals(oclFragment, "(not code.oclIsUndefined()) and code='29762-2' and "
				+ "((codeSystem.oclIsUndefined()) or codeSystem='2.16.840.1.113883.6.1')");
	}
	
	@Test
	public void testExtractConstraintForVocabulary2() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorVocab2.getRules()).get(0));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
		String oclFragment = this.extractConstraintForVocabulary(code.getVocabulary().get(0), code, "CE", false);
		assertEquals(oclFragment, "(not code.oclIsUndefined()) and "
				+ "CommonOperationsStatic::matchesValueSet('1.3.6.1.4.1.12559.11.10.1.3.1.42.9&version=2015-01-01T01:02:03', code, codeSystem, null, null)");
	}
	
	@Test
	public void testExtractConstraintForVocabulary3() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorVocab2.getRules()).get(0));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:statusCode");
		String oclFragment = this.extractConstraintForVocabulary(code.getVocabulary().get(0), code, "CS", false);
		assertEquals(oclFragment, "(not code.oclIsUndefined()) and code='completed' and "
				+ "((codeSystem.oclIsUndefined()) or codeSystem='2.16.840.1.113883.5.14')");
	}
	
	@Test
	public void testExtractConstraintForVocabulary4() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorVocab2.getRules()).get(0));
		RuleDefinition value = RuleDefinitionUtil.getElementByName(firstRD, "hl7:value");
		String oclFragment = this.extractConstraintForVocabulary(value.getVocabulary().get(0), value, "CS", true);
		System.out.println(oclFragment);
		assertEquals(oclFragment, "(not oclAsType(CS).code.oclIsUndefined()) and "
				+ "CommonOperationsStatic::matchesCodeToValueSet('1.3.6.1.4.1.12559.11.10.1.3.1.42.10&version=2015-01-01T01:02:02', oclAsType(CS).code)");
		oclFragment = this.extractConstraintForVocabulary(value.getVocabulary().get(0), value, "CS", false);
		assertEquals(oclFragment, "(not code.oclIsUndefined()) and CommonOperationsStatic::matchesCodeToValueSet('1.3.6.1.4.1.12559.11.10.1.3.1.42.10&version=2015-01-01T01:02:02', code)");
		oclFragment = this.extractConstraintForVocabulary(value.getVocabulary().get(0), value, "CD", true);
		assertEquals(oclFragment, "(not oclAsType(CD).code.oclIsUndefined()) and "
				+ "CommonOperationsStatic::matchesValueSet('1.3.6.1.4.1.12559.11.10.1.3.1.42.10&version=2015-01-01T01:02:02', oclAsType(CD).code, oclAsType(CD).codeSystem, null, null)");
		oclFragment = this.extractConstraintForVocabulary(value.getVocabulary().get(1), value, "CD", true);
		System.out.println(oclFragment);
		assertEquals(oclFragment, "((oclAsType(CD).codeSystem.oclIsUndefined()) or oclAsType(CD).codeSystem='1.2.3')");
	}

	@Test
	public void testGenerateCommentConstraint1() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorVocab1.getRules()).get(0));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
		String comment = (new RDVocabularyAnalyzer().generateCommentConstraint(code));
		assertEquals(comment,"In Section Coded Social History, the code of "
				+ "/hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/hl7:code SHALL have "
				+ "code='29762-2'and codeSytem='2.16.840.1.113883.6.1'");
	}
	
	@Test
	public void testGenerateCommentConstraint2() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorVocab2.getRules()).get(0));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
		String comment = (new RDVocabularyAnalyzer().generateCommentConstraint(code));
		System.out.println("comment = " + comment);
		assertEquals(comment, "In Pregnancy Observation, the code of /hl7:observation[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.5']/hl7:code SHALL "
				+ "be from the valueSet 1.3.6.1.4.1.12559.11.10.1.3.1.42.9 (flexibility : 2015-01-01T01:02:03)");
	}
	
	@Test
	public void testGenerateCommentConstraint3() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorVocab2.getRules()).get(0));
		RuleDefinition value = RuleDefinitionUtil.getElementByName(firstRD, "hl7:value");
		String comment = (new RDVocabularyAnalyzer().generateCommentConstraint(value));
		assertEquals(comment, "In Pregnancy Observation, the code of /hl7:observation[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.5']/hl7:value "
				+ "SHALL be from the valueSet 1.3.6.1.4.1.12559.11.10.1.3.1.42.10 (flexibility : 2015-01-01T01:02:02) OR SHALL have codeSystem='1.2.3'");
	}

	@Test
	public void testGetProcessIdentifier() {
		assertTrue(new RDVocabularyAnalyzer().getProcessIdentifier().equals(AnalyzerEnum.RD_VOCAB_PROCESS.getValue()));
	}

}
