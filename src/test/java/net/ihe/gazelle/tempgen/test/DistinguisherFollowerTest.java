package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.action.DistinguisherFollower;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;

public class DistinguisherFollowerTest {
	
	@Before
	public void before() {
		DistinguisherFollower.followDistinguisher("/hl7:addr/@use", "HP");
		DistinguisherFollower.followDistinguisher("/hl7:section[@classCode='DOCSECT']/@classCode", "DOCSECT");
	}

	@Test
	public void testFollowDistinguisher() {
		DistinguisherFollower.followDistinguisher("/hl7:id/@root", "1.2.3");
		assertTrue(DistinguisherFollower.verifyIfPathAndValueAreDistinguisher("/hl7:id/@root", "1.2.3"));
	}

	@Test
	public void testVerifyIfPathAndValueAreDistinguisher() {
		assertTrue(DistinguisherFollower.verifyIfPathAndValueAreDistinguisher("/hl7:addr/@use", "HP"));
	}

	@Test
	public void testVerifyIfAttributeAndValueAreDistinguisher() {
		RuleDefinition rd = new RuleDefinition();
		rd.setName("hl7:section");
		rd.getAttribute().add(new Attribute());
		rd.getAttribute().get(0).setClassCode("DOCSECT");
		rd.getAttribute().get(0).setParentObject(rd);
		TemplateDefinition td = new TemplateDefinition();
		rd.setParentObject(td);
		td.getAttributeOrChoiceOrElement().add(rd);
		Decor dec = new Decor();
		dec.setRules(new Rules());
		dec.getRules().setParentObject(dec);
		dec.getRules().getTemplateAssociationOrTemplate().add(td);
		td.setParentObject(dec.getRules());
		assertTrue(DistinguisherFollower.verifyIfAttributeAndValueAreDistinguisher(rd.getAttribute().get(0), "classCode", "DOCSECT"));
		assertFalse(DistinguisherFollower.verifyIfAttributeAndValueAreDistinguisher(rd.getAttribute().get(0), "classCode", "AAA"));
	}
	
	@Test
	public void testCleanDistinguishers() throws Exception {
		DistinguisherFollower.cleanDistinguishers();
		assertFalse(DistinguisherFollower.verifyIfPathAndValueAreDistinguisher("/hl7:addr/@use", "HP"));
	}
	
	@Test
	public void testUnFollowDistinguisher() throws Exception {
		DistinguisherFollower.followDistinguisher("/hl7:id/@root", "1.2.3");
		DistinguisherFollower.unFollowDistinguisher("/hl7:addr/@use");
		assertFalse(DistinguisherFollower.verifyIfPathAndValueAreDistinguisher("/hl7:addr/@use", "HP"));
		DistinguisherFollower.unFollowDistinguisher("");
		assertTrue(DistinguisherFollower.verifyIfPathAndValueAreDistinguisher("/hl7:id/@root", "1.2.3"));
	}
	
	@Test
	public void testShowDifferentDistinguisher() throws Exception {
		String ss = DistinguisherFollower.showDifferentDistinguisher();
		assertTrue(ss.contains("/hl7:addr/@use"));
	}

}
