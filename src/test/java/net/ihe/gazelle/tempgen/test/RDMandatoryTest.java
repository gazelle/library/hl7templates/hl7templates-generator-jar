package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.rules.analyzer.RDMandatoryAnalyzer;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class RDMandatoryTest {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_custodian.xml");
	}

	@Test
	public void testGenerateOCLConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition custodian = RuleDefinitionUtil.getElementByName(firstRD, "cda:custodian");
		assertTrue((new RDMandatoryAnalyzer()).generateOCLConstraint(custodian).equals(
				"self.custodian->forAll(nullFlavor.oclIsUndefined())"));
		RuleDefinition assignedCustodian = RuleDefinitionUtil.getElementByName(custodian, "cda:assignedCustodian");
		assertTrue((new RDMandatoryAnalyzer()).generateOCLConstraint(assignedCustodian).equals(
				"self.custodian.assignedCustodian->forAll(nullFlavor.oclIsUndefined())"));
	}

	@Test
	public void testGenerateCommentConstraint() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition custodian = RuleDefinitionUtil.getElementByName(firstRD, "cda:custodian");
		assertTrue((new RDMandatoryAnalyzer()).generateCommentConstraint(custodian).equals(
				"In US Realm Header, in /hl7:ClinicalDocument[hl7:templateId/@root='2.16.840.1.113883.10.20.22.1.1'], "
				+ "the element(s) hl7:custodian SHALL not have nullFlavor (mandatory)"));
		RuleDefinition assignedCustodian = RuleDefinitionUtil.getElementByName(custodian, "cda:assignedCustodian");
		assertTrue((new RDMandatoryAnalyzer()).generateCommentConstraint(assignedCustodian).equals(
				"In US Realm Header, in /hl7:ClinicalDocument[hl7:templateId/@root='2.16.840.1.113883.10.20.22.1.1']/hl7:custodian, "
				+ "the element(s) hl7:assignedCustodian SHALL not have nullFlavor (mandatory)"));
	}

}
