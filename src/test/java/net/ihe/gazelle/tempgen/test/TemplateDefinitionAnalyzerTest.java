package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import net.ihe.gazelle.goc.uml.utils.PackagedElementUtil;
import net.ihe.gazelle.goc.uml.utils.XMIUtil;
import net.ihe.gazelle.goc.xmi.XMI;
import net.ihe.gazelle.goc.xmi.XMIMarshaller;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.tempgen.action.TemplateDefinitionAnalyzer;
import net.ihe.gazelle.tempgen.handler.ProblemHandler;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;

public class TemplateDefinitionAnalyzerTest extends TemplateDefinitionAnalyzer {

	@Test
	public void testProcess() {
		
	}

	@Test
	public void testProcessAttributes() {
		TemplateDefinition td = new TemplateDefinition();
		td.setId("ee");
		td.setName("aa");
		this.process(td, new XMI());
		this.processAttributes(new ArrayList<Attribute>(Arrays.asList(new Attribute[]{new Attribute()})));
		assertTrue(ProblemHandler.findNotification("There are no way to have such situation : the attributes elements are ignored in ") != null);
	}

	@Test
	public void testProcessChoices() {
		TemplateDefinition td = new TemplateDefinition();
		td.setId("ee");
		td.setName("aa");
		this.process(td, new XMI());
		this.processChoices(new ArrayList<ChoiceDefinition>(Arrays.asList(new ChoiceDefinition[]{new ChoiceDefinition()})));
		assertTrue(ProblemHandler.findNotification("here are no way to have such situation : the choice element are ignored in") != null);
	}

	
	@Test
	public void testProcessElements() throws JAXBException {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_assert.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		XMI xmi = XMIUtil.createXMI("test");
		this.process(td, xmi);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMIMarshaller.printXMI(xmi, baos);
		assertTrue(baos.toString().contains("(not self.nullFlavor.oclIsUndefined()) or self.templateId-&gt;select((not root.oclIsUndefined()) and root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2')-&gt;size()&gt;0"));
	}

	@Test
	public void testProcessIncludes() {
		TemplateDefinition td = new TemplateDefinition();
		td.setId("ee");
		td.setName("aa");
		this.process(td, new XMI());
		this.processIncludes(new ArrayList<IncludeDefinition>(Arrays.asList(new IncludeDefinition[]{new IncludeDefinition()})));
		assertTrue(ProblemHandler.findNotification("There are no way to have such situation : the include elements are ignored in ") != null);
	}

	@Test
	public void testProcessItem() {
		// TODO
	}

	@Test
	public void testAttachTemplateSpec() throws JAXBException {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_assert.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		XMI xmi = XMIUtil.createXMI("test");
		PackagedElement pe = PackagedElementUtil.initPackagedElement();
		this.attachTemplateSpec(td, xmi , pe);
		XMIMarshaller.printXMI(xmi, System.out);
		assertTrue(xmi.getTemplateSpec().get(0).getPath().equals("templateId.root"));
	}

	@Test
	public void testGenerateGeneralizationForPackagedElement() throws JAXBException {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_assert.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		XMI xmi = XMIUtil.createXMI("test");
		this.process(td, xmi);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMIMarshaller.printXMI(xmi, baos);
		assertTrue(baos.toString().contains("<general href=\"../../cda-model/models/cda.uml#_DAJdBa70EeGxJei_o6JmIA\" xmi:type=\"uml:Class\"/>"));
	}
	
	@Test
	public void testProcessElements2() throws JAXBException {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_comptest.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		XMI xmi = XMIUtil.createXMI("test");
		this.process(td, xmi);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMIMarshaller.printXMI(xmi, baos);
		assertTrue(baos.toString().contains("select((not typeCode.oclIsUndefined()) and typeCode=XActRelationshipEntryRelationship::REFR"));
	}

}
