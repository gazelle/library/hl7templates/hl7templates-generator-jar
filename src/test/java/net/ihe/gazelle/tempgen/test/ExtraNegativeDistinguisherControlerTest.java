package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;

import org.junit.Test;

import net.ihe.gazelle.tempgen.gui.control.ExtraNegativeDistinguisherControler;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public class ExtraNegativeDistinguisherControlerTest extends ExtraNegativeDistinguisherControler {

	@Test
	public void testAcceptNegativeDistinguisher() {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_section.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition title = RuleDefinitionUtil.getElementByName(firstRD, "cda:title");
		System.setIn(new ByteArrayInputStream("y".getBytes()));
		assertTrue(this.acceptNegativeDistinguisher(title));
		System.setIn(new ByteArrayInputStream("n".getBytes()));
		assertFalse(this.acceptNegativeDistinguisher(title));
	}

	@Test
	public void testExtractQuestion() {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_section.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition title = RuleDefinitionUtil.getElementByName(firstRD, "cda:title");
		assertTrue(this.extractQuestion(title).equals("Do you accept a negative distinguisher for the RuleDefinition : "
				+ "/hl7:section[hl7:templateId/@root='2.16.840.1.113883.10.20.22.2.24']/hl7:title?"));
		assertTrue(this.extractQuestion(firstRD).equals("Do you accept a negative distinguisher for the RuleDefinition : "
				+ "/hl7:section[hl7:templateId/@root='2.16.840.1.113883.10.20.22.2.24']?"));
	}

}
