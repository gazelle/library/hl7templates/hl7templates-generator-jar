package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.common.marshaller.ObjectMarshaller;
import net.ihe.gazelle.common.marshaller.ObjectMarshallerImpl;
import net.ihe.gazelle.goc.uml.utils.PackagedElementUtil;
import net.ihe.gazelle.goc.xmm.OwnedRule;
import net.ihe.gazelle.goc.xmm.OwnedRuleType;
import net.ihe.gazelle.goc.xmm.PackagedElement;
import net.ihe.gazelle.tempgen.action.AssertAnalyzer;
import net.ihe.gazelle.tempgen.action.GenerationProperties;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AssertRole;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Let;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AssertAnalyzerTest extends AssertAnalyzer {
	
	
	private static Logger log = LoggerFactory.getLogger(AssertAnalyzerTest.class);
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		GenerationProperties.ignoreAssertionGeneration = false;
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_assert.xml");
	}

	@Test
	public void testProcess() throws JAXBException {
		this.currentAssert = new Assert();
		this.currentAssert.setTest("hl7:id/@root='1.2.3'");
		this.currentAssert.setRole(AssertRole.ERROR);
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		this.currentAssert.setParentObject(firstRD);
		this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
		this.listGeneratedOwnedRule = new ArrayList<OwnedRule>();
		this.flattenAssert(currentAssert, 0);
		this.process(this.currentAssert, this.currentPackagedElement, this.listGeneratedOwnedRule);
		assertTrue(this.currentPackagedElement.getOwnedRule().size()>0);
		assertTrue(!this.currentPackagedElement.getOwnedRule().get(0).getSpecification().getBody().equals(""));
	}
	
	@Test
	public void testProcess2() throws JAXBException {
		this.currentAssert = new Assert();
		this.currentAssert.setTest("hl7:id/@root='1.2.3'");
		this.currentAssert.setRole(AssertRole.ERROR);
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		this.currentAssert.setParentObject(firstRD);
		this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
		this.listGeneratedOwnedRule = new ArrayList<OwnedRule>();
		this.flattenAssert(currentAssert, 0);
		GenerationProperties.ignoreAssertionGeneration = true;
		this.process(this.currentAssert, this.currentPackagedElement, this.listGeneratedOwnedRule);
		assertTrue(this.currentPackagedElement.getOwnedRule().isEmpty());
	}
	
	@Test
	public void testProcess3() throws JAXBException {
		this.currentAssert = new Assert();
		this.currentAssert.setTest("hl7:id/@root='1.2.3'");
		this.currentAssert.setRole(AssertRole.HINT);
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		this.currentAssert.setParentObject(firstRD);
		this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
		this.listGeneratedOwnedRule = new ArrayList<OwnedRule>();
		this.flattenAssert(currentAssert, 0);
		GenerationProperties.ignoreAssertionGeneration = false;
		this.process(this.currentAssert, this.currentPackagedElement, this.listGeneratedOwnedRule);
		assertTrue(!this.currentPackagedElement.getOwnedRule().isEmpty());
		assertTrue(this.currentPackagedElement.getOwnedRule().get(0).getOwnedRuleType() == OwnedRuleType.INFO);
	}

	@Test
	public void testProcessTest1() {
		this.currentAssert = new Assert();
		this.currentAssert.setTest("hl7:id/@root='1.2.3'");
		this.currentAssert.setRole(AssertRole.ERROR);
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		this.currentAssert.setParentObject(firstRD);
		this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
		this.listGeneratedOwnedRule = new ArrayList<OwnedRule>();
		this.flattenAssert(currentAssert, 0);
		this.processTest("hl7:id/@root='1.2.3'");
		assertTrue(this.currentPackagedElement.getOwnedRule().size()>0);
		assertTrue(!this.currentPackagedElement.getOwnedRule().get(0).getSpecification().getBody().equals(""));
	}
	
	@Test
	public void testProcessTest2() {
		this.currentAssert = new Assert();
		this.currentAssert.setTest("hl7:id/@root='1.2.3'");
		this.currentAssert.setRole(AssertRole.ERROR);
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		this.currentAssert.setParentObject(firstRD);
		this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
		this.listGeneratedOwnedRule = new ArrayList<OwnedRule>();
		this.flattenAssert(currentAssert, 0);
		this.processTest(null);
		assertTrue(this.currentPackagedElement.getOwnedRule().isEmpty());
	}
	
	@Test
	public void testFlattenAssert1() {
		this.currentAssert = new Assert();
		this.currentAssert.setTest("hl7:id/@root='1.2.3'");
		this.currentAssert.setRole(AssertRole.ERROR);
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		this.currentAssert.setParentObject(firstRD);
		this.flattenAssert(this.currentAssert, 0);
		assertTrue(this.currentAssert.getTestFlatten().equals("hl7:id/@root='1.2.3'"));
	}
	
	@Test
	public void testFlattenAssert2() {
		this.currentAssert = new Assert();
		this.currentAssert.setTest("parent::cda:ClinicalDocument/$cdaCode");
		this.currentAssert.setRole(AssertRole.ERROR);
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		this.currentAssert.setParentObject(firstRD);
		this.flattenAssert(this.currentAssert, 0);
		assertTrue(this.currentAssert.getTestFlatten().equals("parent::cda:ClinicalDocument/$cdaCode"));
	}
	
	@Test
	public void testFlattenAssert3() {
		this.currentAssert = new Assert();
		this.currentAssert.setTest("parent::cda:ClinicalDocument/$cdaCode");
		this.currentAssert.setRole(AssertRole.ERROR);
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		this.currentAssert.setParentObject(firstRD);
		Let let = new Let();
		let.setName("cdaCode");
		let.setValue("cda:code");
		let.setParentObject(firstRD);
		firstRD.getLetOrAssertOrReport().add(let);
		this.flattenAssert(this.currentAssert, 0);
		assertTrue(this.currentAssert.getTestFlatten().equals("parent::cda:ClinicalDocument/cda:code"));
	}
	
	@Test
	public void testFlattenAssert4() {
		this.currentAssert = new Assert();
		this.currentAssert.setTest("parent::cda:ClinicalDocument/$cdaCode");
		this.currentAssert.setRole(AssertRole.ERROR);
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		this.currentAssert.setParentObject(firstRD);
		Let let = new Let();
		let.setName("cdaCode");
		let.setValue("cda:code/$codeAttr");
		let.setParentObject(firstRD);
		Let let2 = new Let();
		let2.setName("codeAttr");
		let2.setValue("@code");
		let2.setParentObject(firstRD);
		firstRD.getLetOrAssertOrReport().add(let);
		firstRD.getLetOrAssertOrReport().add(let2);
		firstRD.getLetOrAssertOrReport().add(this.currentAssert);
		this.flattenAssert(this.currentAssert, 0);
		assertTrue(this.currentAssert.getTestFlatten().equals("parent::cda:ClinicalDocument/cda:code/@code"));
	}
	
	@Test
	public void testFlattenAssert5() {
		Assert ass = new Assert();
		this.currentAssert = ass;
		this.flattenAssert(null, 0);
		assertTrue(this.currentAssert == ass);
		assertTrue(this.currentAssert.getTestFlatten() == null);
	}

	@Test
	public void testVerifyThatAssertIsProcessable3() {
		this.currentAssert = new Assert();
		this.currentAssert.setTestFlatten("cda:ClinicalDocument/cda:code");
		this.currentAssert.setRole(AssertRole.ERROR);
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		this.currentAssert.setParentObject(firstRD);
		assertTrue(this.verifyThatAssertIsProcessable(currentAssert));
		assertFalse(this.verifyThatAssertIsProcessable(null));
		assertFalse(this.verifyThatAssertIsProcessable(new Assert()));
	}
	
	@Test
	public void testVerifyThatAssertIsProcessable4() {
		this.currentAssert = new Assert();
		this.currentAssert.setTest("cda:ClinicalDocument/$cdaCode");
		this.currentAssert.setRole(AssertRole.ERROR);
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		this.currentAssert.setParentObject(firstRD);
		Let let = new Let();
		let.setName("cdaCode");
		let.setValue("cda:code/$codeAttr");
		let.setParentObject(firstRD);
		Let let2 = new Let();
		let2.setName("codeAttr");
		let2.setValue("@code");
		let2.setParentObject(firstRD);
		firstRD.getLetOrAssertOrReport().add(let);
		firstRD.getLetOrAssertOrReport().add(let2);
		firstRD.getLetOrAssertOrReport().add(this.currentAssert);
		this.flattenAssert(this.currentAssert, 0);
		assertTrue(this.verifyThatAssertIsProcessable(currentAssert));
	}

	@Test
	public void testFulfillOwnedRuleForConstraintGenerator() {
		this.currentAssert = new Assert();
		this.currentAssert.setTest("hl7:id/@root='1.2.3'");
		this.currentAssert.setRole(AssertRole.ERROR);
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		this.currentAssert.setParentObject(firstRD);
		this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
		this.listGeneratedOwnedRule = new ArrayList<OwnedRule>();
		this.flattenAssert(currentAssert, 0);
		this.fulfillOwnedRuleForConstraintGenerator();
		assertTrue(this.currentPackagedElement.getOwnedRule().size()==1);
		assertTrue(this.currentPackagedElement.getOwnedRule().get(0).getSpecification().getBody().contains("1.2.3"));
		assertTrue(this.currentPackagedElement.getOwnedRule().get(0).getConstrainedElement().equals(this.currentPackagedElement.getId()));
		assertTrue(this.currentPackagedElement.getOwnedRule().get(0).getOwnedComment().getBody() != null);
	}

	@Test
	public void testGenerateCommentConstraint1() {
		this.currentAssert = new Assert();
		this.currentAssert.setTest("hl7:id/@root='1.2.3'");
		this.currentAssert.setRole(AssertRole.ERROR);
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		this.currentAssert.setParentObject(firstRD);
		this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
		this.listGeneratedOwnedRule = new ArrayList<OwnedRule>();
		this.flattenAssert(currentAssert, 0);
		System.out.println(this.generateCommentConstraint());
		assertTrue(this.generateCommentConstraint().contains("shall verify this requirement : hl7:id/@root='1.2.3'"));
	}
	
	@Test
	public void testGenerateCommentConstraint2() {
		this.currentAssert = new Assert();
		this.currentAssert.setTest("hl7:id/@root='1.2.3'");
		this.currentAssert.getContent().add("This is high");
		this.currentAssert.setRole(AssertRole.ERROR);
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		this.currentAssert.setParentObject(firstRD);
		this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
		this.listGeneratedOwnedRule = new ArrayList<OwnedRule>();
		this.flattenAssert(currentAssert, 0);
		System.out.println(this.generateCommentConstraint());
		assertTrue(this.generateCommentConstraint().contains("This is high"));
	}
	
	@Test
	public void testGenerateCommentConstraint3() {
		this.currentAssert = new Assert();
		this.currentAssert.setTest("hl7:id/@root=$haha");
		this.currentAssert.setRole(AssertRole.ERROR);
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		Let let = new Let();
		let.setName("haha");
		let.setValue("mmm");
		firstRD.getLetOrAssertOrReport().add(let);
		this.currentAssert.setParentObject(firstRD);
		this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
		this.listGeneratedOwnedRule = new ArrayList<OwnedRule>();
		this.flattenAssert(currentAssert, 0);
		System.out.println(this.generateCommentConstraint());
		assertTrue(this.generateCommentConstraint().contains("hl7:id/@root=$haha"));
	}

	@Test
	public void testGenerateOCLConstraint() {
		this.currentAssert = new Assert();
		this.currentAssert.setTest("hl7:id/@root=$haha");
		this.currentAssert.setRole(AssertRole.ERROR);
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		Let let = new Let();
		let.setName("haha");
		let.setValue("mmm");
		firstRD.getLetOrAssertOrReport().add(let);
		this.currentAssert.setParentObject(firstRD);
		this.currentPackagedElement = PackagedElementUtil.initPackagedElement();
		this.listGeneratedOwnedRule = new ArrayList<OwnedRule>();
		this.flattenAssert(currentAssert, 0);
		System.out.println(this.generateOCLConstraint());
		assertTrue(this.generateOCLConstraint().contains("cda:id/@root=mmm"));
	}

	@Test
	public void testGetListNamespacesPrefix() {
		Set<String> ss = AssertAnalyzer.getListNamespacesPrefix(decorTemplates.getRules());
		assertTrue(ss.size()==2);
		assertTrue(ss.contains("hl7"));
		assertTrue(ss.contains("epsos"));
	}
	
	@Test
	public void testProcessXX1() throws Exception {
		GenerationProperties.ignoreAssertionGeneration = true;
		GenerationProperties.userSupportForAssertClass = null;
		GenerationProperties.ignoreXPathAxes = true;
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_assert2.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		RuleDefinition rdfirst = TemplateDefinitionUtil.getFirstElement(td);
		Assert ass = RuleDefinitionUtil.getAsserts(rdfirst).get(0);
		PackagedElement pe = PackagedElementUtil.initPackagedElement();
		List<OwnedRule> lgen = new ArrayList<OwnedRule>();
		(new AssertAnalyzer()).process(ass, pe, lgen);
		assertTrue(pe.getOwnedRule().size()==0);
	}
	
	@Test
	public void testProcessXX2() throws Exception {
		GenerationProperties.ignoreAssertionGeneration = false;
		GenerationProperties.userSupportForAssertClass = null;
		GenerationProperties.ignoreXPathAxes = true;
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_assert2.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		RuleDefinition rdfirst = TemplateDefinitionUtil.getFirstElement(td);
		Assert ass = RuleDefinitionUtil.getAsserts(rdfirst).get(1);
		PackagedElement pe = PackagedElementUtil.initPackagedElement();
		List<OwnedRule> lgen = new ArrayList<OwnedRule>();
		(new AssertAnalyzer()).process(ass, pe, lgen);
		assertTrue(pe.getOwnedRule().size()==1);
	}
	
	@Test
	public void testProcessXX3() throws Exception {
		GenerationProperties.ignoreAssertionGeneration = false;
		GenerationProperties.userSupportForAssertClass = null;
		GenerationProperties.ignoreXPathAxes = true;
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_assert2.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		RuleDefinition rdfirst = TemplateDefinitionUtil.getFirstElement(td);
		Assert ass = RuleDefinitionUtil.getAsserts(rdfirst).get(0);
		PackagedElement pe = PackagedElementUtil.initPackagedElement();
		List<OwnedRule> lgen = new ArrayList<OwnedRule>();
		(new AssertAnalyzer()).process(ass, pe, lgen);
		assertTrue(pe.getOwnedRule().size()==0);
	}
	
	@Test
	public void testProcessXX4() throws Exception {
		GenerationProperties.ignoreAssertionGeneration = false;
		GenerationProperties.userSupportForAssertClass = null;
		GenerationProperties.ignoreXPathAxes = false;
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_assert2.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		RuleDefinition rdfirst = TemplateDefinitionUtil.getFirstElement(td);
		Assert ass = RuleDefinitionUtil.getAsserts(rdfirst).get(0);
		PackagedElement pe = PackagedElementUtil.initPackagedElement();
		List<OwnedRule> lgen = new ArrayList<OwnedRule>();
		(new AssertAnalyzer()).process(ass, pe, lgen);
		assertTrue(pe.getOwnedRule().size()==1);
	}
	
	@Test
	public void testGetFormatedXPATH() throws Exception {
		String res = getFormatedXPATH("\"ee");
		assertTrue(res.equals("\\u0027ee"));
		res = getFormatedXPATH(null);
		assertTrue(res == null);
		res = getFormatedXPATH("");
		assertTrue(res.equals(""));
		res = getFormatedXPATH("zzéé");
		assertTrue(res.equals("zzéé"));
	}
	
	@Test
	public void testExtractOwnedRuleType1() throws Exception {
		this.currentAssert = new Assert();
		this.currentAssert.setRole(AssertRole.HINT);
		OwnedRuleType typ = this.extractOwnedRuleType();
		assertTrue(typ == OwnedRuleType.INFO);
	}
	
	@Test
	public void testExtractOwnedRuleType2() throws Exception {
		OwnedRuleType typ = this.extractOwnedRuleType();
		assertTrue(typ == null);
		this.currentAssert = new Assert();
		typ = this.extractOwnedRuleType();
		assertTrue(typ == null);
		this.currentAssert.setRole(AssertRole.FATAL);
		typ = this.extractOwnedRuleType();
		assertTrue(typ == OwnedRuleType.ERROR);
	}

}
