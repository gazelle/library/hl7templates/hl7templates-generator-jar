package net.ihe.gazelle.tempgen.test;

import net.ihe.gazelle.tempgen.action.DecorValidationLevelsAction;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DecorValidationLevelsActionTest {

    Decor decorTemplates = null;

    @Before
    public void setUp() {
        decorTemplates = DecorMarshaller.loadDecor("src/test/resources/flattening/L0.xml");
    }

    @Test
    public void testCreateDecorLevel1() throws JAXBException, IOException {
        Decor aa = DecorValidationLevelsAction.createDecorLevel1(decorTemplates);
        assertTrue(aa != null);
        ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(aa, baos1);
        String content1 = baos1.toString();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/flattening/L1.xml");
        ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(dec, baos2);
        String content2 = baos2.toString();
        assertEquals(content2.trim(), content1.trim());
    }

    @Test
    public void testCreateDecorLevel2() throws JAXBException, FileNotFoundException {
        Decor aa = DecorValidationLevelsAction.createDecorLevel1(decorTemplates);
        aa = DecorValidationLevelsAction.createDecorLevel2(aa);
        assertTrue(aa != null);
        ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(aa, baos1);
        String content1 = baos1.toString();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/flattening/L2.xml");
        ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(dec, baos2);
        String content2 = baos2.toString();
        assertEquals(content2.trim(), content1.trim());
    }

    @Test
    public void testCreateDecorLevel3() throws JAXBException, FileNotFoundException {
        Decor aa = DecorValidationLevelsAction.createDecorLevel1(decorTemplates);
        aa = DecorValidationLevelsAction.createDecorLevel2(aa);
        aa = DecorValidationLevelsAction.createDecorLevel3(aa);
        assertTrue(aa != null);
        ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(aa, baos1);
        String content1 = baos1.toString();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/flattening/L3.xml");
        ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(dec, baos2);
        String content2 = baos2.toString();
        assertEquals(content2.trim(), content1.trim());
    }

    @Test
    public void testCreateDecorLevel4() throws JAXBException, FileNotFoundException {
        Decor aa = DecorValidationLevelsAction.createDecorLevel1(decorTemplates);
        aa = DecorValidationLevelsAction.createDecorLevel2(aa);
        aa = DecorValidationLevelsAction.createDecorLevel3(aa);
        aa = DecorValidationLevelsAction.createDecorLevel4(aa);
        assertTrue(aa != null);
        ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(aa, baos1);
        String content1 = baos1.toString();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/flattening/L4.xml");
        ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(dec, baos2);
        String content2 = baos2.toString();
        assertEquals(content2.trim(), content1.trim());
    }

    @Test
    public void testCreateDecorLevel5() throws JAXBException, FileNotFoundException {
        Decor aa = DecorValidationLevelsAction.createDecorLevel1(decorTemplates);
        aa = DecorValidationLevelsAction.createDecorLevel2(aa);
        aa = DecorValidationLevelsAction.createDecorLevel3(aa);
        aa = DecorValidationLevelsAction.createDecorLevel4(aa);
        aa = DecorValidationLevelsAction.createDecorLevel5(aa);
        assertTrue(aa != null);
        ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(aa, baos1);
        String content1 = baos1.toString();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/flattening/L5.xml");
        ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(dec, baos2);
        String content2 = baos2.toString();
        assertEquals(content2.trim(), content1.trim());
    }

    @Test
    public void testCreateDecorLevelFlattenVocab() throws JAXBException, FileNotFoundException {
        Decor aa = DecorValidationLevelsAction.createDecorLevel1(decorTemplates);
        aa = DecorValidationLevelsAction.createDecorLevel2(aa);
        aa = DecorValidationLevelsAction.createDecorLevel3(aa);
        aa = DecorValidationLevelsAction.createDecorLevel4(aa);
        aa = DecorValidationLevelsAction.createDecorLevel5(aa);
        aa = DecorValidationLevelsAction.createDecorLevelFlattenVocab(aa);
        assertTrue(aa != null);
        ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(aa, baos1);
        String content1 = baos1.toString();
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/flattening/FLATTEN_VOCAB.xml");
        ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
        DecorMarshaller.marshallDecor(dec, baos2);
        String content2 = baos2.toString();
        assertEquals(content2.trim(), content1.trim());
    }

}
