package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.*;

import org.junit.Test;

import net.ihe.gazelle.tempgen.action.UMLElementsManager;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;

public class UMLElementsManagerTest {

	@Test
	public void testGetMaxAttribute1() {
		String path = "//hl7:author[templateId/@root=2.16.840.1.113883.2.4.3.11.60.22.10.102]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name/hl7:family";
		int max = (new UMLElementsManager()).getMaxAttribute(DPathExtractor.extractDElementFromDPath(path), 2);
		assertTrue(max == 1);
	}
	
	@Test
	public void testGetMaxAttribute2() {
		String path = "//hl7:author[templateId/@root=2.16.840.1.113883.2.4.3.11.60.22.10.102]/hl7:assignedAuthor";
		int max = (new UMLElementsManager()).getMaxAttribute(DPathExtractor.extractDElementFromDPath(path), 2);
		assertTrue(max == -1);
	}
	
	@Test
	public void testGetMaxAttribut3() {
		String path = "//hl7:author[templateId/@root=2.16.840.1.113883.2.4.3.11.60.22.10.102]/hl7:assignedAuthor";
		int max = (new UMLElementsManager()).getMaxAttribute(DPathExtractor.extractDElementFromDPath(path), 1);
		assertTrue(max == 1);
	}
	
	@Test
	public void testGetMaxAttribute4() {
		String path = "//hl7:author[templateId/@root=2.16.840.1.113883.2.4.3.11.60.22.10.102]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name/hl7:family";
		int max = (new UMLElementsManager()).getMaxAttribute(DPathExtractor.extractDElementFromDPath(path), 3);
		assertTrue(max == -1);
	}

	@Test
	public void testGetMinAttribute() {
		String path = "//hl7:author[templateId/@root=2.16.840.1.113883.2.4.3.11.60.22.10.102]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name/hl7:family";
		int min = (new UMLElementsManager()).getMinAttribute(DPathExtractor.extractDElementFromDPath(path), 2);
		assertTrue(min == 0);
		min = (new UMLElementsManager()).getMinAttribute(DPathExtractor.extractDElementFromDPath(path), 1);
		assertTrue(min == 1);
		min = (new UMLElementsManager()).getMinAttribute(DPathExtractor.extractDElementFromDPath(path), 3);
		assertTrue(min == 0);
		min = (new UMLElementsManager()).getMinAttribute(DPathExtractor.extractDElementFromDPath(path), 0);
		assertTrue(min == 0);
		min = (new UMLElementsManager()).getMinAttribute(DPathExtractor.extractDElementFromDPath(path), 4);
		assertTrue(min == 0);
		min = (new UMLElementsManager()).getMinAttribute(DPathExtractor.extractDElementFromDPath(path), 5);
		assertTrue(min == 0);
	}

}
