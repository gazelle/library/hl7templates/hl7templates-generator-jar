package net.ihe.gazelle.tempgen.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import net.ihe.gazelle.tempgen.nodes.check.NodeCirc;
import net.ihe.gazelle.tempgen.nodes.check.NodeCirc.Color;

public class NodeCircTest {

	@Test
	public void testGetColor() {
		NodeCirc nc = new NodeCirc();
		nc.setColor(Color.BLACK);
		assertTrue(nc.getColor() == Color.BLACK);
	}

	@Test
	public void testGetNodeName() {
		NodeCirc nc = new NodeCirc();
		nc.setNodeName("zz");
		assertTrue(nc.getNodeName().equals("zz"));
	}

	@Test
	public void testGetListChildNodes() {
		assertTrue((new NodeCirc()).getListChildNodes().size()==0);
		NodeCirc nc = new NodeCirc();
		nc.setListChildNodes(new ArrayList<NodeCirc>());
		assertTrue(nc.getListChildNodes().size()==0);
	}

}
