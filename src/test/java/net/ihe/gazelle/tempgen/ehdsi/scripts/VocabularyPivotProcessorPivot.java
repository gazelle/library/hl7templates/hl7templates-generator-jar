package net.ihe.gazelle.tempgen.ehdsi.scripts;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.tempapi.impl.VocabularyProcessorImpl;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ValueSetUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class VocabularyPivotProcessorPivot extends VocabularyProcessorImpl {

	private static List<ValueSet> listVSActive;

	private Vocabulary currentVocabulary;

	@SuppressWarnings("unchecked")
	@Override
	public void process(Vocabulary t, Object... objects) {
		if (listVSActive == null) {
			listVSActive = extractListActiveValueset(EHDSIPivotTransformer.ROOT);
		}
		this.currentVocabulary = t;
		if (this.currentVocabulary != null && this.currentVocabulary.getValueSet() != null
				&& this.currentVocabulary.getValueSet().contains(EHDSIPivotTransformer.ROOT)) {
			ValueSet relatedVS = extractValueSet(this.currentVocabulary.getValueSet());
			if (relatedVS != null) {
				this.currentVocabulary.setFlexibility(relatedVS.getEffectiveDate().toString());
			}
			else {
				System.out.println("Problem to find value set for : " + currentVocabulary.getValueSet());
			}
		}
	}
	
	private static List<ValueSet> extractListActiveValueset(String rOOT2) {
		Decor dec = DecorMarshaller
				.loadDecor("/home/aboufahj/Téléchargements/epsos-20171123T105428-en-US-decor-compiled.xml");
		List<ValueSet> existingVS = new ArrayList<>();
		for (ValueSet valueSet : dec.getTerminology().getValueSet()) {
			if (valueSet.getVersionLabel() != null && valueSet.getVersionLabel().equals("201711") && valueSet.getId().contains(rOOT2)) {
				existingVS.add(valueSet);
			}
		}
		for (ValueSet valueSet : existingVS) {
			List<ValueSet> extractListVSDuplicated = ValueSetUtil.extractListCompatibleValueSet(valueSet.getId(), existingVS);
			if (extractListVSDuplicated.size()>1) {
				System.err.println("Problem with value set : " + valueSet.getId());
			}
		}
		System.out.println("list of valuesets : " + existingVS.size());
		return existingVS;
	}

	private ValueSet extractValueSet(String valueSet2) {
		for (ValueSet valueSet : listVSActive) {
			if (valueSet.getId().equals(valueSet2)) {
				return valueSet;
			}
		}
		return null;
	}

}
