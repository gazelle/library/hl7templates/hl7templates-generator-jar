package net.ihe.gazelle.tempgen.inc.test;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.inc.checker.IncCheckChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IncCheckChoiceDefinitionTest extends IncCheckChoiceDefinition {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/inc/decor_choice.xml");
	}

	@Test
	public void testProcess() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition participantRole = RuleDefinitionUtil.getElementByName(firstRD, "hl7:participantRole");
		ChoiceDefinition choice = RuleDefinitionUtil.getChoices(participantRole).get(0);
		Map<String, Set<String>> mapIncludedTemplates = new HashMap<String, Set<String>>();
		assertTrue(mapIncludedTemplates.size()==0);
		this.process(choice, mapIncludedTemplates);
		assertTrue(mapIncludedTemplates.size()>0);
	}

}
