package net.ihe.gazelle.tempgen.inc.test;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempgen.inc.action.IncFlatTemplateDefinition;
import net.ihe.gazelle.tempgen.inc.action.IncludeStats;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class IncFlatTemplateDefinitionTest extends IncFlatTemplateDefinition {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/inc/decor_td.xml");
	}

	@Test
	public void testProcess() {
		TemplateDefinition td = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
		List<TemplateDefinition> ltd = RulesUtil.getTemplates(decorTemplates.getRules());
		List<TemplateDefinition> listCDATemplate = TemplateDefinitionUtil.extractListCDATemplate(ltd);
		IncludeStats incStats = new IncludeStats();
		this.process(td, null, listCDATemplate, incStats);
		assertTrue(TemplateDefinitionUtil.getIncludes(td) == null || TemplateDefinitionUtil.getIncludes(td).size()==0);
	}

	@Test
	public void testProcessIncludes() {
		TemplateDefinition td = RulesUtil.getTemplates(decorTemplates.getRules()).get(0);
		this.incStats = new IncludeStats();
		List<TemplateDefinition> ltd = RulesUtil.getTemplates(decorTemplates.getRules());
		this.listCDATemplate = TemplateDefinitionUtil.extractListCDATemplate(ltd);
		assertTrue(TemplateDefinitionUtil.getElements(td).size()==0);
		this.setCurrentTemplateDefinition(td);
		this.processIncludes(TemplateDefinitionUtil.getIncludes(td));
		assertTrue(incStats.getNumberIntegrated() == 1);
		assertTrue(TemplateDefinitionUtil.getElements(td).size()==1);
	}

}
