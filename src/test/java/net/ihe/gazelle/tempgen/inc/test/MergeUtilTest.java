package net.ihe.gazelle.tempgen.inc.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempgen.inc.action.MergeUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ConformanceType;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;

public class MergeUtilTest {

	@Test
	public void testUpdateClonePropertiesRuleDefinitionIntegerStringBooleanConformanceType() {
		RuleDefinition rd = new RuleDefinition();
		MergeUtil.updateCloneProperties(rd, 1, "2", true, ConformanceType.C);
		assertTrue(rd.getMinimumMultiplicity() == 1);
		assertTrue(rd.getMaximumMultiplicity().equals("2"));
		assertTrue(rd.isIsMandatory());
		assertTrue(rd.getConformance() == ConformanceType.C);
	}

	@Test
	public void testUpdateClonePropertiesIncludeDefinitionIntegerStringBooleanConformanceType() {
		IncludeDefinition rd = new IncludeDefinition();
		MergeUtil.updateCloneProperties(rd, 1, "2", true, ConformanceType.C);
		assertTrue(rd.getMinimumMultiplicity() == 1);
		assertTrue(rd.getMaximumMultiplicity().equals("2"));
		assertTrue(rd.isIsMandatory());
		assertTrue(rd.getConformance() == ConformanceType.C);
	}

	@Test
	public void testUpdateClonePropertiesChoiceDefinitionIntegerStringBooleanConformanceType() {
		ChoiceDefinition rd = new ChoiceDefinition();
		MergeUtil.updateCloneProperties(rd, 1, "2");
		assertTrue(rd.getMinimumMultiplicity() == 1);
		assertTrue(rd.getMaximumMultiplicity().equals("2"));
	}

}
